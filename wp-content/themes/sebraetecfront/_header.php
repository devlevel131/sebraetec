<!doctype HTML>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="<?= THEME_IMG ?>favicon.ico">

    <link rel="stylesheet" href="<?= THEME_ASSETS ?>bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= THEME_ASSETS ?>icomoon/style.css"/>
    <link rel="stylesheet" href="<?= THEME_CSS ?>fonts.css"/>
    <link rel="stylesheet" href="<?= THEME_CSS ?>main.css"/>

    <?php wp_head();?>

</head>

<body>

    <header>

        
        <?php
            $login_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
            $cart_url = wc_get_cart_url();
            $cart_count = WC()->cart->get_cart_contents_count();
        ?>

        <div class="bg-blue py-4">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-lg-3 d-flex align-items-center">

                        <div id="mobile-menu-button" href="" class="d-block d-lg-none me-3 mt-2"><span class="icon-menu color-white size-30 size-lg-34"></div>

                        <a id="logo" href="<?php echo get_home_url(); ?>">
                            <img src="<?= THEME_IMG ?>sebrae_logo.png" class="img-fluid"/>
                        </a>

                    </div>

                    <div class="col-lg-5 d-none d-lg-flex align-items-center justify-content-center">
                        
                        <form id="header-search" class="header-search d-flex align-items-center justify-content-between">
                            <input type="text"/>
                            <button type="submit"><span class="icon-search-2 color-white size-25"></span></button>
                        </form>

                    </div>

                    <div class="col-6 col-lg-4 d-flex align-items-center justify-content-end justify-content-lg-between ">

                        <div id="header-user" class="d-flex align-items-center justify-content-end justify-content-lg-between me-3 me-lg-0">


                            <a href="<?= $login_url ?>" id="header-user-icon">
                                <span class="icon-user color-white size-18 size-lg-25"></span>
                            </a>

                            <div class="ps-3 color-white d-none d-lg-block">

                                <?php if(is_user_logged_in()):?>

                                    Olá, faça seu <a class="avenir-black color-white a-line" href="<?= $login_url ?>">login</a><br/>
                                ou <a class="avenir-black color-white a-line" href="<?= $login_url ?>">cadastre-se</a>
                
                                <?php else: ?>

                                    Olá, faça seu <a class="avenir-black color-white a-line" href="<?= $login_url ?>">login</a><br/>
                                ou <a class="avenir-black color-white a-line" href="<?= $login_url ?>">cadastre-se</a>

                                <?php endif; ?>

                            </div>

                        </div>

                        <div id="header-cart">
                            <a class="cart" href="<?= $cart_url ?>">
                                <div class="cart-count size-12 size-lg-16"><?= $cart_count ?></div>
                                <span class="icon-shopping-cart color-white size-20 size-sm-28"></span>
                            </a>
                        </div>

                    </div>

                    <div class="col-12 d-flex d-lg-none align-items-center justify-content-center mt-4">
                        
                        <form id="header-search-mob" class="header-search d-flex align-items-center justify-content-between">
                            <input type="text"/>
                            <button type="submit"><span class="icon-search-2 color-white size-25"></span></button>
                        </form>

                    </div>

         
                </div>
            </div>
        </div>

    </header>

    <section id="menus" class="py-4">

        <div class="container">

            <div class="row">

                <div class="col-3 d-none d-lg-block">
          
                    <div id="menu-category" class="menu-drop">

                        <span class="icon-menu"></span>

                        <span>Todas as Categorias</span>

                        <span class="icon-nav-bottom"></span>

                        <div class="menu-drop-submenu">
                            <ul>
                            
                                <li>
                                    <a>Design dsd sdu sduhoh</a>
                                </li>
                                <li>
                                    <a>Design</a>
                                </li>
                                <li>
                                    <a>Design</a>
                                </li>
                                <li>
                                    <a>Design</a>
                                </li>
                                <li>
                                    <a>Design</a>
                                </li>
                                <li>
                                    <a>Design</a>
                                </li>
                            <ul>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-lg-6 size-03vw size-sm-16 size-lg-14 size-xl-16">
                    <ul id="menu-list">
                        <li>
                            <a href="#">Design</a>
                        </li>
                        <li>
                            <a href="#">Produtividade</a>
                        </li>
                        <li>
                            <a href="#">Qualidade</a>
                        </li>
                        <li>
                            <a href="">Top 10 Mais Vendidos</a>
                        </li>
                    </ul>
                </div>
                <div class="col-3 d-none d-lg-flex justify-content-end">
                
                <div id="menu-sector" class="menu-drop">

                    <span class="icon-menu"></span>

                    <span>Selecione o setor</span>

                    <span class="icon-nav-bottom"></span>

                    <div class="menu-drop-submenu">
                        <ul>
                        
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                        <ul>
                    </div>
                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--MENU MOBILE-->
    <nav id="menu-mobile" class="d-none">

        <div class="bg-blue-2">
            <div class="container py-4 pb-4 h-100">
                <div class="row h-100">

                    <div class="col-6 px-3 px-sm-4 d-flex align-items-center py-3">
                        <div class="color-white size-24 lh-1">
                            Selecione<br/> a Categoria
                        </div>
                    </div>

                    <div class="col-6 px-3 px-sm-4 d-flex align-items-center py-3">
                        
                        <div id="mobile-menu-close">
                            X
                        </div>
                        <div class="color-white size-24 lh-1">
                            Selecione<br/> o Setor
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container py-4">

            <div class="row">

                <div class="col-6 px-3 px-sm-4">
                
                    <div class="menu-mobile">
                        <ul>
                            <li>
                                <a>Design dsd sdu sduhoh</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                            <li>
                                <a>Design</a>
                            </li>
                        <ul>
                    </div>
            
                </div>

                <div class="col-6 px-3 px-sm-4">
                
                <div class="menu-mobile">
                    <ul>
                        <li>
                            <a>Design dsd sdu sduhoh</a>
                        </li>
                        <li>
                            <a>Design</a>
                        </li>
                        <li>
                            <a>Design</a>
                        </li>
                        <li>
                            <a>Design</a>
                        </li>
                        <li>
                            <a>Design</a>
                        </li>
                        <li>
                            <a>Design</a>
                        </li>
                    <ul>
                </div>
        
            </div>

            </div>

        </div>

    </nav>
    <!--MENU MOBILE-->

