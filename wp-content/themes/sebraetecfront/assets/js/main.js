jQuery(function($) {

    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $(window).on('resize scroll', function() {

        $(".scroll-opacity").filter(function() {
            return ($(this).isInViewport())
        }).css('opacity', '1');

    });

    $(document).ready(function() {


        $("#mobile-menu-button").click(function() {

            $("body").toggleClass('open-menu');
            $("#menu-mobile").toggleClass('d-none');

        });

        $("#mobile-menu-close").click(function() {

            $("body").toggleClass('open-menu');
            $("#menu-mobile").toggleClass('d-none');

        });

        $('#dinamic-banner-carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            items: 1,
            dots: false,
            mouseDrag: true,
            autoplay: true
        });

        $('#dinamic-banner-nav .owl-prev').click(function() {
            $('#dinamic-banner-carousel').trigger('prev.owl.carousel');
        });

        $('#dinamic-banner-nav .owl-next').click(function() {
            $('#dinamic-banner-carousel').trigger('next.owl.carousel');
        });

        $('#dinamic-banner-carousel-mob').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            autoplay: true,
            items: 1
        });


        $('#top-products-carousel').owlCarousel({
            loop: true,
            margin: 50,
            nav: true,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                992: {
                    items: 3
                }
            }
        });

        $('.product-carousel').owlCarousel({
            loop: true,
            margin: 0,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                992: {
                    items: 5,
                    nav: true
                }
            }
        });

        $('#related-carousel').owlCarousel({
            loop: false,
            margin: 0,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: true

                },
                768: {
                    items: 2,
                    nav: false,
                    dots: true
                },
                992: {
                    items: 4,
                    nav: true,
                    dots: false
                }
            }
        });

        $('#post-related-carousel').owlCarousel({
            loop: false,
            margin: 40,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: true

                },
                768: {
                    items: 2,
                    nav: false,
                    dots: true
                },
                992: {
                    items: 3,
                    nav: false,
                    dots: true
                }
            }
        });

        $('#product-testimony').owlCarousel({
            margin: 10,
            nav: true,
            dots: false,
            items: 2,
            center: true,
            loop: true,
            merge: true,
            video: true
        });


    });

    /* Checkout Term */
    var checkout_form = $('form.checkout');

    checkout_form.on('checkout_place_order', function() {
        if ($('#confirm-order-flag').length == 0) {
            checkout_form.append('<input type="hidden" id="confirm-order-flag" name="confirm-order-flag" value="1">');
        }
        return true;
    });

    var parcela_credito = '';
    var parcela_debito = '';


    $(document).ready(function() {


        $(document).on('change', '#parcela-cielo-webservice-credito', function() {
            parcela_credito = $(this).children("option:selected").val();
        });

        $(document).on('change', '#parcela-cielo-webservice-debito', function() {
            parcela_debito = $(this).children("option:selected").val();
        });

    });

    $(document.body).on('checkout_error', function() {
        var error_count = $('.woocommerce-error li').length;


        if (error_count == 1) { // Validation Passed (Just the Fake Error I Created Exists)
            // Show Confirmation Modal or Whatever
            $('.woocommerce-NoticeGroup-checkout .woocommerce-error').hide();

            var cnpj = $('#billing_cnpj').val();
            var dap_nirf = $('#billing_dap_nirf').val();
            var razao = $('#billing_company').val();
            var telefone = $('#billing_phone').val();
            var celular = $('#billing_cellphone').val();
            var nome = $('#billing_first_name').val() + ' ' + $('#billing_last_name').val();
            var cpf = $('#billing_cpf').val();
            var email = $('#billing_email').val();
            var endereco = $('#billing_address_1').val() + ' - ' + $('#select2-billing_city-container').text() + '/' + $('#select2-billing_state-container').text();
            var cep = $('#billing_postcode').val();

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '/' + mm + '/' + yyyy;

            Date.prototype.addDays = function(days) {
                var dat = new Date(this.valueOf());
                dat.setDate(dat.getDate() + days);
                return dat;
            }

            var dat = new Date();

            if ($('#select2-billing_perfil-container').attr('title') == 'Produtor Rural'){
                $('#term-cnpj').text(dap_nirf);
            }
            else{
                $('#term-cnpj').text(cnpj);
            }
            
            $('.term-razao').text(razao);
            $('#term-telefone').text(telefone);
            $('#term-celular').text(celular);
            $('#term-nome').text(nome);
            $('#term-cpf').text(cpf);
            $('#term-email').text(email);
            $('#term-endereco').text(endereco);
            $('#term-cep').text(cep);
            $('#term-data').text(dat.addDays(12).toLocaleDateString());
            $('#term-data-atual').text(today);

            $('#modal-term').modal('show');

        } else { // Validation Failed (Real Errors Exists, Remove the Fake One)
            $('.woocommerce-error li').each(function() {
                var error_text = $(this).text();
                console.log(error_text);
                if (error_text == '\n\t\t\tcustom_notice\t\t') {
                    $(this).css('display', 'none');
                }
            });
        }
    });

    $(document).ready(function() {

        $('#confirm-order-button').click(function() {
            $('#confirm-order-flag').val('');
            $('#parcela-cielo-webservice-credito').val(parcela_credito);
            $('#parcela-cielo-webservice-debito').val(parcela_debito);
            $('#modal-term').hide();
            $('#place_order').trigger('click');
        });

    });

    /* Mostrar opções */

    $(document).ready(function() {

        $(".btn-quero-comprar").click(function(e) {
            e.preventDefault()
            $(".product-box-buy").slideToggle();
        });

        if ($("#tm-extra-product-options").hasClass("tc-extra-product-options")) {
            $(".product-form").show();
        }


    });

    /* Pega iframe depoimentos */

    $(document).ready(function() {

        $("#product-testimony").on("click", ".product-testimony-thumb", function() {
            var iframe = $(this).data('iframe');
            $("#videoModal .video-container iframe").attr('src', iframe);
        });

        $("#videoModal").on('hide.bs.modal', function(e) {
            $("#videoModal .video-container iframe").attr('src', '');
        })

    });

    /* Títulos - produto com a mesma altura */

    $(document).ready(function() {
        var maxHeight = 0;

        $("h2.woocommerce-loop-product__title").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $("h2.woocommerce-loop-product__title").height(maxHeight);

    });

    /* Frase de feito - produto com a mesma altura */

    $(document).ready(function() {
        var maxHeight = 0;

        $(".woocommerce-LoopProduct-link .frase-efeito").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $(".woocommerce-LoopProduct-link .frase-efeito").height(maxHeight);

    });

    /*top produtos - nome com mesma altura */
    $(document).ready(function() {
        var maxHeight = 0;

        $("#top-products-carousel .top-product-name").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $("#top-products-carousel .top-product-name").height(maxHeight);

    });

    /*top produtos - frase com mesma altura */
    $(document).ready(function() {
        var maxHeight = 0;

        $("#top-products-carousel .top-product-phrase").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $("#top-products-carousel .top-product-phrase").height(maxHeight);

    });

    /* Divide box - produto com a mesma altura */

    $(document).ready(function() {
        var maxHeight = 0;

        $(".woocommerce-LoopProduct-link .divide-box").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $(".woocommerce-LoopProduct-link .divide-box").height(maxHeight);

    });

    /* Preço - produto com a mesma altura */

    $(document).ready(function() {
        var maxHeight = 0;

        $(".woocommerce-LoopProduct-link .price").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $(".woocommerce-LoopProduct-link .price").height(maxHeight);

    });

    /* Imagem galeria - produto com a mesma altura */

    $(document).ready(function() {
        var maxHeight = 0;

        $(".wpa-product-gallery .slick-slide").each(function() {
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });

        $(".wpa-product-gallery .slick-slide").height(maxHeight);

    });


    /* Selecionar PJ */

    $(document).ready(function() {
        if ($("form").hasClass("woocommerce-checkout")) {
            $('#billing_persontype').val(2);
        }
        if ($("nav").hasClass("woocommerce-MyAccount-navigation")) {
            $('#billing_persontype').val(2);
        }
        if ($("form").hasClass("adq-billing")) {
            $('#billing_persontype option[value="2"]').attr("selected", true);
        }
    });

    /* Valida campos de números */

    $(document).ready(function() {
        $("input[type='number'].tm-epo-field").change(function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max) {
                $(this).val(max);
            } else if ($(this).val() < min) {
                $(this).val(min);
            }
        });
    });

    /* Confirmação de e-mail */
    jQuery(document).ready(function() {
        jQuery('#ebook-form .wpcf7-submit').click(function() {
            // We remove the error to avoid duplicate errors
            jQuery('.error').remove();
            // We create a variable to store our error message
            var errorMsg = jQuery('<span class="wpcf7-not-valid-tip">Os e-mails precisam ser iguais.</span>');
            // Then we check our values to see if they match
            // If they do not match we display the error and we do not allow form to submit
            if (jQuery('#ebook-form .your-email').find('input').val() !== jQuery('#ebook-form .your-email-confirm').find('input').val()) {
                errorMsg.insertAfter(jQuery('#ebook-form .your-email-confirm').find('input'));
                return false;
            } else {
                // If they do match we remove the error and we submit the form
                jQuery('.error').remove();
                return true;
            }
        });
    });

    /* Mensagem enviada */
    jQuery(document).ready(function() {
        document.addEventListener('wpcf7mailsent', function(event) {
            if ('1604' == event.detail.contactFormId) { // Change 123 to the ID of the form 
                jQuery('#modal-ebook-sucesso').modal('show'); //this is the bootstrap modal popup id

                setTimeout(function() {
                    jQuery('#ebook-form .wpcf7-response-output').hide();
                    var redirect_url = jQuery('input.wpcf7-form-control.wpcf7-hidden.form-redirect').val();
                    window.location = redirect_url;
                }, 3000);
            }
            if ('1607' == event.detail.contactFormId) { // Change 123 to the ID of the form 
                jQuery('#modal-news-sucesso').modal('show'); //this is the bootstrap modal popup id
                jQuery('#newsletter .wpcf7-response-output').hide();
            }
        }, false);
    });

    /* Pega url */
    jQuery(document).ready(function() {
        var url = window.location.href;
        jQuery('input.wpcf7-form-control.wpcf7-hidden.form-url').attr("value", url);
    });

    jQuery(document).ready(function() {
        $(function() {
            $("#ebook-phone").keyup(function() {
                $(this).val($(this).val().replace(/[+]/g, ""));
            });
        });
    });

    $('#wc-simple-finish').click(function(event) {

        event.preventDefault();

        $('#wc-simple-form').submit();

        var id = $(this).val();

        var form = document.getElementById('wc-simple-form');

        var add_to_cart = document.createElement('input');
        var redirect = document.createElement('input');

        add_to_cart.setAttribute('name', 'add-to-cart');
        add_to_cart.setAttribute('value', id);
        add_to_cart.setAttribute('type', 'hidden');

        redirect.setAttribute('name', 'redirect');
        redirect.setAttribute('value', 'true');
        redirect.setAttribute('type', 'hidden');

        form.appendChild(add_to_cart);
        form.appendChild(redirect);

        form.submit();
    });
	
	$(document).ready(function() {

        //clean
        $("#billing_cnpj_field > label").empty();
        $("#billing_dap_nirf_field > label").empty(); //case exists anything

        //add the * in dap/nirf field
        $("#billing_dap_nirf_field > label").append('DAP/NIRF <abbr class="required" title="obrigatório">*</abbr>');

        //set
        if ($("#billing_perfil").val() == "Produtor Rural") {

            $("#billing_cnpj_field > label").append('CNPJ <span class="optional">(opcional)</span>');
            $("#billing_dap_nirf_field").addClass('d-block'); //because checkout form
            $("#billing_dap_nirf_field").show();
        } else {
            $("#billing_cnpj_field > label").append('CNPJ <abbr class="required" title="obrigatório">*</abbr>');
            $("#billing_dap_nirf_field").removeClass('d-block'); //because checkout form
            $("#billing_dap_nirf_field").hide();
        }

        //changes in perfil field
        $('#billing_perfil').change(function() {

            if ($(this).val() == "Produtor Rural") {
                $("#billing_dap_nirf_field").addClass('d-block'); //because checkout form
                $("#billing_dap_nirf_field").show();
                $("#billing_cnpj_field > label").empty();
                $("#billing_cnpj_field > label").append('CNPJ <span class="optional">(opcional)</span>');

            } else {
                $("#billing_dap_nirf_field").removeClass('d-block'); //because checkout form
                $("#billing_dap_nirf_field").hide();
                $("#billing_cnpj_field > label").empty();
                $("#billing_cnpj_field > label").append('CNPJ <abbr class="required" title="obrigatório">*</abbr>');
            }
        });

    });

});