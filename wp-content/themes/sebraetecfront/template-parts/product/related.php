<section class="related-products">

    <div class="container pt-5 pt-lg-0">

        <div class="color-blue mb-5 text-uppercase">Produtos Relacionados</div>

        <div id="related-carousel" class="owl-carousel owl-theme">

            <?php for($i = 0 ; $i < 6 ; $i++): ?>
            <div class="item">

                <div class="st-product pb-lg-4">

                    <div class="st-product-tag">Design</div>
                 
                    <a href="#" class="st-product-thumb mb-2">
                        <img src="<?= THEME_IMG ?>NOUP_03.png" class="img-fluid"/>
                    </a>

                    <div class="color-gray avenir-medium mb-3">Certificação de Serviços Automotivos</div>

                    <div class="color-gray mb-2 size-14">Certifique o processo de reparação/produção automotivo de sua empresa.</div>

                    <div class="mt-auto">

                        <div  class="size-14 avenir-medium">De <s>R$889,00</s></div>

                        <div class="size-18 avenir-black">Por até R$389,00</div>
                        <div class="size-14 color-gray avenir-medium">Divida sem juros no cartão</div>
                        <div class="color-red size-12 avenir-medium oblique"><span class="icon-star-full"></span> TOP 10 MAIS VENDIDOS</div>

                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <a href="#" class="btn st-product-add-to-cart">TAMBEM QUERO!</a>
                        </div>

                    </div>

                </div>
    
            </div>  
            <?php endfor; ?>

        </div>

    </div>

</section>
