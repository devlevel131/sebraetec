<section class="product">
    <div class="container">

        <div class="color-gray mb-4">
            <span class="avenir-medium d-block mb-3 size-24 text-uppercase">Design</span>

            <h1 class="avenir-black size-26">Desenvolvimento de Rótulos e Embalagens</h1>
            <h2 class="size-16">Regularize o seu produto perante o orgão de Comunicação</h2>
        </div>

        <div class="row">

            <div class="col-12 col-lg-8">

                <div class="row">

                    <div class="col-12 col-lg-6 mb-4">
                    
                        <div id="product-gallery">
                            <img src="<?= THEME_IMG ?>product_image.jpg" class="img-fluid"/>
                        </div>

                    </div>

                    <div class="col-12 col-lg-6 mb-4">
                        <div class="product-desc">

                            <span class="color-blue text-uppercase">Objetivo:</span>

                            <div class="color-gray size-15 lh-1-4">
                                Certifique o processo de reparação/produção automotivo de sua empresa.Certifique o processo de reparação/produção omotivo de sua empresa.Certifique o processo de omotivo de sua empresa.
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                    <div class="product-form">
                        <div class="product-form-title bg-blue px-5 py-3 color-white avenir-medium size-18">
                            Por favor, preencha às questões abaixo para que seja possível gerar a cotação:
                        </div>
                        <form class="product-form-content px-5 py-4">

                            <div class="mb-3 size-14">
                                O Cliente tem conhecimento absoluto do conteúdo da Ficha Técnica do Produto e está de acordo com as Condições descritas no Documento? Se a resposta for NÃO, o produto NÃO poderá ser contratado.
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="sim">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="nao">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Não</label>
                                </div>
                            </div>

                            <div class="mb-3  size-14">
                                O Cliente está ciente de que a realização de fotografias publicitárias para o E-commerce NÃO é contemplada pela Consultoria, sendo, portanto, de sua responsabilidade?
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="sim">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="nao">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Não</label>
                                </div>
                            </div>

                            <div class="mb-3  size-14">
                                O Cliente está ciente de que, após o período de 1 (um) ano de Consultoria, a aquisição de domínio e hospedagem passam a ser de sua responsabilidade?
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="sim">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="nao">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Não</label>
                                </div>
                            </div>

                            <div class="mb-3 size-14">
                                <div class="row">
                                    <div class="col-12 col-lg-7 mb-2 mb-lg-0">
                                        Qual a modalidade de plataforma que o Cliente deseja contratar para o E-Commerce?
                                    </div>
                                    <div class="col-12 col-lg-5">
                                    <select class="form-select form-select-sm" aria-label=".form-select-sm example">
                                        <option selected>Open this select menu</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                </div>   
                            </div>

                            <div class="mb-3  size-14">
                                O Cliente deseja contratar o desenvolvimento de 02 (dois) cards profissionais para divulgação do E-commerce pelo Cliente por meio de redes sociais?
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="sim">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="nao">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Não</label>
                                </div>
                            </div>

                            <div class="mb-3  size-14">
                                O cliente está ciente de que a consultoria NÃO contempla o design da marca?
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="sim">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="nao">
                                    <label class="form-check-label color-blue" for="inlineRadio1">Não</label>
                                </div>
                            </div>

                        </form>
                    </div>
                        <!--product-form-end--> 
                        <div class="py-4 d-flex flex-column flex-lg-row align-items-center justify-content-center">
                            <a class="btn btn-yellow product-page-btn btn-add-to-cart text-uppercase me-lg-3 mb-3 mb-lg-0">Adicionar ao carrinho</a>
                            <a class="btn btn-blue product-page-btn text-uppercase">Finalizar compra</a>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-12 col-lg-4 mb-4">
                <div class="product-right-col ps-4 pe-4 ps-lg-0 pe-lg-5">

                    <div class="d-flex d-lg-block flex-column align-items-center">

                        <div class="mb-4">
                            <div class="size-16 avenir-medium">De <s>R$889,00</s></div>
                            <div class="size-24 avenir-black color-gray mb-1">Por até R$389,00</div>
                            <div class="size-16 color-gray avenir-medium mb-2">Divida sem juros no cartão</div>
                            <div class="color-red size-12 avenir-medium oblique"><span class="icon-star-full"></span> TOP 10 MAIS VENDIDOS</div>
                        </div>

                        <a href="#" class="btn btn-blue btn-buy w-100">QUERO COMPRAR</a>

                    </div>

                    <div class="mb-4 mt-4 d-flex align-items-center justify-content-center justify-content-lg-start">
                        <span class="btn-like me-1">
                            <span class="icon-like"></span>
                        </span>
                        <span class="color-gray avenir-medium text-uppercase size-04vw size-xs-16">Quem comprou, recomenda!</span>
                    </div>

                    <div>
                        <div id="product-testimony" class="owl-carousel owl-theme">
                            <?php for($i = 0 ; $i < 6 ; $i++): ?>

                                <div class="item">
                                    <a class="product-testimony-thumb" data-bs-toggle="modal" data-bs-target="#videoModal" >
                                        <img src="<?= THEME_IMG ?>video_img_2.jpg" class="img-fluid"/>
                                    </a>
                                </div>

                            <?php endfor;?>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!--MODAL-->
        <div class="modal fade" id="videoModal" tabindex="-1" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="videoModalLabel">Depoimento</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="video-container">
                    <iframe width="640" height="360" src="https://www.youtube.com/embed/vgcI6JUmgxM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
        </div>
        <!--modal-->

    </div>
</section>