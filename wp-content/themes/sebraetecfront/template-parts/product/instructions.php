<section id="product-instructions">
    <div class="container">

        <div class="mb-4">
            <span class="color-blue d-block">ENTREGA:</span>
            <span class="color-gray d-block">Certifique o processo de reparação/produção automotivo de sua empresa.Certifique o processo de reparação/produção omotivo de sua empresa.Certifique o processo de omotivo de sua empresa.</span>
        </div>

        <div class="mb-4">
            <span class="color-blue d-block">ENTREGA:</span>
            <span class="color-gray d-block">O Tour virtual 360º vem se mostrando uma das ferramentas com maior poder de venda e eficácia na internet. Assim como uma fotografia agrega valor ao seu negócio, a fotografia 360 multiplica esse valor agregado, marcando uma experiência sem limites, como se o Cliente estivesse no local. Ele possibilita um passeio completo por todos ambientes de uma mesma estrutura.</span>
        </div>

        <div class="mb-4">
            <span class="color-gray avenir-medium d-block">Observação:</span>
            <span class="color-gray d-block">Para contratar a consultoria a empresa já deve dispor de website ou aplicativos previamente à contratação</span>
        </div>

        <div class="mb-4">
            <span class="color-gray avenir-medium d-block">Recomendado para os seguintes Portes:</span>
            <span class="color-gray d-block">Microempresa, Microempreendedor Individual e Empresa de Pequeno Porte.</span>
        </div>

        <div class="mb-4">
            <span class="color-gray avenir-medium d-block">Beneficios:</span>
            <span class="color-gray d-block">
                – Maior interatividade na página digital do cliente;</br>
                – Melhor posicionamento da empresa;</br>
                – Maior visibilidade nas buscas do Google. As empresas que possuem Google Street View Trusted estão melhores posicionadas no rankeamento de busca;</br>
                – Integração com aplicativos do Google Maps e Street View.</br>
            </span>
        </div>

        <div class="mb-4">
            <span class="color-gray avenir-medium">Prazo de Entrega: </span>
            <span class="color-gray">45 dias.</span>
        </div>

        <div class="mb-4">
            <span class="color-blue d-block text-uppercase">Atende aos setores:</span>
            <span class="color-gray d-block">Turismo, Serviço</span>
        </div>

        <div class="mb-4">
            <span class="color-blue d-block text-uppercase">Atende aos segmentos:</span>
            <span class="color-gray d-block">Segmento: Hotéis, Pousadas, Hostel, Restaurantes, Delicatessen, Bares, Escritórios, Lojas, Prédios Empresariais, Faculdades, Escolas, Condomínios, Prédios Residenciais, Shoppings, Academias, Clínicas, SPAS, Pontos Turísticos, Museus.</span>
        </div>

        <div class="mb-4">
            <span class="color-blue d-block text-uppercase">Publico Alvo:</span>
            <span class="color-gray d-block">Microempreendedor Individual, Microempresa, Empresa de Pequeno Porte.</span>
        </div>

    </div>
</section>