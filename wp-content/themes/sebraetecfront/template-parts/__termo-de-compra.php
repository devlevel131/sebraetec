<?php
defined( 'ABSPATH' ) || exit;
?>

<div>
    <table>
        <tbody>
            <tr>
                <td>
                    <p>EMPRESA DEMANDANTE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CNPJ ou equivalente: </strong> <span id="term-cnpj">#cnpj_equivalente#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Razão Social: </strong> <span class="term-razao">#razao_social#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Telefone:</strong> <span id="term-telefone">#telefone#</span></p>
                </td>
                <td>
                    <p><strong>Celular</strong>: <span id="term-celular">#celular#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Nome do Responsável:</strong> <span id="term-nome">#nome_responsavel#</span></p>
                </td>
                <td>
                    <p><strong>CPF:</strong> <span id="term-cpf">#cpf#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>E-mail:</strong> <span id="term-email">#email#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Endereço:</strong> <span id="term-endereco">#endereco#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CEP:</strong> <span id="term-cep">#cep#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>DADOS DO EVENTO</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Produto(s):</strong> 
					<?php					
					$items_cart = WC()->cart->get_cart();
					foreach ( $items_cart as $cart_item_key => $cart_item ) {
						$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							?>
							
							<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ) . ' - '; ?>
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); 
							if ($cart_item_key !== array_key_last($items_cart)) {
								echo ' / ';
							}
							?>
							
							<?php
						}
					}
					?>
					</p>
                </td>
				
            </tr>
            <tr>
                <td>
                    <p><strong>Data Prevista de Início:</strong> <span id="term-data">12 dias</span></p>
                </td>
                <td>
                    <p><strong>Data Prevista de Término: </strong>Tabela - Anexo I</p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA PRIMEIRA – DO PREÇO</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>Por este instrumento, assume(m) o compromisso de efetuar o pagamento ao SEBRAE/BA, da importância total de <?php echo $amount;?>, pelos serviços referidos neste Termo de Adesão e Compromisso, no(s) seguinte(s) termo(s):</p>
    <p><strong>A <span class="term-razao">#razao_social#</span> a importância de <?php echo $amount;?>.</strong></p>
    <p><strong>Parágrafo Primeiro</strong> – A contrapartida deve ser paga no Cartão de débito; Cartão de crédito, de acordo com a orientação vigente.</p>
    <p>
        <strong>Parágrafo Segundo</strong> – O não pagamento de qualquer das parcelas implicará, conforme o caso, na suspensão ou cancelamento do serviço e, a critério do SEBRAE/BA, na adoção de medidas legais cabíveis, judiciais ou
        extrajudiciais.
    </p>
    <p><strong>Parágrafo Terceiro</strong> – Os Responsáveis Financeiros citados nesta Cláusula Primeira compreendem que estão arcando com contrapartida de até 30% do preço do serviço em função de subsídio concedido pelo SEBRAE/BA.</p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA SEGUNDA – DAS OBRIGAÇÕES</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>I - São obrigações da <span class="term-razao">#razao_social#</span>:</p>
    <p><strong>a) </strong>Avaliar as entregas realizadas pela ENTIDADE EXECUTORA (parcial e final).</p>
    <p><strong>b) </strong>Responsabilizar-se pelo efetivo repasse da contrapartida ao SEBRAE/BA, quando a contrapartida não for de responsabilidade, exclusiva, do parceiro/responsável financeiro.</p>
    <p>
        <strong>c) </strong>Disponibilizar ao SEBRAE Nacional e/ou SEBRAE/BA, a qualquer tempo, informações sobre os serviços prestados, sobre os resultados obtidos ou sobre a ENTIDADE EXECUTORA contratada pelo SEBRAE/BA por meio do
        SEBRAETEC.
    </p>
    <p><strong>d) </strong>Cumprir as disposições previstas no Edital SEBRAETEC Bahia vigente no ato da celebração do Termo de Adesão.</p>
    <p><strong>e) </strong>Responsabilizar-se para que a utilização dos recursos na prestação de serviços não seja indevida ou desnecessária.</p>
    <p><strong>f) </strong>Receber os representantes do SEBRAE/BA com o devido agendamento prévio.</p>
    <p>
        <strong>g) </strong>Responder às pesquisas de satisfação dos serviços prestados e da efetividade do SEBRAETEC realizadas pelo SEBRAE/BA e/ou pelo SEBRAE/NA, responsabilizando-se pela veracidade, exatidão e completude das respostas.
    </p>
    <p><strong>h) </strong>Restituir ao SEBRAE/BA os valores investidos na proposta, caso haja comprovação de ação em conjunto com a ENTIDADE EXECUTORA para lesar o SEBRAE/BA.</p>
    <p><strong>i) </strong>Assinar este Termo de Adesão de Compromisso antes do início do serviço, conforme padrão definido no Sistema de contratação do SEBRAETEC.</p>
    <p>
        <strong>j) </strong>Assinar a Declaração de Conclusão do Serviço apenas após o término completo do serviço, sob pena de restituir os valores pagos pelo SEBRAE/BA para a realização do serviço, conforme padrão definido no Sistema de
        contratação do SEBRAETEC.
    </p>
    <p>
        <strong>k) </strong>Responder pelo pagamento proporcional dos serviços até então realizados em caso de solicitação de distrato da proposta por sua própria iniciativa, quando a contrapartida não for de responsabilidade, exclusiva, do
        parceiro/responsável financeiro.
    </p>
    <p>
        <strong>l) </strong>Estar enquadrada como público do SEBRAETEC: Clientes do Sistema SEBRAE com CNPJ (MEI, ME e EPP); Pessoas físicas que estejam registradas no Sistema de Informações Cadastrais do Artesanato Brasileiro - SICAB
        tenham a Carteira Nacional do Artesão ou Carteira Nacional de Trabalhador Manual, fature até R$ 4.800.000,00 (quatro milhões e oitocentos mil reais) por ano e esteja com a carteira válida no momento do atendimento; e produtores
        rurais que possuam inscrição estadual de produtor, número do Imóvel Rural na Receita Federal (NIRF) ou declaração de aptidão (DAP) ao Programa Nacional de Fortalecimento da Agricultura Familiar (Pronaf). Soma-se ao grupo de
        produtores rurais os pescadores com registro no Ministério da Agricultura, Pecuária e Abastecimento.
    </p>
    <p><strong>m) </strong>Responsabilizar-se pelas informações prestadas no momento do atendimento.</p>
    <p><strong>Parágrafo único</strong> – Após assinatura da Declaração Parcial ou de Conclusão do Serviço a Empresa Demandante não poderá contestar, sob qualquer hipótese, a revisão do serviço prestado.</p>
    <p>II – São obrigações do SEBRAE/BA:</p>
    <ol>
        <li>Contratar a Prestadora de Serviço Tecnológico - PST para executar os serviços decorrentes deste termo.</li>
        <li>Acompanhar toda a execução do serviço ora acordado, bem como subsidiar os custos nas condições estabelecidas.</li>
        <li>Efetuar, quando couber, a devolução da contrapartida paga pelo cliente, nos casos em que este pagamento ocorra antes do processo de cotação.</li>
        <li>Demais obrigações impostas pelo Edital de Cadastramento SEBRAETEC vigente.</li>
    </ol>
    <p><strong>CLÁUSULA TERCEIRA– DAS SANÇÕES</strong></p>
    <p>
        Caso se verifique participação do contratante ou da Empresa BENEFICIADA em conjunto com Prestador de Serviços Tecnológicos em ações prejudiciais ao SEBRAE/BA, estes ficarão impedidos de demandar serviços no SEBRAETEC pelo prazo de
        até dois anos, além de outras medidas judiciais cabíveis que o SEBRAE/BA entenda pertinente.
    </p>
    <p>É permitido a qualquer das partes denunciar o contrato, desde que de forma motivada.</p>
    <p><strong>CLÁUSULA QUARTA– DA MORA</strong></p>
    <p>Em caso de mora no cumprimento das obrigações, haverá incidência de multa de 2% no vencimento e 1% ao mês.</p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA QUINTA – DO FORO</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>Fica estabelecido entre as partes que o foro competente para a solução de qualquer litígio decorrente deste contrato é o de Salvador - BA, excluído qualquer outro.</p>
    <p>E, por assim terem acordado, assinam este Termo de Adesão e Compromisso.</p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p>Salvador - BA, <span id="term-data-atual">#Data#</span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table style="text-align:center;width:100%;">
        <tbody>
            <tr>
                <td>
                    <p><img src="<?= THEME_IMG ?>assinatura-jorge-khoury.jpg" class="img-fluid"/></p>
                </td>
                <td>
                    <p></p>
                </td>
                <td>
                    <p><img src="<?= THEME_IMG ?>assinatura-franklin-santana.jpg" class="img-fluid"/></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        Jorge Khoury<br />
                        Diretor Superintendente
                    </p>
                </td>
                <td>
                    <p></p>
                </td>
                <td>
                    <p>
                        Franklin Santana Santos<br />
                        Diretor Técnico
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <p><strong>ANEXO I - PRAZO MÁXIMO DE ENTREGA </strong></p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>Solução SEBRAETEC</strong></p>
                </td>
                <td>
                    <p><strong>Prazo de Entrega (dias)</strong></p>
                </td>
            </tr>
            <tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Certifica&ccedil;&atilde;o de Processos de Teste MPT.BR N&iacute;vel 2</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR 15575:2013 - Desempenho de Edifica&ccedil;&otilde;es Habitacionais</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Norma ABNT NBR 16170:2013 - Qualidade do P&atilde;o tipo Franc&ecirc;s</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 15189:2015 - Laborat&oacute;rios Cl&iacute;nicos</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 22000:2019 - Sistemas de Gest&atilde;o da Seguran&ccedil;a de Alimentos</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 9001:2015 - Sistema de Gest&atilde;o da Qualidade - Petr&oacute;leo, G&aacute;s e Sistemistas da Ind&uacute;stria (Setor Petr&oacute;leo)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO IEC 20000-1:2011 - Tecnologia da Informa&ccedil;&atilde;o - Gest&atilde;o de Servi&ccedil;os</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISSO 9001:2015</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma Rainforest Alliance para Agricultura Sustent&aacute;vel (RAS) - Cadeia de Cust&oacute;dia</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma Rainforest Alliance para Agricultura Sustent&aacute;vel (RAS) - C&oacute;digo de Conduta</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Regulamenta&ccedil;&atilde;o da Produ&ccedil;&atilde;o Org&acirc;nica no Brasil</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; s&eacute;rie de normas ABNT NBR ISO 12647 - Tecnologia gr&aacute;fica - Provas digitais</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o ao Programa Brasileiro da Qualidade e Produtividade do Habitat (PBQP-H)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave;s Normas Ambientais para Servi&ccedil;os Automotivos</p></td>
		<td><p>35</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave;s Normas de Qualidade para Servi&ccedil;os Automotivos</p></td>
		<td><p>35</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o conforme Modelo CMMI-DEV, CMMI-ACQ e CMMI-SVC</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de agroind&uacute;strias aos Servi&ccedil;os de Inspe&ccedil;&atilde;o de Produtos de Origem Animal e/ou Vegetal</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de estabelecimentos produtores de bebidas alco&oacute;licas para registro</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de ind&uacute;strias &agrave;s boas pr&aacute;ticas de fabrica&ccedil;&atilde;o de produtos saneantes, de higiene pessoal, cosm&eacute;ticos e perfumes</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de Produtos aos Regulamentos T&eacute;cnicos e aos Mecanismos de Avalia&ccedil;&atilde;o da Conformidade Compuls&oacute;rios - Panelas Met&aacute;licas</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o do Manejo Nutricional de Rebanho Leiteiro - Bovinocultura</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o para Certifica&ccedil;&atilde;o de Produ&ccedil;&atilde;o Integrada da Cadeia Agr&iacute;cola e Boas Pr&aacute;ticas na Produ&ccedil;&atilde;o Agr&iacute;cola</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Aplica&ccedil;&atilde;o do M&eacute;todo de Implementa&ccedil;&atilde;o de Softwares com Tecnologia BIM</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Processos nas Ind&uacute;strias de Alimentos</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Processos nas Ind&uacute;strias de Alimentos - Modalidade Compacta</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Tempo de Vida de Prateleira</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Tempo de Vida Prateleira</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Biotecnologia da Fermenta&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas de Fabrica&ccedil;&atilde;o para Empresas de Alimentos e Bebidas</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas Higi&ecirc;nico-Sanit&aacute;rias e Cuidados Contra a COVID-19</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas Higi&ecirc;nico-Sanit&aacute;rios e Cuidados Contra COVID-19</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas na Pecu&aacute;ria de Leite e/ou Corte - Ovino e Caprino</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas no Segmento de Beleza</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas para Servi&ccedil;os de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Branding</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o Ambiental de Servi&ccedil;os Automotivos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o conforme norma ABNT NBR ISO 9001:2015 - Sistema de Gest&atilde;o da Qualidade</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o conforme protocolo GlobalGAP</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Conte&uacute;do Local Para Servi&ccedil;os e Equipamentos (ANP)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Produtos Org&acirc;nicos</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Servi&ccedil;os Automotivos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certificado de Registro Cadastral - CRC (Setor Petr&oacute;leo)</p></td>
		<td><p>150</p></td>
	</tr>
	<tr>
		<td><p>Consultoria Omnichannel Para Integra&ccedil;&atilde;o dos Canais de Vendas</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para a Implanta&ccedil;&atilde;o de Estrat&eacute;gias de Manejo de Moscas-das-Frutas baseadas no Controle Autocida</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para Acompanhamento de Projeto Piloto em BIM</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para cria&ccedil;&atilde;o de BIM Mandate e Plano de Implanta&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para Mapeamento de Fluxo de Pessoas e Intelig&ecirc;ncia para o Varejo</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para o Monitoramento de Moscas-das-Frutas em Pomares de Frutas</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos </p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Geral</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Ind&uacute;stria Gr&aacute;fica</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Modelagem de Produtos de Moda</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Otimiza&ccedil;&atilde;o do Processo Produtivo Gr&aacute;fico</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos com Conectividade (IoT)</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Arte de Card&aacute;pio</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Logomarca</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Mascote</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Pe&ccedil;as Digitais &ndash; Redes Sociais</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Pe&ccedil;as Gr&aacute;ficas &ndash; Itens de Papelaria</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Website</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Dep&oacute;sito de Patente de Inven&ccedil;&atilde;o ou de Modelo de Utilidade</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Cole&ccedil;&otilde;es - Arquitetura de Cole&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Cole&ccedil;&otilde;es - Design de Moda</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de M&iacute;dias Digitais de Comunica&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Novos Produtos Aliment&iacute;cios - Geral</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Produto</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento e Implanta&ccedil;&atilde;o de Estrat&eacute;gia, Campanha e A&ccedil;&otilde;es Transm&iacute;dia Storytelling</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Ambiente Interno</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Fachada </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Parklet</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Quiosque </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Embalagem </p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design de Produto Tridimensional - Artesanato</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design de R&oacute;tulo</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design e Melhoria de Servi&ccedil;os</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Efici&ecirc;ncia Energ&eacute;tica</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Card&aacute;pio para Servi&ccedil;os de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Fichas T&eacute;cnicas para o Segmento de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Fichas T&eacute;cnicas para o Segmento de Alimenta&ccedil;&atilde;o - Padroniza&ccedil;&atilde;o de Produtos Panific&aacute;veis</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Fertiliza&ccedil;&atilde;o In Vitro (FIV) - Rebanho</p></td>
		<td><p>150</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o da Loja Virtual</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Processos de Gest&atilde;o da Inova&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Requisitos de SMS (Sa&uacute;de, Meio Ambiente e Seguran&ccedil;a) para Fornecedores - Setor Petr&oacute;leo</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Requisitos para Acredita&ccedil;&atilde;o Hospitalar - ONA</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o do C&oacute;digo de Barra</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o ou Adequa&ccedil;&atilde;o na Opera&ccedil;&atilde;o do Delivery</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Impulsionamento das Redes Sociais</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Insemina&ccedil;&atilde;o Artificial por Tempo Fixo - IATF - Rebanho - Bovino</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Ind&uacute;stria da Moda</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Ind&uacute;stria de Alimentos</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Laborat&oacute;rio Fabril</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria da Qualidade do Leite</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Melhoria de Layout Produtivo</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Melhoria de Processo de Produ&ccedil;&atilde;o para o Segmento de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Banana</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Caf&eacute;</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Geral</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Calibra&ccedil;&atilde;o</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Ensaios - Avalia&ccedil;&atilde;o de Carca&ccedil;a de Bovino de Corte atrav&eacute;s de Ultrassonografia</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Ensaios - Servi&ccedil;o de An&aacute;lise Microbiol&oacute;gica</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Organiza&ccedil;&atilde;o e Controle de Estoque - Segmento de Beleza</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Otimiza&ccedil;&atilde;o da Cadeia de Suprimentos</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;gua Subterr&acirc;nea</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;guas Superficiais - Estaduais</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;guas Superficiais - Federais</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Passeio Virtual &ndash; Tour Virtual 360&ordm; </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o - Ind&uacute;stria da Moda</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o - Padroniza&ccedil;&atilde;o de M&eacute;todos de Manufatura no Setor da Moda</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Planejamento para Busca Org&acirc;nica &ndash; SEO </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Plano de Gerenciamento de Res&iacute;duos S&oacute;lidos</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Presen&ccedil;a Digital do Zero</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Preserva&ccedil;&atilde;o de Produtos por T&eacute;cnicas de Congelamento</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Prospec&ccedil;&atilde;o de &Aacute;guas Subterr&acirc;neas com An&aacute;lise Geof&iacute;sica - Ambiente Fraturado ou C&aacute;rstico</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Prototipagem de Novos Produtos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Prototipagem via Laborat&oacute;rio Aberto</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Qualidade do Caf&eacute; - Crit&eacute;rios SCAA - Colheita e P&oacute;s-Colheita</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Qualidade no Turismo - Implanta&ccedil;&atilde;o de procedimentos</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Realidade Aumentada Para os Pequenos Neg&oacute;cios</p></td>
		<td><p>270</p></td>
	</tr>
	<tr>
		<td><p>Realiza&ccedil;&atilde;o de Modelagem e Simula&ccedil;&otilde;es de projetos em BIM</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio na Cozinha</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio nos Pequenos Neg&oacute;cios</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio nos Pequenos Neg&oacute;cios - Segmento de Beleza</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Registro de Desenho Industrial</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Rotulagem de Alimentos e Informa&ccedil;&atilde;o Nutricional</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Rotulagem de Alimentos e Informa&ccedil;&atilde;o Nutricional</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Sistema APPCC - An&aacute;lise de Perigos e Pontos Cr&iacute;ticos de Controle</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Turista Oculto - Diagn&oacute;stico Tecnol&oacute;gico</p></td>
		<td><p>30</p></td>
	</tr>
	<tr>
		<td><p>Ultracongelamento de Produtos Aliment&iacute;cios</p></td>
		<td><p>90</p></td>
	</tr>
        </tbody>
    </table>
</div>
