<section id="dinamic-banner" class="mb-4">

    <?php $banner_list = get_field('slide_home', 59); ?>

    <?php if( $banner_list ): ?>

    <!--DESKTOP-->
    <div class="d-none d-md-block">

      <div id="dinamic-banner-carousel" class="owl-carousel owl-theme">

        <?php foreach ($banner_list as $banner): ?>
          <div class="item">

            <?php if($banner["link_slide_home"]): ?>	

              <a href="<?= $banner["link_slide_home"] ?>">
                <img src="<?= $banner["background_slide_home"] ?>" class="img-fluid">
              </a>

            <?php else:?>

              <img src="<?= $banner["background_slide_home"] ?>" class="img-fluid">

            <?php endif;?>

          </div>
        <?php endforeach;?>
      </div>

      <div id="dinamic-banner-nav">
          <div class="container d-flex justify-content-between">
              <button type="button" class="owl-prev">
                  <div class="arrow-left">
              </button>
              <button type="button" class="owl-next">
                  <div class="arrow-right">
              </button>
          </div>
      </div>

    </div>
    <!--DESKTOP END-->

    <!--MOBILE-->
    <div class="d-block d-md-none">

        <div id="dinamic-banner-carousel-mob" class="owl-carousel owl-theme">
          <?php foreach ($banner_list as $banner_mob): ?>	
            <div class="item">

              <?php if($banner_mob["link_slide_home"]): ?>	

                <a href="<?= $banner_mob["link_slide_home"] ?>">
                  <img src="<?= $banner_mob["background_mobile_slide_home"] ?>" class="img-fluid">
                </a>

              <?php else:?>
              
                <img src="<?= $banner_mob["background_mobile_slide_home"] ?>" class="img-fluid"/>
              
              <?php endif;?>

            </div>
          <?php endforeach;?>
        </div>

    </div>
    <!--MOBILE END-->
      
    <?php endif; ?>

</section>