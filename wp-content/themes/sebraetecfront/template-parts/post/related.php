<section class="post-related">
    <?php
        $cats =  get_the_category();
        $cat = $cats[0];
    ?>
    <?php $category_id = get_cat_ID($cat->slug); ?>
    <?php $relatedPosts = get_posts(array(
        'post_type' => 'post',
        'numberposts' => 3,
        'orderby' => 'rand',
        'category' => $category_id 
      ))
    ?>

    <?php
      $date_template = 'j \d\e F \d\e Y';

      if(get_locale() == "en_US"){
          $date_template = 'F j, Y';
      }
    ?>

    <div class="container pb-5 custom-position">
      <div class="size-26 color-blue post-related-title mb-4">Postagens Relacionadas</div>
      <?php /*div class="row"*/ ?>
      <div id="post-related-carousel" class="owl-carousel owl-theme">
          <?php foreach($relatedPosts as $item): ?>
            
            <?php //div class="col-12 col-lg-4 mb-4" ?>
            <div class="item">
                <div class="post-loop-card d-flex flex-column">

                  <a class="post-loop-card-img animated-thumb" href="<?= get_post_permalink($item->ID)?>">
                    <img class="img-fluid related-thumb" src="<?= get_the_post_thumbnail_url($item->ID) ?>"/>
                  </a>
                  
                  <div class="py-4 post-loop-card-content">

                    <a class="d-block size-18 avenir-bold color-gray mb-3" href="<?= get_post_permalink($item->ID)?>">
                        <?= $item->post_title?>
                    </a>

                    <p class="size-14 color-gray"><?= get_the_excerpt($item->ID); ?></p>

                    <div class="d-flex align-items-center justify-content-center mt-3">
                      <a href="<?= get_post_permalink($item->ID)?>" class="btn st-product-add-to-cart">SAIBA MAIS!</a>
                    </div>
                  </div>
                    
                </div>
            </div>
            <?php //div ?>

          <?php endforeach; ?>
      </div>
    </div>

</section>