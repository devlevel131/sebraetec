<section class="related-product">

    <div class="container pb-5 custom-position">
      <div class="size-26 color-blue post-related-title mb-4">
        Conheça estas soluções do Sebraetec para a sua empresa
      </div>

      <div id="post-related-carousel" class="owl-carousel owl-theme">

        <?php $posts = get_field('cases_products'); if( $posts ): ?>
        <?php foreach( $posts as $post):?>
        <?php setup_postdata($post); ?>
          
        <?php $ppl = get_the_permalink($post); ?>
        <?php if(!strpos($ppl, 'post_type')):	?>
          <div class="item">
            <div class="st-product pb-4">						
                <?php wc_get_template_part( 'content', 'product' );?>										
            </div>	
          </div>
        <?php endif; ?>

        <?php endforeach; ?>
        <?php wp_reset_postdata();?>
        <?php endif; ?>	

      </div>
    </div>

</section>