<div class="post-shared d-flex align-items-center justify-content-end mb-4">

<?php 

  $linkedin_url = 'https://www.linkedin.com/sharing/share-offsite/?url='.get_permalink();
  $facebook_url = 'https://www.facebook.com/sharer/sharer.php?u='.get_permalink();
  $twitter_url = 'http://www.twitter.com/share?url='.get_permalink();
  $main_url = get_permalink();

?>

<span class="pe-3">Compartilhe: </span>

<a class="post-social-icon" href="<?= $facebook_url ?>" target="_blank">
  <span class="icon-facebook"></span>
</a>
          
<a class="post-social-icon" href="<?= $twitter_url ?>" target="_blank">
  <span class="icon-twitter"></span>
</a>

<a class="post-social-icon" href="<?= $linkedin_url ?>" target="_blank">
  <span class="icon-linkedin-2"></span>
</a>

<a class="post-social-icon" id="shareButton" href="#">
  <span class="icon-share-2"></span>
</a>

<script>

  var shareButton = document.getElementById('shareButton');
  shareButton.addEventListener('click', event => {
      if (navigator.share) {
          navigator.share({
                  title: '<?= the_title() ?>',
                  url: '<?= $main_url ?>'
              }).then(() => {
                  console.log('Obrigado por compartilhar!');
              })
              .catch(console.error);
      } else {
          console.log('Compartilhamento não suportado pelo navegador');
      }
  });

</script>

</div>