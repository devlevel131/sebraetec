<section id="video-section-2" class="pb-lg-5 scroll-opacity">

    <div class="container">

        <div class="mb-5 w-100 d-flex justify-content-center">
            <div class="video-container">
                <?php echo get_field('video_cases_de_sucesso_home');?>
            </div>
        </div>

        <div class="text-center">

            <div class="size-16 color-gray mb-5"><?php echo get_field('texto_cases_de_sucesso_home');?></div>

            <a href="<?php echo get_field('link_cases_de_sucesso_home');?>" id="btn-video-section-2" class="btn btn-blue btn-video-section avenir-light">Cases de sucesso</a>
        </div>

    </div>

    <div class="division d-block d-lg-none mt-5"></div>

</section>