<section id="home-products" class="scroll-opacity">

    <div class="container">

        <div class="avenir-black color-gray size-24 mb-2"><?php echo get_field('titulo_produtos_destacados_home'); ?></div>

        <!--DESKTOP-->
        <div class="d-none d-lg-block">
			<div class="row">
				
				<?php $posts = get_field('produtos_destacados_home'); if( $posts ): ?>
				<?php foreach( $posts as $post):?>
				<?php setup_postdata($post); ?>
					
				<?php 
				$ppl = get_the_permalink($post);
				if(!strpos($ppl, 'post_type')):	?>
				<div class="col-3">
					<div class="st-product pb-4">

					<?php wc_get_template_part( 'content', 'product' );?>
					</div>
				</div>
				<?php endif; ?>

				<?php endforeach; ?>
				<?php wp_reset_postdata();?>
				<?php endif; ?>	
					
			</div>
        </div>
        <!--DESKTOP END-->

        <!--MOBILE-->
        <div class="d-block d-lg-none">

        <div id="home-products-mob" class="product-carousel owl-carousel owl-theme">

        <?php $posts = get_field('produtos_destacados_home'); if( $posts ): ?>
		<?php foreach( $posts as $post):?>
		<?php setup_postdata($post); ?>

			<?php 
			$ppl = get_the_permalink($post);
			if(!strpos($ppl, 'post_type')):	?>
			<div class="item">			
				<div class="st-product pb-4">

					<?php wc_get_template_part( 'content', 'product' );?>
				
				</div>
			</div>
			<?php endif; ?>

		<?php endforeach; ?>
		<?php wp_reset_postdata();?>
		<?php endif; ?>	

        </div>

        </div>
        <!--MOBILE END-->

        <div class="mb-4 mb-lg-5 mt-1 mt-lg-0 d-flex align-items-center justify-content-center">

            <a id="btn-solutions" href="<?php echo get_home_url() . '/'; ?>produtos" class="btn btn-yellow d-none d-lg-block">Ver todas as soluções</a>

            <a class="size-18 avenir-medium color-blue d-block d-lg-none" href="<?php echo get_home_url() . '/'; ?>produtos">Ver todas as soluções ></a>
        </div>


    </div>

    <div class="division d-block d-lg-none"></div>

</section>