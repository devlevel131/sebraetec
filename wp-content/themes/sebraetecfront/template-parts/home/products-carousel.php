<?php
if( have_rows('produtos_menor_destaque_home') ):
while ( have_rows('produtos_menor_destaque_home') ) : the_row();?>
<section class="carousel-section scroll-opacity">    
	<div class="container pt-5 pt-lg-0">			
		<div class="avenir-black color-gray mb-1 size-24">			
			<?php echo get_sub_field( "titulo_produtos_menor_destaque_home" ); ?>		
		</div>				
		<div class="product-carousel owl-carousel owl-theme">					
			<?php $posts = get_sub_field('produtos_produtos_menor_destaque_home'); if( $posts ): ?>			
			<?php foreach( $posts as $post):?>			
			<?php setup_postdata($post); ?>			

			<?php 
			$ppl = get_the_permalink($post);
			if(!strpos($ppl, 'post_type')):	?>
	
			<div class="item">								
				<div class="st-product pb-4">						
					<?php wc_get_template_part( 'content', 'product' );?>										
				</div>				
			</div>			

			<?php endif; ?>		

			<?php endforeach; ?>			
			<?php wp_reset_postdata();?>			
			<?php endif; ?>			
		</div>		    
	</div>    
	<div class="division d-block d-lg-none"></div>
</section>
<?php
endwhile;
endif;
wp_reset_query();?>	
