<section id="ebook-section" class="scroll-opacity" style="background:url(<?php echo get_field('background_baixe_ebook_home');?>) top center no-repeat;background-size: cover;">

    <div class="container py-5">

        <div class="row">

            <div class="col-12 col-lg-6 position-relative d-flex justify-content-center">
                
                <img id="ebook-img" src="<?php echo get_field('imagem_baixe_ebook_home');?>" class="img-fluid d-block"/>

                <div id="ebook-info" class="text-center d-none d-lg-block">
                    <h1 class="avenir-black size-20 size-xl-25 color-white"><?php echo get_field('titulo_baixe_ebook_home');?></h1>
                    <h3 class="color-white size-11 size-xl-14"><?php echo get_field('texto_baixe_ebook_home');?></h3>
                </div>

            </div>

            <div class="col-12 col-lg-6 d-flex flex-column text-center align-items-center justify-content-center">
				
				<div class="ebook-title text-center color-white size-lg-20 lh-1 text-uppercase mb-4">
                        <?php echo get_field('titulo_formulario_baixe_ebook_home');?>
                </div>
					
				<?php echo do_shortcode( '[contact-form-7 id="1604" title="Ebook" html_id="ebook-form" html_class="pt-3 pt-lg-0"]' );?>
				<script>
				/* Pega url */
				jQuery(document).ready(function(){
					var redirect = "<?php echo get_field('link_baixe_ebook_home');?>";
					jQuery('input.wpcf7-form-control.wpcf7-hidden.form-redirect').attr("value",redirect) ; 
				}); 	
				</script>
                <div class="color-white pt-5 d-block d-lg-none px-0 px-sm-5"><?php echo get_field('frase_abaixo_baixe_ebook_home');?></div>

            </div>

        </div>

    </div>

    <div class="division d-block d-lg-none mb-5"></div>

</section>