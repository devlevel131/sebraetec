<section id="video-section-1" class="pt-5 pt-lg-0 scroll-opacity">

    <div class="container pb-5">
        <div class="row">
            <div class="col-12">
                <div class="sobre-sebrae-title color-gray avenir-light size-28 lh-1 d-block d-lg-none mb-3 text-center">
					<?php echo get_field('titulo_como_sebrae_home');?>
                </div>
                <div class="color-gray mb-3 d-block d-lg-none text-center">
                    <?php echo get_field('texto_como_sebrae_home');?>
                </div>
            </div>

            <div class="col-12 col-lg-6 d-flex justify-content-end align-items-center">
                <div class="video-container">									<?php echo get_field('video_como_sebrae_home');?>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex px-lg-5 align-items-center flex-column justify-content-center">

                <div class="sobre-sebrae-title color-gray avenir-light size-28 lh-1 d-none d-lg-block">									<?php echo get_field('titulo_como_sebrae_home');?>
                </div>

                <div class="color-gray mt-5 d-none d-lg-block">
                    <?php echo get_field('texto_como_sebrae_home');?>
                </div>

                <div class="text-center mt-5">
                    <a href="<?php echo get_field('link_como_sebrae_home');?>" id="btn-video-section-1" class="btn btn-blue avenir-light">O que é SEBRAETEC</a>
                </div>
                
            </div>
        </div>
    </div>

    <div class="division d-block d-lg-none"></div>

</section>