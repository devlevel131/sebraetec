<?php if(get_field( "banner_home" )): ?>
<section id="top-banner">

    <div class="container my-4">
	
		<?php 
			$banner = get_field('banner_home');
			$banner_mobile = get_field('banner_mobile_home');
			if(wp_is_mobile() && $banner_mobile != '' ){
				$banner = $banner_mobile;
			}
		?>
		<?php if(get_field( "link_banner_home" )): ?>
		<a href="<?php echo get_field('link_banner_home'); ?>">
			<img src="<?php echo $banner; ?>" class="img-fluid d-block"/>
		</a>
		<?php else:?>
        <img src="<?php echo $banner; ?>" class="img-fluid d-block"/>
		<?php endif;?>
		
    </div>

</section>
<?php endif;?>