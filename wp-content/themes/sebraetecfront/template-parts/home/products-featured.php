<section id="top-products" class="mb-5 scroll-opacity">

    <div class="container py-4">

        <div class="color-yellow avenir-black size-24 mb-4 text-center text-lg-left">
            <span class="icon-star-full"></span> TOP 10 PRODUTOS MAIS VENDIDOS
        </div>

    
        <div class="px-5">
			<div id="top-products-carousel" class="owl-carousel owl-theme">
				<?php $args = array('posts_per_page' => 10, 'post_type' => 'product', 'product_cat' => 'top-10-mais-vendidos', );				
				query_posts($args); ?>								
				<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>								
				<?php global $product;?>				
				<div class="item">					
					<div class="top-product color-white">											

						<?php
						$cat_id = get_post_meta( $post->ID, 'epc_primary_' . 'product_cat', true );
						if($cat_id){
							$category_detail = get_term_by('id', $cat_id, 'product_cat');
							$category_link = get_category_link( $cat_id );
							echo '<a class="top-product-tag mb-1" href="'.$category_link.'">'.$category_detail->name.'</a>';
						} else{
							$count = 1; 
							$terms = get_the_terms($post->ID, 'product_cat' );
							if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
								foreach ($terms as $term) {
									if($term->slug != 'top-10-mais-vendidos'){
										if($count == 1){
											echo '<a class="top-product-tag mb-1" href="'.get_term_link($term->slug, 'product_cat').'">'.$term->name.'</a>';
										}
										$count++; 
									}
								}
							}
						}
						
						?>						
						
						<a class="animated-thumb mb-3" href="<?php echo get_permalink( $post->ID );?>">						   
						<?php echo get_the_post_thumbnail( $post->ID, 'product-featured-size' ); ?>						
						</a>

						<div class="top-product-name mb-3 text-center size-20"><?php the_title(); ?></div>
						
						<div class="top-product-phrase text-center mb-3 size-18 lh-1"><?php echo get_field('phrase', $post->ID);?></div>
				
						<?php if ( $price_html = $product->get_price_html() ) : ?>							
						<span class="price"><?php echo $price_html; ?></span>						
						<?php endif; ?>			
						
						<div class="avenir-medium size-13 mb-3">Divida sem juros no cartão</div>						
						
						<div class="text-center pb-1">						
						<?php									
						if( has_term( 'orcamento', 'tipos-de-produtos', $post ) ){							
							echo '<a class="btn-top-product btn btn-yellow" href="'.get_permalink( $post->ID ).'">SOLICITAR PROPOSTA</a>';						
						}else{							
							echo '<a class="btn-top-product btn btn-yellow" href="'.get_permalink( $post->ID ).'">COMPRAR</a>';						
						}?>								
						</div>					
					</div>				
				</div>								
				<?php endwhile; ?>								
				<?php endif; wp_reset_query();?>				
			</div>
        </div>
    
    </div>

</section>