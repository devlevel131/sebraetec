<?php $args = array('posts_per_page' => 3, 'post_type' => 'post' );				
query_posts($args); ?>								
<?php if ( have_posts() ) : ?>
<section id="other-contents" class="scroll-opacity">

    <div class="container mb-5 pt-5 pt-lg-0">

        <div class="avenir-black color-gray mb-1 size-24"><?php echo get_field('titulo_conteudos_home');?></div>
        <div class="color-gray mb-4"><?php echo get_field('subtitulo_conteudos_home');?></div>

        <div class="row">

			<?php while (have_posts()) : the_post(); ?>

            <div class="col-12 col-lg-4">

                <div class="mb-4 d-flex flex-column align-items-center">

                    <a href="<?php echo get_permalink( $post->ID );?>" class="d-block mb-2 animated-thumb">
						<?php echo get_the_post_thumbnail( $post->ID, 'blog-size' ); ?>
                    </a>

                    <div class="size-24 color-gray text-center mb-2 lh-1">
                        <a class="color-gray" href="<?php echo get_permalink( $post->ID );?>">
                            <?php the_title(); ?>
                        </a>
                    </div>

                    <div class="color-gray text-center size-13"><?php echo get_the_excerpt(); ?></div>

                </div>
                
            </div>
			
			<?php endwhile; ?>								
			

            <div class="text-center">
                <a id="btn-other-contents" href="https://loja.devlevel131.com.br/conteudo-principal/" class="btn btn-yellow avenir-black">VER MAIS CONTEÚDOS</a>
            </div>

        </div>

    </div>

    <div class="division d-block d-lg-none mt-5"></div>

</section>
<?php endif; wp_reset_query();?>	