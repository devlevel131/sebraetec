<section id="dinamic-banner">

    <!--DESKTOP-->
    <div class="d-none d-lg-block">

    <?php if( have_rows('slide_home', 59) ): ?>
		<div id="dinamic-banner-carousel" class="owl-carousel owl-theme">
		<?php while ( have_rows('slide_home', 59) ) : the_row();	?>		
			<div class="item">

				<?php if(get_sub_field( "link_slide_home" )): ?>	
				<a href="<?php echo get_sub_field('link_slide_home'); ?>">
					<img src="<?php echo get_sub_field('background_slide_home'); ?>" class="img-fluid"/>
				</a>
				<?php else:?>
				
                <img src="<?php echo get_sub_field('background_slide_home'); ?>" class="img-fluid"/>
				
				<?php endif;?>

            </div>
		<?php 
		endwhile;
		?>
		</div>
		<?php
		endif;
		wp_reset_query();
		?>	

        <div id="dinamic-banner-nav">
            <div class="container d-flex justify-content-between">
                <button type="button" class="owl-prev">
                    <div class="arrow-left">
                </button>
                <button type="button" class="owl-next">
                    <div class="arrow-right">
                </button>
            </div>
        </div>

    </div>
    <!--DESKTOP END-->

    <!--MOBILE-->
    <div class="d-block d-lg-none">
        <?php if( have_rows('slide_home', 59) ): ?>
		<div id="dinamic-banner-carousel-mob" class="owl-carousel owl-theme">
		<?php while ( have_rows('slide_home', 59) ) : the_row();	?>		
			<div class="item">

				<?php if(get_field( "link_slide_home" )): ?>	
				<a href="<?php echo get_sub_field('link_slide_home'); ?>">
					<img src="<?php echo get_sub_field('background_mobile_slide_home'); ?>" class="img-fluid"/>
				</a>
				<?php else:?>
				
                <img src="<?php echo get_sub_field('background_mobile_slide_home'); ?>" class="img-fluid"/>
				
				<?php endif;?>

            </div>
		<?php 
		endwhile;
		?>
		</div>
		<?php
		endif;
		wp_reset_query();
		?>	
    </div>
    <!--MOBILE END-->

</section>