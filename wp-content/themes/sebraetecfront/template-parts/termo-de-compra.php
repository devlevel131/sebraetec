<?php
defined( 'ABSPATH' ) || exit;
?>

<?php 
	$amount = WC()->cart->get_cart_total();

	/*arrays com shortcodes e variaveis que as subistituirão, respectivamente*/
	$shortcode =  array("[termo_total]", '[termo_razao]');
	$variaveis =  array($amount, '<span class="term-razao">#razao_social#</span>');

	/*id da pagina de configuração dos termos*/
	$term_page = 2140;
?>

<div>
    <table>
        <tbody>
            <tr>
                <td>
                    <p>EMPRESA DEMANDANTE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CNPJ ou DAP/NIRF: </strong> <span id="term-cnpj">#cnpj_equivalente#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Razão Social: </strong> <span class="term-razao">#razao_social#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Telefone:</strong> <span id="term-telefone">#telefone#</span></p>
                </td>
                <td>
                    <p><strong>Celular</strong>: <span id="term-celular">#celular#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Nome do Responsável:</strong> <span id="term-nome">#nome_responsavel#</span></p>
                </td>
                <td>
                    <p><strong>CPF:</strong> <span id="term-cpf">#cpf#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>E-mail:</strong> <span id="term-email">#email#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Endereço:</strong> <span id="term-endereco">#endereco#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CEP:</strong> <span id="term-cep">#cep#</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>DADOS DO EVENTO</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Produto(s):</strong> 
					<?php					
					$items_cart = WC()->cart->get_cart();
					foreach ( $items_cart as $cart_item_key => $cart_item ) {
						$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							?>
							
							<?php 
                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ) . ' - '; 
                        
                                echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );

                                $meta = get_post_meta($_product->get_id());

                                if(isset($meta['term-type'])){
                                    echo ' (termo de adesão ' . $meta['term-type'][0] . ')';
                                }

                                if ($cart_item_key !== array_key_last($items_cart)) {
                                    echo ' / '; 
                                }
							?>
                   
							
							<?php
						}
					}
					?>
					</p>
                </td>
				
            </tr>
            <tr>
                <td>
                    <p><strong>Data Prevista de Início:</strong> <span id="term-data">12 dias</span></p>
                </td>
                <td>
                    <p><strong>Data Prevista de Término: </strong>Tabela - Anexo I</p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>

		<!-- clausuras -->
		<?php 
			$content = get_field('termo_adesao', $term_page); 
			if ($content){
				$content = str_replace($shortcode, $variaveis, $content);
				echo $content;
			}
		?>
		<!-- clausuras -->

		<!--assinaturas-->
    <table>
        <tbody>
            <tr>
                <td>
                    <p>Salvador - BA, <span id="term-data-atual">#Data#</span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table style="text-align:center;width:100%;">
        <tbody>
						<?php $signatures = get_field('termo_assinatura', $term_page); ?>

						<?php 
							function imageEncodeURL($path)
							{
								$image = file_get_contents($path);
								$finfo = new finfo(FILEINFO_MIME_TYPE);
								$type  = $finfo->buffer($image);
								return "data:".$type.";charset=utf-8;base64,".base64_encode($image);
							}
						?>

            <tr>
								<?php foreach ($signatures as $key => $signature): ?>
                <td>
                    <p>
											<?php if($signature['termo_assinatura_img']): ?>
												<img src="<?= imageEncodeURL($signature['termo_assinatura_img']);  ?>" class="img-fluid"/>
											<?php endif; ?>
										</p>
                </td>

								<?php if($key == 0): ?>
                <td>
                    <p></p>
                </td>
								<?php endif;?>
   
								<?php endforeach; ?>
            </tr>
            <tr>
								<?php foreach ($signatures as $key => $signature): ?>
								
                <td>
                    <p>
                        <?php echo $signature['termo_assinatura_nome']; ?><br />
                        <?php echo $signature['termo_assinatura_cargo']; ?>
                    </p>
                </td>

								<?php if($key == 0): ?>
                <td>
                    <p></p>
                </td>
								<?php endif; ?>
  
								<?php endforeach; ?>
            </tr>
        </tbody>
    </table>
    <p></p>
		<p></p>
		<!--assinaturas end-->

		<!--anexo-->
    <?php echo get_field('termo_anexo', $term_page); ?>
		<!--anexo-->
 
</div>
