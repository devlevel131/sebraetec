<!doctype HTML>
<html lang="pt-br">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="<?= THEME_IMG ?>favicon.ico">

    <link rel="stylesheet" href="<?= THEME_ASSETS ?>bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= THEME_ASSETS ?>icomoon/style.css"/>
    <link rel="stylesheet" href="<?= THEME_ASSETS ?>owlcarousel/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="<?= THEME_ASSETS ?>owlcarousel/assets/owl.theme.default.min.css"/>
    <link rel="stylesheet" href="<?= THEME_CSS ?>fonts.css"/>
    <link rel="stylesheet" href="<?= THEME_CSS ?>main.css"/>


    <?php wp_head();?>

</head>

<body <?php body_class(); ?>>

    <header>

        
        <?php
            $login_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
            $cart_url = wc_get_cart_url();
            $cart_count = WC()->cart->get_cart_contents_count();
        ?>

        <div class="bg-blue py-4">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-lg-3 d-flex align-items-center">

                        <div id="mobile-menu-button" href="" class="d-block d-lg-none me-3 mt-2"><span class="icon-menu color-white size-30 size-lg-34"></div>

                        <a id="logo" href="<?php echo get_home_url(); ?>">
                            <img src="<?= THEME_IMG ?>sebrae_logo.png" class="img-fluid"/>
                        </a>

                    </div>

                    <div class="col-lg-5 d-none d-lg-flex align-items-center justify-content-center">
                        
                        <?php 
                            $search_query = "";

                            if (isset($_GET['post_type'])) {

                                $post_type = $_GET['post_type'];

                                if($post_type == "product"){
                                    $search_query = get_search_query();
                                } 
                            } 
                        ?>

                        

                        <form id="header-search" action="<?php echo get_home_url() . '/'; ?>" method="get" class="header-search d-flex align-items-center justify-content-between" role="search">
                            <input id="s" name="s" type="text" placeholder="O que você procura?" value="<?= $search_query ?>">
                            <button type="submit" form="header-search"><span class="icon-search-2 color-white size-25"></span></button>
							<input type="hidden" name="post_type" value="product" />
                        </form>
						
                    </div>

                    <div class="col-6 col-lg-4 d-flex align-items-center justify-content-end justify-content-lg-between ">

                        <div id="header-user" class="d-flex align-items-center justify-content-end justify-content-lg-between me-3 me-lg-0">


                            <a href="<?= $login_url ?>" id="header-user-icon">
                                <span class="icon-user color-white size-18 size-lg-25"></span>
                            </a>

                            <div class="ps-3 color-white d-none d-lg-block">

                                <?php 
								$current_user = wp_get_current_user();
								if(is_user_logged_in()):?>

                                    <a class="color-white a-line" href="<?= $login_url ?>">Olá, <strong class="avenir-black"><?php echo esc_html( $current_user->display_name )?></strong><br/>
                                acessar <strong class="avenir-black">conta</strong></a>
                
                                <?php else: ?>

                                    <a class="color-white a-line" href="<?= $login_url ?>">Olá, faça seu <strong class="avenir-black">login</strong><br/>
                                ou <strong class="avenir-black">cadastre-se</strong></a>

                                <?php endif; ?>

                            </div>

                        </div>

                        <div id="header-cart">
                            <a class="cart" href="<?= $cart_url ?>">
                                <div class="cart-count size-12 size-lg-16"><?= $cart_count ?></div>
                                <span class="icon-shopping-cart color-white size-20 size-sm-28"></span>
                            </a>
                        </div>

                    </div>

                    <div class="col-12 d-flex d-lg-none align-items-center justify-content-center mt-4">
                        
                        <form id="header-search-mob" action="<?php echo get_home_url() . '/'; ?>" method="get" class="header-search d-flex align-items-center justify-content-between" role="search">
							<input id="s" name="s" type="text" placeholder="O que você procura?" value="<?php the_search_query(); ?>">
                            <button type="submit" form="header-search-mob"><span class="icon-search-2 color-white size-25"></span></button>
							<input type="hidden" name="post_type" value="product" />
                        </form>

                    </div>
         
                </div>
            </div>
        </div>

    </header>

    <section id="menus" class="py-4">

        <div class="container">

            <div class="row">

                <div class="col-3 d-none d-lg-block">
          
                    <div id="menu-category" class="menu-drop">

                        <span class="icon-menu"></span>

                        <span>Todas as Categorias</span>

                        <span class="icon-nav-bottom"></span>

                        <div class="menu-drop-submenu">
                            <?php
                                wp_nav_menu(
                                    array(
                                        'theme_location' => 'category-menu',
                                        'container'      => false,
                                        'fallback_cb'    => false
                                    )
                                );
                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-lg-6 d-flex align-items-center">
                    <div id="menu-list" class="w-100 size-03vw size-sm-16 size-lg-14 size-xxl-16">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'top-list-menu',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-3 d-none d-lg-flex justify-content-end">
                
                <div id="menu-sector" class="menu-drop">

                    <span class="icon-menu"></span>

                    <span>Selecione o setor</span>

                    <span class="icon-nav-bottom"></span>

                    <div class="menu-drop-submenu">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'sector-menu',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>
                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--MENU MOBILE-->
    <nav id="menu-mobile" class="d-none menu-mobile">

        <div class="bg-blue-2">
            <div class="container py-4">
                <div class="row">

                    <div class="col-6 px-3 px-sm-4 d-flex align-items-center py-3">
                        <div class="color-white size-24 lh-1">
                            Selecione<br/> a Categoria
                        </div>
                    </div>

                    <div class="col-6 px-3 px-sm-4 d-flex align-items-center py-3">
                        
                        <div id="mobile-menu-close">
                            X
                        </div>
                        <div class="color-white size-24 lh-1">
                            Selecione<br/> o Setor
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container py-4 pb-4">

            <div class="row">

                <div class="col-6 px-3 px-sm-4">
                
                    <div class="menu-mobile">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'category-menu',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>
            
                </div>

                <div class="col-6 px-3 px-sm-4">
                
                <div class="menu-mobile">
                    <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'category-menu',
                                'container'      => false,
                                'fallback_cb'    => false
                            )
                        );
                    ?>
                </div>
        
            </div>

            </div>

        </div>

    </nav>
    <!--MENU MOBILE-->

