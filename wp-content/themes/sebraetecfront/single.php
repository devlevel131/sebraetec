<?php get_header() ?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php //$date_template = 'j \d\e F \d\e Y'; ?>

<?php get_template_part('template-parts/post/banner') ?>

<section class="post-content pt-3">

  <article class="color-gray container pb-4 default-lists">
  
    <?php /*date class="color-gray size-16 gotham-bold">
      <?php echo get_the_date($date_template); ?>
    </date*/ ?>

    <div class="d-flex justify-content-end"> <a href="<?php echo get_home_url() . '/conteudo-principal'; ?>"> < Voltar</a></div>
    <h3 class="size-30 avenir-light color-gray mb-5 d-none d-lg-block">Conteúdos para empreender com inovação</h3>

    <h1 class="size-36 overflow-x-hidden avenir-medium color-blue d-block mb-2">
      <?= the_title()?>
    </h1>

    <?php get_template_part('template-parts/post/share') ?>

    <?php if(!empty(get_the_post_thumbnail_url())): ?>

      <div class="text-center post-thumbnail mb-4">
        <img src="<?= get_the_post_thumbnail_url()?>" class="d-inline-block img-fluid"/>
      </div>

    <?php endif; ?>
  
    <?= the_content() ?>

  </article>

</section>

<?php endwhile; ?>

<?php else: ?>

<?php endif; ?>

<?php get_template_part('template-parts/post/related') ?>

<?php get_footer() ?>