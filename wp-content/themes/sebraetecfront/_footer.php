<footer>

<div class="bg-blue-2 d-none d-lg-block">
    <div class="container py-4">
        <div class="row">

            <div class="col-12 col-lg-6">

                <span class="text-uppercase color-white d-block mb-5 avenir-black">Formas de pagamento</span>

                <div class="d-flex align-items-center justify-content-end pt-3">
                    <div id="payments" class="">
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>
                        <div class="me-2 mb-2 d-inline-block"><img src="<?= THEME_IMG ?>creditcard.jpg"/></div>

                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6">

                <div class="pt-5 px-5">

                    <div class="text-center avenir-black color-white mb-3">Receba ofertas com preços exclusivos.</div>

                    <form id="newsletter">
                        <input type="text" class="form-control mb-3 text-center rounded" id="newsletter_name" placeholder="Digite seu nome" >
                        <input type="email" class="form-control mb-3 text-center rounded" id="newsletter_email" placeholder="Digite seu e-mail">

                        <div class="mb-3 form-check d-flex align-items-center justify-content-center">
                            <input type="checkbox" class="form-check-input mt-0" id="newsletter_check">
                            <label class="form-check-label color-white size-13 ps-2" for="newslettercheck">Eu concordo com a Política de Privacidade do Sebrae Bahia</label>
                        </div>

                        <div class="text-center">
                            <input type="button" class="btn btn-form btn-yellow avenir-black" value="Enviar"/>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </div>
</div>
<div class="bg-blue-2 bg-lg-blue pt-4 pb-4 pb-lg-0">
    <div class="container">
        <div class="row pt-5">

            <div class="col-12 col-lg-2 text-center text-lg-end px-5 px-lg-0 mb-5">
                <a id="logo-footer" href="<?php echo get_home_url(); ?>">
                    <img src="<?= THEME_IMG ?>sebrae_logo.png" class="img-fluid"/>
                </a>
            </div>
            <div class="col-12 col-lg-7 px-lg-5">

                <div class="row">

                    <div class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center">
                        <ul>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">O que é sebraetec</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Casos de sucesso</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Conteudos</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Contatoc</a>
                            </li>
                        </ul>
                    </div>

                    
                    <div  class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center">
                        <ul>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Categoria</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Setor</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">TOP 10</a>
                            </li>
                        </ul>
                    </div>

                    
                    <div class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center mt-4 mt-lg-0">
                        <ul>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Notícias</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">StartUP BA</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Seja um prestador de serviço</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Procure o sebrae</a>
                            </li>
                            <li class="d-block mb-3">
                                <a class="text-white a-line" href="#">Política de privacidade</a>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
            <div class="col-12 col-lg-3 text-white text-center d-none d-lg-block">
                <div class="mb-1 mt-4 mt-lg-0 mb-lg-3">
                    <span class="icon-sebrae size-60"></span>
                </div>
                <div id="footer-social-icons">
                    <ul class="d-flex align-items-center justify-content-center justify-content-lg-between">
                        <li class="pe-2 pe-lg-0">
                            <a class="footer-social-icon size-14" href="">
                                <span class="icon-facebook"></span>
                            </a>
                        </li>
                        <li class="pe-2 pe-lg-0">
                            <a class="footer-social-icon size-14" href="">
                                <span class="icon-twitter"></span>
                            </a>
                        </li>
                        <li class="pe-2 pe-lg-0">
                            <a class="footer-social-icon size-14" href="">
                                <span class="icon-youtube"></span>
                            </a>
                        </li>
                        <li class="pe-2 pe-lg-0">
                            <a class="footer-social-icon size-14" href="">
                                <span class="icon-instagram"></span>
                            </a>
                        </li>
                        <li class="pe-2 pe-lg-0">
                            <a class="footer-social-icon size-14" href="">
                                <span class="icon-linkedin-2"></span>
                            </a>
                        </li>
                        <li>
                            <a class="footer-social-icon-whatsapp size-50 color-white" href="">
                                <span class="icon-whatsapp"></span>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="text-center size-20 size-lg-13">
                    <span>WhatsApp: (71) 98159-8695</span><br/>
                    <span>(de segunda a sexta-feira, das 08h30 ás 17h30)</span>
                </div>
            </div>

            <div class="col-12 text-center py-5 size-11 size-lg-13 color-white d-none d-lg-block">
                2021 - Servio de apoio as micro e pequenas empresas da bahia. Central de relacionamento Sebrae: 0800 000 00
            </div>

        </div>
    </div>
</div>

<div class="bg-blue pt-5 d-block d-lg-none">
<div class="container">

    <div class="text-white text-center">

        <div class="">
            <span class="icon-sebrae size-60"></span>
        </div>

        <div id="footer-social-icons">
            <ul class="d-flex align-items-center justify-content-center justify-content-lg-between">
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="">
                        <span class="icon-facebook"></span>
                    </a>
                </li>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="">
                        <span class="icon-twitter"></span>
                    </a>
                </li>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="">
                        <span class="icon-youtube"></span>
                    </a>
                </li>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="">
                        <span class="icon-instagram"></span>
                    </a>
                </li>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="">
                        <span class="icon-linkedin-2"></span>
                    </a>
                </li>
                <li>
                    <a class="footer-social-icon-whatsapp size-50 color-white" href="">
                        <span class="icon-whatsapp"></span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="text-center size-20 size-lg-13">
            <span>WhatsApp: (71) 98159-8695</span><br/>
            <span>(de segunda a sexta-feira, das 08h30 ás 17h30)</span>
        </div>

    </div>

    <div class="col-12 text-center py-5 size-11 size-lg-13 color-white">
        2021 - Servio de apoio as micro e pequenas empresas da bahia. Central de relacionamento Sebrae: 0800 000 00
    </div>

</div>
</div>

</footer>

<script src="<?= THEME_ASSETS ?>jquery/jquery-3.6.0.min.js"></script>
<script src="<?= THEME_ASSETS ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?= THEME_JS ?>main.js"></script>
<?php wp_footer()?>

</body>



</html>