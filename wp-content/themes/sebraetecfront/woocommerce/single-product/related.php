<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

	<section class="related products">

		

		<?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Related products', 'woocommerce' ) );

		if ( $heading ) :
			?>
			<h2 class="size-16 color-blue mb-5 text-uppercase"><?php echo esc_html( $heading ); ?></h2>
		<?php endif; ?>

		<?php 
			//armenguei por falta de tempo pra pesquisar uma solução mais correta devido ao prazo
			//mas vou ao menos deixar explicado pro dev do futuro
			//o objetivo era pegar por categoria com exeção do top-10-mais-vendidos
			//ha um filtro em functions.php aumentando o numero de resultados desse armengue. busque por 'parte da solução do related.php'
			//o arquivo original vai estar como _related.php
			//
			//pegando as categorias da pagina com exeção do top mais vendidos e inserindo em um array
			$terms = get_the_terms($post->ID, 'product_cat');
			$categories = [];
		
			foreach ($terms as $term){
				if($term->slug != 'top-10-mais-vendidos'){
					array_push($categories, $term->term_id);
				}
			}
			
		?>
		
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

					<?php
					$post_object = get_post( $related_product->get_id() );

					//pegando as categorias do produto relacionado
					$related_cat_ids = $related_product->category_ids;

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					//verificando se ao menos uma categoria do produto relacinado contem naquele array la
					foreach ($related_cat_ids as $related_cat_id){
						if(in_array($related_cat_id, $categories, TRUE)){
							wc_get_template_part( 'content', 'product' );
							break;
						}
					}

		
					?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>
	<?php
endif;

wp_reset_postdata();
