<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" id="wc-simple-form" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		/*woocommerce_quantity_input(
			array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);*/

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<div class="woocommerce-single-btns">
		
			<?php if( has_term( 'orcamento', 'tipos-de-produtos', $post ) ):?>
				<p class="single_add_to_quote_loop">
					<a class="btn btn-blue product-page-btn text-uppercase single_adq_button_loop product_type_simple" id="add_to_quote_loop" data-quantity="1" data-product-id="<?php echo esc_attr( $product->get_id() ); ?>" rel="nofollow" href="#">Finalizar orçamento</a>
				</p>
			<?php else:?>
				<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="btn btn-yellow product-page-btn btn-add-to-cart text-uppercase me-lg-3 mb-3 mb-lg-0 single_add_to_cart_button">Adicionar ao carrinho</button>
			
				<button id="wc-simple-finish" class="btn btn-blue product-page-btn text-uppercase pisol_single_buy_now pisol_buy_now_button pisol_type_simple" type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">Finalizar compra</button>
			<?php endif;?>
										
		</div>
		
		
		<?php //do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
