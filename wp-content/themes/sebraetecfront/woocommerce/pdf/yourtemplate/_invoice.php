<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<div>
    <p><strong>TERMO DE ADESÃO E COMPROMISSO</strong></p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p>EMPRESA DEMANDANTE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CNPJ ou equivalente: </strong> <span id="term-cnpj"><?php echo get_post_meta($this->order->id, '_billing_cnpj', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Razão Social: </strong> <span id="term-razao"><?php echo get_post_meta($this->order->id, '_billing_company', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Telefone:</strong> <span id="term-telefone"><?php $this->billing_phone(); ?></span></p>
                </td>
                <td>
                    <p><strong>Celular</strong>: <span id="term-celular"><?php echo get_post_meta($this->order->id, '_billing_cellphone', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Nome do Responsável:</strong> <span id="term-nome"><?php echo get_post_meta($this->order->id, '_billing_first_name', true).' '.get_post_meta($this->order->id, '_billing_last_name', true); ?></span></p>
                </td>
                <td>
                    <p><strong>CPF:</strong> <span id="term-cpf"><?php echo get_post_meta($this->order->id, '_billing_cpf', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>E-mail:</strong> <span id="term-email"><?php echo get_post_meta($this->order->id, '_billing_email', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Endereço:</strong> <span id="term-endereco"><?php echo get_post_meta($this->order->id, '_billing_address_1', true).' - '.get_post_meta($this->order->id, '_billing_city', true).'/'.get_post_meta($this->order->id, '_billing_state', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CEP:</strong> <span id="term-cep"><?php echo get_post_meta($this->order->id, '_billing_postcode', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>DADOS DO EVENTO</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Produto(s):</strong> 

					
					<?php
						$items = $this->get_order_items(); 
						if( sizeof( $items ) > 0 ) {
							foreach( $items as $item_id => $item ) {
								
								echo $item['name'].' - '.$item['order_price'];
								if ($item_id !== array_key_last($items)) {
									echo ' / ';
								}
			
							
							}
						}
					?>
					
					</p>
                </td>
				
            </tr>
            <tr>
                <td><?php $data = $this->order->get_date_created();?>
                    <p><strong>Data Prevista de Início: </strong><?php echo date('d/m/Y', strtotime("+12 days",strtotime($data)));?></p>
                </td>
                <td>
                    <p><strong>Data Prevista de Término: </strong>Tabela - Anexo I</p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA PRIMEIRA – DO PREÇO</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>Por este instrumento, assume(m) o compromisso de efetuar o pagamento ao SEBRAE/BA, da importância total de <?php echo $this->order->get_formatted_order_total(); ?>, pelos serviços referidos neste Termo de Adesão e Compromisso, no(s) seguinte(s) termo(s):</p>
    <p><strong>A <?php echo get_post_meta($this->order->id, '_billing_company', true); ?> a importância de <?php echo $this->order->get_formatted_order_total(); ?>.</strong></p>
    <p><strong>Parágrafo Primeiro</strong> – A contrapartida deve ser paga no Cartão de débito; Cartão de crédito, de acordo com a orientação vigente.</p>
    <p>
        <strong>Parágrafo Segundo</strong> – O não pagamento de qualquer das parcelas implicará, conforme o caso, na suspensão ou cancelamento do serviço e, a critério do SEBRAE/BA, na adoção de medidas legais cabíveis, judiciais ou extrajudiciais.
    </p>
    <p><strong>Parágrafo Terceiro</strong> – Os Responsáveis Financeiros citados nesta Cláusula Primeira compreendem que estão arcando com contrapartida de até 30% do preço do serviço em função de subsídio concedido pelo SEBRAE/BA.</p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA SEGUNDA – DAS OBRIGAÇÕES</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>I - São obrigações da <?php echo get_post_meta($this->order->id, '_billing_company', true); ?>:</p>
    <p><strong>a) </strong>Avaliar as entregas realizadas pela ENTIDADE EXECUTORA (parcial e final).</p>
    <p><strong>b) </strong>Responsabilizar-se pelo efetivo repasse da contrapartida ao SEBRAE/BA, quando a contrapartida não for de responsabilidade, exclusiva, do parceiro/responsável financeiro.</p>
    <p>
        <strong>c) </strong>Disponibilizar ao SEBRAE Nacional e/ou SEBRAE/BA, a qualquer tempo, informações sobre os serviços prestados, sobre os resultados obtidos ou sobre a ENTIDADE EXECUTORA contratada pelo SEBRAE/BA por meio do
        SEBRAETEC.
    </p>
    <p><strong>d) </strong>Cumprir as disposições previstas no Edital SEBRAETEC Bahia vigente no ato da celebração do Termo de Adesão.</p>
    <p><strong>e) </strong>Responsabilizar-se para que a utilização dos recursos na prestação de serviços não seja indevida ou desnecessária.</p>
    <p><strong>f) </strong>Receber os representantes do SEBRAE/BA com o devido agendamento prévio.</p>
    <p>
        <strong>g) </strong>Responder às pesquisas de satisfação dos serviços prestados e da efetividade do SEBRAETEC realizadas pelo SEBRAE/BA e/ou pelo SEBRAE/NA, responsabilizando-se pela veracidade, exatidão e completude das respostas.
    </p>
    <p><strong>h) </strong>Restituir ao SEBRAE/BA os valores investidos na proposta, caso haja comprovação de ação em conjunto com a ENTIDADE EXECUTORA para lesar o SEBRAE/BA.</p>
    <p><strong>i) </strong>Assinar este Termo de Adesão de Compromisso antes do início do serviço, conforme padrão definido no Sistema de contratação do SEBRAETEC.</p>
    <p>
        <strong>j) </strong>Assinar a Declaração de Conclusão do Serviço apenas após o término completo do serviço, sob pena de restituir os valores pagos pelo SEBRAE/BA para a realização do serviço, conforme padrão definido no Sistema de
        contratação do SEBRAETEC.
    </p>
    <p>
        <strong>k) </strong>Responder pelo pagamento proporcional dos serviços até então realizados em caso de solicitação de distrato da proposta por sua própria iniciativa, quando a contrapartida não for de responsabilidade, exclusiva, do
        parceiro/responsável financeiro.
    </p>
    <p>
        <strong>l) </strong>Estar enquadrada como público do SEBRAETEC: Clientes do Sistema SEBRAE com CNPJ (MEI, ME e EPP); Pessoas físicas que estejam registradas no Sistema de Informações Cadastrais do Artesanato Brasileiro - SICAB
        tenham a Carteira Nacional do Artesão ou Carteira Nacional de Trabalhador Manual, fature até R$ 4.800.000,00 (quatro milhões e oitocentos mil reais) por ano e esteja com a carteira válida no momento do atendimento; e produtores
        rurais que possuam inscrição estadual de produtor, número do Imóvel Rural na Receita Federal (NIRF) ou declaração de aptidão (DAP) ao Programa Nacional de Fortalecimento da Agricultura Familiar (Pronaf). Soma-se ao grupo de
        produtores rurais os pescadores com registro no Ministério da Agricultura, Pecuária e Abastecimento.
    </p>
    <p><strong>m) </strong>Responsabilizar-se pelas informações prestadas no momento do atendimento.</p>
    <p><strong>Parágrafo único</strong> – Após assinatura da Declaração Parcial ou de Conclusão do Serviço a Empresa Demandante não poderá contestar, sob qualquer hipótese, a revisão do serviço prestado.</p>
    <p>II – São obrigações do SEBRAE/BA:</p>
    <ol>
        <li>Contratar a Prestadora de Serviço Tecnológico - PST para executar os serviços decorrentes deste termo.</li>
        <li>Acompanhar toda a execução do serviço ora acordado, bem como subsidiar os custos nas condições estabelecidas.</li>
        <li>Efetuar, quando couber, a devolução da contrapartida paga pelo cliente, nos casos em que este pagamento ocorra antes do processo de cotação.</li>
        <li>Demais obrigações impostas pelo Edital de Cadastramento SEBRAETEC vigente.</li>
    </ol>
    <p><strong>CLÁUSULA TERCEIRA– DAS SANÇÕES</strong></p>
    <p>
        Caso se verifique participação do contratante ou da Empresa BENEFICIADA em conjunto com Prestador de Serviços Tecnológicos em ações prejudiciais ao SEBRAE/BA, estes ficarão impedidos de demandar serviços no SEBRAETEC pelo prazo de
        até dois anos, além de outras medidas judiciais cabíveis que o SEBRAE/BA entenda pertinente.
    </p>
    <p>É permitido a qualquer das partes denunciar o contrato, desde que de forma motivada.</p>
    <p><strong>CLÁUSULA QUARTA– DA MORA</strong></p>
    <p>Em caso de mora no cumprimento das obrigações, haverá incidência de multa de 2% no vencimento e 1% ao mês.</p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>CLÁUSULA QUINTA – DO FORO</strong></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>Fica estabelecido entre as partes que o foro competente para a solução de qualquer litígio decorrente deste contrato é o de Salvador - BA, excluído qualquer outro.</p>
    <p>E, por assim terem acordado, assinam este Termo de Adesão e Compromisso.</p>
    <table>
        <tbody>
            <tr>
                <td>
					<?php $date_created = $this->order->get_date_created();?>
                    <p>Salvador - BA, <?php echo $date_created->date("d/m/Y");?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table style="text-align:center;width:100%;">
        <tbody>
            <tr>
                <td>
                    <p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABjALEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9UKKKKACiio57hbeF5JG2pGu5moAkorO8P+IbDxRpcGpabcLc2cw/dyr/ABVo0vh92QBRRRTAKN21qK8r17UpLr4/eGNMk8zyIdHu72Pa23955ka/N/wFquMOd3A9UoooqACiiigAooooAKKK8w8WeLLu1+OHgjQILqSOzvLO/uLqFfuybVj8vd/49VRi5uwHp9FFFSAUUUUAPooooAZXBfE7xZrvh/8Asuy8M2VnfaxqEjLEl/IyxBVXd/DXe1w/xSgeCw0/W4Rvl0i6W5ZPWP7sn/jrVtR5ZVFGQEfw4+JkHja2ms7uH+yfEtjtTUdIm/1kDeq/3o2/haug8aSSQeE9XaL/AFn2WTb/AN81yPj74Z2fxC/s/wARaPqEmieJLMeZZaxbLu3L/wA85F/5aRt/drj9WvviZ4o02LwprHhgWl7JcRibxDpd6v2KSFZFZm2t+8Xcq/drphRp1JxnTlbun0/z/MZo/s/w/wDCN6j4o8IebI/9mta3sav/AArcQ7m2/wDAlb/vqvaB/SvEvBv/ABLf2oPGFn/DcaDZSr/teW23/wBmr26s8Yuarz97P8ACiiiuQQV4+w/tH9pxGBP/ABLfDzK3/bab5f8A0GvYK+atO8SapqnxU+I8vhyNZNZeSHS7eeVf3drHHHuaRv8AgTbVX+9XbhablztdgPpWivOPhb8ULTxJ8JdG8U6vfQweZb5up5WWNfMX5X/8eWuj8G+PNG8fWM91o101zHBL5Mm6Noyrf7rVzzpzp83NHYDpKKKKyAKKKKACvDvHUm39qj4axhfmbTNR3N/s7a9xrwzxgrN+1p4B/urot43/AKFXZhfjl/hl+Qz3OiiiuMQUUUUAPooooAZUcsKTxtHIqurDayt3qSigDz3QriTwF4ii8PXTFtFvizaZO3/LFv4rdv8A2WvQqxfE/h218VaTJZXeQrfMkqHa0bfwsv8AtVh+GfElxY6gPDmvfu9TjXFtdsv7u+j/ALyt/e/vLXTL99H2kfi6/wCf+YHFSs1r+13F8q+Vd+EW+b/aW6Wva68P8WP9h/au8EXB/wCYhoV7aL8391lkr3CqxG0Jf3RhRRRXIIjuGZbeQou9gvyqvevLP2ffAF94R8N6jqGtw+Tr2t302oXUbNu8nc3yx/8AAa9XorWNSUYOC6geY+H/AIA+EfDmrT3kVrNcxtcSXMNjczNJbW8jHc3lx/dWue+G7f2X+0X8TtNjyLa4hsr/AGn7oby1X5a9vrxHwfiP9qfx0P4m0e0b/wAerrpVZ1o1PaSv7v6oD26iiivPAKKKKACvEPEypcftbeDV/wCWlvoF3N/wFm217fXiWpQM37X2jSN9xfCE23/e+1L/APFV14f4pejGe20UUxpFjVmZtqrXIIfRWfpGuafr1n9p028t7+2zt8y2kWRf/Ha0KPhAfRRRQAyiiigArL1zw/Z+IrP7NeQiRc71YcOjf3lb+Fq1KKabi7xA+cfiBpOp+Hfjd8Jbi9vhqFtHeXdtBcyLtn2yRqu1v73+9X0b6V41+0WkllcfDrV4/wDlx8UWvmN/0zZWVq9lWurES56VOX9bjFooorkEFFFFABXhejutn+194gjb5ftnhq3Zf9rbI1e6V4lr6/Z/2r/Csir/AMfGg3au3+6y114b7cf7rA9toopr7u1cgDqKy9Dl1Ka3xqkMMVyp+9C25WrUo+EArxjxkv8AZ37Tvw+utzBdQ0nUbL5f+me2SvZ68i+Ktv5Pxe+EuonpHf3lr/38tz/8TXThv4j9JfkB67XNeLvDt74ogFnFqkul2b/LM1ov72Rf7u7+GulWiudS5XzAZXhnwzp/hLR7bTNMgW2s7ddqxrWrRRQ227sB9FFFIBlFFFABRRRQB45+1Ymz4Vrdf8+urWEzf9/1X/2avXbdvNjRl+6y/LXnH7SGnnVPgp4sjjXdLDZm5j/3o/3i/wDoNdf4Hvl1Twbod2rbvOsoX3f8BWuqXvYaPq/0A3qKKK5QCiiigArx/wAaQ7f2ifAE6/e+w3sbf+O17BXEa/4Vlv8A4meGNbjXdBYw3Mcjf3dyrtrpw8oxlLm7P8gO3ooormAKKKKACvMfjVFtk8CXvl7ls/E1o7N/dVlkj/8Aai16dWH4s8OL4o0tbSRtnl3EdwjejRyK6/8AoNaU3GE05AbifcooWiswCiiigB9FFFADKKKKACiiigDnviBZrqPgfXrd/uSWUyn/AL4asD4A3H2r4M+EZA2//iXx/M1dpq0Rn0u7hVdzyRMqr/e+WuV+D3hu78I/DnRdKvl8u7t4dsibt21t33a6OZew5fMZ21FFFc4gooooAKKKKACiiigAooooAKKKKACiiigAooooAfRRRQAyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAfRRRQB/9k=" class="img-fluid"/></p>
                </td>
                <td>
                    <p></p>
                </td>
                <td>
                    <p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABrAMIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAoorzD4oeONXj1C38FeCZLVvGupRef59yN0WlWu7a93Kv8X92Nf4m/wBlWoA2fGPxa8OeCdQi0u6uLi+1yaPzotH0i0kvr14/73kwqzKv+021f9que1D48Q+G7ZNQ8UeDPFPhfQjtzrN/Bby20Qb7rSLbzSSRL/tSRrt/i210Pw5+Gek/DWwnSz82+1a+fz9S1m8PmXeoTf3pZP0VfuqPlUV0+o2tpf6fc217HHPYzRNFNHINytGyncrf7O2gCe3uI7q3jmgkWWGRQySK25WX1qxXlP7Ma3MfwM8LrcMXRUmFuxO4/ZvtEn2b8PJ8uvVqACiiigAoorxX9qHxZ4m8N/Dm+i8Iakuh6y1rcXz6rJCsiWdrbxmSVtrKylm+WNf+un+zQB7VRXJfC+41W8+GvhSfXZGn1ybSrSS+kYKrNO0K+Y3y/wC1urraACiiigAooooAKKKKACivOPi58TpfAVrp2m6LZR65411uRoNF0ZpNnnso3SSyN/DDGvzM3+6v3mWuZ+EXxC8Z3XxR8VeBfGdzouo3um6fa6nBeaJbyQKqzFlaF0dmPysvyt/Eu1v4qAPbaKKKACiiigAooooA474r/EG0+Fnw/wBa8UXkX2hdPt90Nvn5p5m+WOJf9pnZV/4FXNfAnwXq2h+HpvEPiyCEePPEjJf61JGvELFR5VonLfu4V+XG7729v4s1yPjjzvi9+0r4e8IIGPhvwNGniHWFb7txeSKy2UP/AAH5pK+hKACuA+O/jJfh/wDB/wAXa7n99bWEi2/P3p5P3cK/9/HWu/rw39oCxHjvxj8M/ABkf7FqerNrGqRxt960sV8za/8AstM0C/8A7NAHqHw/0P8A4RfwH4d0fodP0+3tT/wCNV/pXRUUUAFFFFABXyF8VptS8ceFfGV3HeST/wDCceJtO8H6Jbbt0UVnDdbJplX/AGm+1szf3VWvePjx4uufCHwx1iXTXkXXL9V0vTPKba/2u4byYWX/AHWfd/wGuEh8L6fo/wAZPhZ4E0uNhpXgzQLjVWVh1Zl+yQM3+1/x8t/vUAe9W8KWtvHDCu2ONQqr6CpqKKACiiigAooooAK53xt4y0v4feFtR8Q61cra6ZYRNPNJuwfRVX+8zNtVV/iZhXRV4D438r42fHTTvBBBufC3g9Y9a1xSP3U983/Hnbsf9n5pWX/coA2/gt4K1XUL65+JHjKBF8Xa7Cq21l95dGsPvR2i/wC191pG/ib/AHVqr8BdO/t7xt8TviGQuzxBrC6dYlejWtiv2fzP+BSLL/3ytdd8cPFl94N+Gur3ek7f7eulXT9JRv4r2ZvLh/8AHmVv+A1t/D/whB4B8E6F4etnMsOmWkdt5jfekZV+Zv8AgTfN+NAHSUUUUAFFFFABTHZYkZm+VRyafXmn7RvjWX4e/A3xrr9uP9Kt9Nkjt+du2aT91Gf++5FoA4/9kuzfV9A8Z+O7j97ceMPEl7ewzM25vssMn2eBf91fKbb/ALO2ve64z4P+F4/Bfws8JaJEixLY6XbwbV/veWu7/wAezXZ0AFeIfC2N/G3xy+IvjVrjfY6WY/COmw7cKoh/fXMm7+LdNJt/7Z11Xxu+JQ+GPgaa9tVW78R3zrYaJp/8d5eyfLEir7feb/ZVqufB34cRfCn4e6T4cEy3d1bx+Ze323a13dN800zf7zFjQB3NFFFABRRRQB4v44gm8bftC+BtCUK+k+G7W48SX6f9PDf6PaL/AL3zTt/wGk+Dqt4p+KXxW8YORJA2pQeHrGTd/wAsbOP95t9vOml/4Etc/wDDvx1Ba6D8XvjFqg8qxkvriGxSRvlay09Whj2/9dJlnb/eeu0/Zj8I33gr4HeFbHVVZNZnga/1DzPv/abh2mk3f7W6Q0Aeq0UUUAFFFFABRRRQBynxL8fad8LvAet+K9V3fYtLtmuGjX70jfwxr/tM21fxrkv2cfBd74V+HSalrqKvirxLcSa7rDY+ZZ5vmEf/AGzj2R/8Arl/jjZyfEv4wfDf4cowbSI5JPE+uxqfvQWrItvG3+y00n3f9ivafE3iTT/CPh/Uda1SeO007T4GuLiaRtqqqruNAHlPjAah4/8A2hvC3h2OOZfDfhS3bxDqU0TDbJetuitIG/4C00m3/ZWvba8i/Z30fWJ/C9/4x8SLHHr3jG6/tiSBR/x62zKq2lv/AMBhVd3+0zV67QAUUUUAFFFFABXg/wC2hdMnwRfT1TzG1bWtLsNuOu68ib/2WveK8I/aqkk8v4U24kVY5vH2jeZG38e2bd/7LQB7vXN+NPGWi+APDl9ruv6lFpelWkZlluJ2xnH8Kj+JmIwqr8zNwK5fx98bND8G6h/YdlHceKPGTjNt4Z0RfPu2+7hpf4beP5v9ZKyrt/vfdrF8P/CPVvGXiSz8W/E+W31DU7OTztJ8NWsnm6ZpLfwyfMq/aLgf89mX5f4VWgCn8NfDOp/E7xfH8T/GOn/YWhRofC2g3Mf73TbdvvXEufu3Eygf9c1+X726vcqKKACiiigAriPjL4z/AOEB+FfirxAjKlxZ6fK1ru/iuGXbAv8AwKRlX8a7evmf9tDxNc6lovhj4b+G7m1m8a+JdZspLXT5vnxBDOJmmkXqsatCvzf7LUAS6f4etbyPwD8FbeFru00OztdX8Uyrjywq/NHDJ/tT3A8xl/uxt/er6TrgvhP8LbL4V+HpLaOaTU9Yvp2vdW1e4+ae/um+/Ix/8dVf4VAFd7QAUVieKPDNh4v0W50rU1nNlcbd6211LbyfK275ZI2Vl6fwtXhPjL4Q/F3wbq41P4afEjUNQ0nb/pXh/wAUSLeybV6fZ7iRWbd/syMu7+KT+6AfSNFfPfgO+8YfFC0nNj8WJ9LurNlj1LSX8LW9tqFjIy7vLljmaTa3+1tZW/hZq7hvg3dakyf218RfGusRj70S30OnK3/ArGGBv/HqAKXxo+OFr8L9Lu7XTLVvEvjJrZrm00C0ceZ5a/enmb/ljAv8Uj7V/hX5q6j4U+NpfiB8MfDXim7t0sZNU0+K9khVtyx7l3feryj9pDRdF+D/AOzD8QR4d06LT5dQs/s7yLuae7mmZYd0kjHzJJG3/eZmatj4qam/wU/ZdnsLNUTUrXRbfQ9PjVvvXUirbx/+PNu/4C1AFL9muRfH/iDx78VZQzL4g1N9M0ot0XTrNmhjK/3fMkWSRv8AaNT/ABut3+L3izTPhPaTlNNk8rV/E00Iz5dlHJuitz/tTSL/AN8o3/ArOveLLX9nj4Z+EPB+gaSNX8UzW0Ol6J4eglw11Mqr5kjN2jX5nkkb/wBmrsPhb8P28B6TcPqV2mqeK9Wl+26zqgTb9puNv3V/uxovyxr/AAqv1oA7iOFYY1jjXYijaFXtUtFFABRRRQBx/ib4seCPBOoJY+IvGPh/QL94/MW21TVIbaUr/e2yMrY4rkdU/az+Dmk3Pky/EXw/PJj/AJdLtblf++o91eha14N0HxIyyaxomm6q6jarX1pHNtH/AAJafovhPRfDcbLo2j6fpKv95bG2jhDfXatAHzr4+/4KDfDfwrG0ehWuueNb5kYRrpenyRweZ/CrSTbev95VavjL4nftTfEX4yfEjwq3irQ9W0XwdY61b6hHomiWskd35ccm5mjm+WRpvL3fMrKv8W1a/XCuO8ceVHrvgiSRtrf2wyx5/vNaXNAHgXwu/ad+EPhbT20/w14K8VeGoJG8yZ/+EZuJGmb+9JJH5jSN/tNur1WT9pTwjJbiW1sfGF6zfdjtvB2qlv8Ax62C16zRQB47H+0R5y7rb4afEW6Xs39geTu/4DNIjUrfH/UY0Pl/CL4izt/d+wWi/wDoV1XsNFAHgXh341/FnxZr0thbfBdvD8CqXjvvE2uNBFJ/s/uLaba1dGf+F46hNwvw+0KJj632pMi/+S+7/wAdr1qigDx3VvA/xj1rTLmBfijoOjyzLgTaZ4RYyw/7vnXrr/30tfNmtfsl+Ifgv8VfCfxQuviB4r8T3rX8kWtavYaQt1e2sclvKqyLD+/3LuZY2+Vtqt8tfetFAHkcHxS8beKG2eE/hxetZKvOpeMbz+xlf/dhWKWb/vuOOnW9v8b9QM0k174B8P8Az/u4Y7O91P5f9qTzbb/0GvWqKAPJn0X42q25PFngOX/pm3hu9Rf/AEvaomtvjrGpZdR+Hty38Mbaffw7v+Bee23/AL5r16igD5p+JHgX4seMHttasPDHhXQ/GljGVsfEGj+J7hZov+mUiyWG2aFm+9G3r/C3zVD4P/ag8b2PihfBPj34Vapb+L47fzY5NDvLVrXUlX70lv8AaJ4t3+4rSMtfTlcz408CaF8QtEfS9d0+O/ttwljZsrLDKv3ZIpB80ci/3lw1AHzD+1X8VrvxZ8PtI0C++G3jfRY9Q8Q6ZG8lzaWr+Yq3KyNHH5NzJukZYztWuc/aj+Pt54q8ReCvDWlfDHxrqepaTqkfiG50W605oftUEKt5b/u/M3RrIy7vl/2a6v4nt4n8P/Gj4NeBdf1ddd0T+3JNastau1WK58u3hlVobnHyyMvnrtkXb8v3l3fM3qPw7uv+FlfF7V/iHaW8ieGbXTI9B0i7lXnUG85pJ7iL/pif3aq38W1m+7QUN+Afhmx1yFviTqN/J4l8Xa1GY5dTudPks1soVb/j0t4ZFVoo147bpGXc3t7bRRQSFFFFABRRRQAUUUUAFc34s8KR+LP7G33Elq2l6lBqUbxD7zR5+X/dZWZfxrpKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD5l+PuiaP4u/af+Cei+JNNs9W0aS11lltL6FZImm8qLbuVvvfdX5a+lIoUt41jRVjjVdqqo+UCvkn9rrVm0T9pf8AZuugv/MWuoPlb/no1tH/AOzV9eUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAfMP7YXw/vPE3i/4G69ZQtM+j+MbaCXyxykUzIzN/u/6Ov/fVfT1FFABRRRQAUUUUAFFFFAH/2Q==" class="img-fluid"/></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        Jorge Khoury<br />
                        Diretor Superintendente
                    </p>
                </td>
                <td>
                    <p></p>
                </td>
                <td>
                    <p>
                        Franklin Santana Santos<br />
                        Diretor Técnico
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <p><strong>ANEXO I - PRAZO MÁXIMO DE ENTREGA </strong></p>
    <table>
        <tbody>
            <tr>
                <td>
                    <p><strong>Solução SEBRAETEC</strong></p>
                </td>
                <td>
                    <p><strong>Prazo de Entrega (dias)</strong></p>
                </td>
            </tr>
            <tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Certifica&ccedil;&atilde;o de Processos de Teste MPT.BR N&iacute;vel 2</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR 15575:2013 - Desempenho de Edifica&ccedil;&otilde;es Habitacionais</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Norma ABNT NBR 16170:2013 - Qualidade do P&atilde;o tipo Franc&ecirc;s</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 15189:2015 - Laborat&oacute;rios Cl&iacute;nicos</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 22000:2019 - Sistemas de Gest&atilde;o da Seguran&ccedil;a de Alimentos</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO 9001:2015 - Sistema de Gest&atilde;o da Qualidade - Petr&oacute;leo, G&aacute;s e Sistemistas da Ind&uacute;stria (Setor Petr&oacute;leo)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISO IEC 20000-1:2011 - Tecnologia da Informa&ccedil;&atilde;o - Gest&atilde;o de Servi&ccedil;os</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma ABNT NBR ISSO 9001:2015</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma Rainforest Alliance para Agricultura Sustent&aacute;vel (RAS) - Cadeia de Cust&oacute;dia</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; norma Rainforest Alliance para Agricultura Sustent&aacute;vel (RAS) - C&oacute;digo de Conduta</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; Regulamenta&ccedil;&atilde;o da Produ&ccedil;&atilde;o Org&acirc;nica no Brasil</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave; s&eacute;rie de normas ABNT NBR ISO 12647 - Tecnologia gr&aacute;fica - Provas digitais</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o ao Programa Brasileiro da Qualidade e Produtividade do Habitat (PBQP-H)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave;s Normas Ambientais para Servi&ccedil;os Automotivos</p></td>
		<td><p>35</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o &agrave;s Normas de Qualidade para Servi&ccedil;os Automotivos</p></td>
		<td><p>35</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o conforme Modelo CMMI-DEV, CMMI-ACQ e CMMI-SVC</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de agroind&uacute;strias aos Servi&ccedil;os de Inspe&ccedil;&atilde;o de Produtos de Origem Animal e/ou Vegetal</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de estabelecimentos produtores de bebidas alco&oacute;licas para registro</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de ind&uacute;strias &agrave;s boas pr&aacute;ticas de fabrica&ccedil;&atilde;o de produtos saneantes, de higiene pessoal, cosm&eacute;ticos e perfumes</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o de Produtos aos Regulamentos T&eacute;cnicos e aos Mecanismos de Avalia&ccedil;&atilde;o da Conformidade Compuls&oacute;rios - Panelas Met&aacute;licas</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o do Manejo Nutricional de Rebanho Leiteiro - Bovinocultura</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Adequa&ccedil;&atilde;o para Certifica&ccedil;&atilde;o de Produ&ccedil;&atilde;o Integrada da Cadeia Agr&iacute;cola e Boas Pr&aacute;ticas na Produ&ccedil;&atilde;o Agr&iacute;cola</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Aplica&ccedil;&atilde;o do M&eacute;todo de Implementa&ccedil;&atilde;o de Softwares com Tecnologia BIM</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Processos nas Ind&uacute;strias de Alimentos</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Processos nas Ind&uacute;strias de Alimentos - Modalidade Compacta</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Tempo de Vida de Prateleira</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Avalia&ccedil;&atilde;o de Tempo de Vida Prateleira</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Biotecnologia da Fermenta&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas de Fabrica&ccedil;&atilde;o para Empresas de Alimentos e Bebidas</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas Higi&ecirc;nico-Sanit&aacute;rias e Cuidados Contra a COVID-19</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas Higi&ecirc;nico-Sanit&aacute;rios e Cuidados Contra COVID-19</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas na Pecu&aacute;ria de Leite e/ou Corte - Ovino e Caprino</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas no Segmento de Beleza</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Boas Pr&aacute;ticas para Servi&ccedil;os de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Branding</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o Ambiental de Servi&ccedil;os Automotivos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o conforme norma ABNT NBR ISO 9001:2015 - Sistema de Gest&atilde;o da Qualidade</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o conforme protocolo GlobalGAP</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Conte&uacute;do Local Para Servi&ccedil;os e Equipamentos (ANP)</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Produtos Org&acirc;nicos</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Certifica&ccedil;&atilde;o de Servi&ccedil;os Automotivos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Certificado de Registro Cadastral - CRC (Setor Petr&oacute;leo)</p></td>
		<td><p>150</p></td>
	</tr>
	<tr>
		<td><p>Consultoria Omnichannel Para Integra&ccedil;&atilde;o dos Canais de Vendas</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para a Implanta&ccedil;&atilde;o de Estrat&eacute;gias de Manejo de Moscas-das-Frutas baseadas no Controle Autocida</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para Acompanhamento de Projeto Piloto em BIM</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para cria&ccedil;&atilde;o de BIM Mandate e Plano de Implanta&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para Mapeamento de Fluxo de Pessoas e Intelig&ecirc;ncia para o Varejo</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Consultoria para o Monitoramento de Moscas-das-Frutas em Pomares de Frutas</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos </p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Geral</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Ind&uacute;stria Gr&aacute;fica</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Modelagem de Produtos de Moda</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos - Otimiza&ccedil;&atilde;o do Processo Produtivo Gr&aacute;fico</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Controle e Melhoria de Processos com Conectividade (IoT)</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Arte de Card&aacute;pio</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Logomarca</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Mascote</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Pe&ccedil;as Digitais &ndash; Redes Sociais</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Pe&ccedil;as Gr&aacute;ficas &ndash; Itens de Papelaria</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Cria&ccedil;&atilde;o de Website</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Dep&oacute;sito de Patente de Inven&ccedil;&atilde;o ou de Modelo de Utilidade</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Cole&ccedil;&otilde;es - Arquitetura de Cole&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Cole&ccedil;&otilde;es - Design de Moda</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de M&iacute;dias Digitais de Comunica&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Novos Produtos Aliment&iacute;cios - Geral</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento de Produto</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Desenvolvimento e Implanta&ccedil;&atilde;o de Estrat&eacute;gia, Campanha e A&ccedil;&otilde;es Transm&iacute;dia Storytelling</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Ambiente Interno</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Fachada </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Parklet</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Ambientes &ndash; Quiosque </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Design de Embalagem </p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design de Produto Tridimensional - Artesanato</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design de R&oacute;tulo</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Design e Melhoria de Servi&ccedil;os</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Efici&ecirc;ncia Energ&eacute;tica</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Card&aacute;pio para Servi&ccedil;os de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Fichas T&eacute;cnicas para o Segmento de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Elabora&ccedil;&atilde;o de Fichas T&eacute;cnicas para o Segmento de Alimenta&ccedil;&atilde;o - Padroniza&ccedil;&atilde;o de Produtos Panific&aacute;veis</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Fertiliza&ccedil;&atilde;o In Vitro (FIV) - Rebanho</p></td>
		<td><p>150</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o da Loja Virtual</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Processos de Gest&atilde;o da Inova&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Requisitos de SMS (Sa&uacute;de, Meio Ambiente e Seguran&ccedil;a) para Fornecedores - Setor Petr&oacute;leo</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o de Requisitos para Acredita&ccedil;&atilde;o Hospitalar - ONA</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o do C&oacute;digo de Barra</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Implanta&ccedil;&atilde;o ou Adequa&ccedil;&atilde;o na Opera&ccedil;&atilde;o do Delivery</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Impulsionamento das Redes Sociais</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Insemina&ccedil;&atilde;o Artificial por Tempo Fixo - IATF - Rebanho - Bovino</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Ind&uacute;stria da Moda</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Ind&uacute;stria de Alimentos</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Lean Manufacturing - Laborat&oacute;rio Fabril</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria da Qualidade do Leite</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Melhoria de Layout Produtivo</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Melhoria de Processo de Produ&ccedil;&atilde;o para o Segmento de Alimenta&ccedil;&atilde;o</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Banana</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Caf&eacute;</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Melhoria do Processo Produtivo Agr&iacute;cola - Geral</p></td>
		<td><p>240</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Calibra&ccedil;&atilde;o</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Ensaios - Avalia&ccedil;&atilde;o de Carca&ccedil;a de Bovino de Corte atrav&eacute;s de Ultrassonografia</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Metrologia - Ensaios - Servi&ccedil;o de An&aacute;lise Microbiol&oacute;gica</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Organiza&ccedil;&atilde;o e Controle de Estoque - Segmento de Beleza</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Otimiza&ccedil;&atilde;o da Cadeia de Suprimentos</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;gua Subterr&acirc;nea</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;guas Superficiais - Estaduais</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Outorga de &Aacute;guas Superficiais - Federais</p></td>
		<td><p>360</p></td>
	</tr>
	<tr>
		<td><p>Passeio Virtual &ndash; Tour Virtual 360&ordm; </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o - Ind&uacute;stria da Moda</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Planejamento e Controle de Produ&ccedil;&atilde;o - Padroniza&ccedil;&atilde;o de M&eacute;todos de Manufatura no Setor da Moda</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Planejamento para Busca Org&acirc;nica &ndash; SEO </p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Plano de Gerenciamento de Res&iacute;duos S&oacute;lidos</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Presen&ccedil;a Digital do Zero</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Preserva&ccedil;&atilde;o de Produtos por T&eacute;cnicas de Congelamento</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Prospec&ccedil;&atilde;o de &Aacute;guas Subterr&acirc;neas com An&aacute;lise Geof&iacute;sica - Ambiente Fraturado ou C&aacute;rstico</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Prototipagem de Novos Produtos</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Prototipagem via Laborat&oacute;rio Aberto</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Qualidade do Caf&eacute; - Crit&eacute;rios SCAA - Colheita e P&oacute;s-Colheita</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Qualidade no Turismo - Implanta&ccedil;&atilde;o de procedimentos</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Realidade Aumentada Para os Pequenos Neg&oacute;cios</p></td>
		<td><p>270</p></td>
	</tr>
	<tr>
		<td><p>Realiza&ccedil;&atilde;o de Modelagem e Simula&ccedil;&otilde;es de projetos em BIM</p></td>
		<td><p>120</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio na Cozinha</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio nos Pequenos Neg&oacute;cios</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Redu&ccedil;&atilde;o de Desperd&iacute;cio nos Pequenos Neg&oacute;cios - Segmento de Beleza</p></td>
		<td><p>60</p></td>
	</tr>
	<tr>
		<td><p>Registro de Desenho Industrial</p></td>
		<td><p>45</p></td>
	</tr>
	<tr>
		<td><p>Rotulagem de Alimentos e Informa&ccedil;&atilde;o Nutricional</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Rotulagem de Alimentos e Informa&ccedil;&atilde;o Nutricional</p></td>
		<td><p>90</p></td>
	</tr>
	<tr>
		<td><p>Sistema APPCC - An&aacute;lise de Perigos e Pontos Cr&iacute;ticos de Controle</p></td>
		<td><p>180</p></td>
	</tr>
	<tr>
		<td><p>Turista Oculto - Diagn&oacute;stico Tecnol&oacute;gico</p></td>
		<td><p>30</p></td>
	</tr>
	<tr>
		<td><p>Ultracongelamento de Produtos Aliment&iacute;cios</p></td>
		<td><p>90</p></td>
	</tr>
        </tbody>
    </table>
</div>


<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<!-- hook available: wpo_wcpdf_before_footer -->
	<?php $this->footer(); ?>
	<!-- hook available: wpo_wcpdf_after_footer -->
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
