<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<?php 
	
	$amount = $this->order->get_formatted_order_total();
	$razao_social = get_post_meta($this->order->id, '_billing_company', true);
    $perfil = get_post_meta($this->order->id, '_billing_perfil', true);

	/*arrays com shortcodes e variaveis que as subistituirão, respectivamente*/
	$shortcode =  array("[termo_total]", '[termo_razao]');
	$variaveis =  array($amount, $razao_social);

	/*id da pagina de configuração dos termos*/
	$term_page = 2140;
?>

<div>
    <p><strong>TERMO DE ADESÃO E COMPROMISSO</strong></p><br/>
    <table>
        <tbody>
            <tr>
                <td>
                    <p>EMPRESA DEMANDANTE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <?php if($perfil == "Produtor Rural"): ?>

                    <p><strong>CNPJ ou DAP/NIRF: </strong> <span id="term-cnpj"><?php echo get_post_meta($this->order->id, '_billing_dap_nirf', true); ?></span></p>

                    <?php else: ?>

                    <p><strong>CNPJ ou DAP/NIRF: </strong> <span id="term-cnpj"><?php echo get_post_meta($this->order->id, '_billing_cnpj', true); ?></span></p>

                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Razão Social: </strong> <span id="term-razao"><?php echo get_post_meta($this->order->id, '_billing_company', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Telefone:</strong> <span id="term-telefone"><?php $this->billing_phone(); ?></span></p>
                </td>
                <td>
                    <p><strong>Celular</strong>: <span id="term-celular"><?php echo get_post_meta($this->order->id, '_billing_cellphone', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Nome do Responsável:</strong> <span id="term-nome"><?php echo get_post_meta($this->order->id, '_billing_first_name', true).' '.get_post_meta($this->order->id, '_billing_last_name', true); ?></span></p>
                </td>
                <td>
                    <p><strong>CPF:</strong> <span id="term-cpf"><?php echo get_post_meta($this->order->id, '_billing_cpf', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>E-mail:</strong> <span id="term-email"><?php echo get_post_meta($this->order->id, '_billing_email', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Endereço:</strong> <span id="term-endereco"><?php echo get_post_meta($this->order->id, '_billing_address_1', true).' - '.get_post_meta($this->order->id, '_billing_city', true).'/'.get_post_meta($this->order->id, '_billing_state', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>CEP:</strong> <span id="term-cep"><?php echo get_post_meta($this->order->id, '_billing_postcode', true); ?></span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>DADOS DO EVENTO</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Produto(s):</strong> 

					
					<?php
						$items = $this->get_order_items(); 
						if( sizeof( $items ) > 0 ) {
							foreach( $items as $item_id => $item ) {

                                $termo = "";
                                $meta = get_post_meta($item['product_id']);

                                if(isset($meta['term-type'])){
                                    $termo =  ' (termo de adesão ' . $meta['term-type'][0] . ')';
                                }                             
								
								echo $item['name'].' - '.$item['order_price'] .  $termo . '';
								if ($item_id !== array_key_last($items)) {
									echo ' / ';
								}
			
							
							}
						}
					?>
					
					</p>
                </td>
				
            </tr>
            <tr>
                <td><?php $data = $this->order->get_date_created();?>
                    <p><strong>Data Prevista de Início: </strong><?php echo date('d/m/Y', strtotime("+12 days",strtotime($data)));?></p>
                </td>
                <td>
                    <p><strong>Data Prevista de Término: </strong>Tabela - Anexo I</p>
                </td>
                <td> </td>
            </tr>
        </tbody>
    </table>
    <p></p>

		<!-- clausuras -->
		<?php 
			$content = get_field('termo_adesao', $term_page); 
			if ($content){
				$content = str_replace($shortcode, $variaveis, $content);
				echo $content;
			}
		?>
		<!-- clausuras -->

    
		<!--assinaturas-->
    <table>
        <tbody>
            <tr>
                <td>
										<?php $date_created = $this->order->get_date_created();?>
                    <p>Salvador - BA, <?php echo $date_created->date("d/m/Y");?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>

    <table style="text-align:center;width:100%;">
        <tbody>
						<?php $signatures = get_field('termo_assinatura', $term_page); ?>

						<?php 
							function imageEncodeURL($path)
							{
								$image = file_get_contents($path);
								$finfo = new finfo(FILEINFO_MIME_TYPE);
								$type  = $finfo->buffer($image);
								return "data:".$type.";charset=utf-8;base64,".base64_encode($image);
							}
						?>

            <tr>
								<?php foreach ($signatures as $key => $signature): ?>
                <td>
                    <p>
											<?php if($signature['termo_assinatura_img']): ?>
												<img src="<?= imageEncodeURL($signature['termo_assinatura_img']);  ?>" class="img-fluid"/>
											<?php endif; ?>
										</p>
                </td>

								<?php if($key == 0): ?>
                <td>
                    <p></p>
                </td>
								<?php endif;?>
   
								<?php endforeach; ?>
            </tr>
            <tr>
								<?php foreach ($signatures as $key => $signature): ?>
								
                <td>
                    <p>
                        <?php echo $signature['termo_assinatura_nome']; ?><br />
                        <?php echo $signature['termo_assinatura_cargo']; ?>
                    </p>
                </td>

								<?php if($key == 0): ?>
                <td>
                    <p></p>
                </td>
								<?php endif; ?>
  
								<?php endforeach; ?>
            </tr>
        </tbody>
    </table>
    <p></p>
		<p></p>
		
		<!--assinaturas-->

    <?php echo get_field('termo_anexo', $term_page); ?>
</div>


<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<!-- hook available: wpo_wcpdf_before_footer -->
	<?php $this->footer(); ?>
	<!-- hook available: wpo_wcpdf_after_footer -->
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
