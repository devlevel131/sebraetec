<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	

	<section class="product">



		<div class="mb-4">

			<span class="avenir-medium d-block mb-3 size-24 text-uppercase">
			
			<?php
			$cat_id = get_post_meta( $post->ID, 'epc_primary_' . 'product_cat', true );
			if($cat_id){
				$category_detail = get_term_by('id', $cat_id, 'product_cat');
				$category_link = get_category_link( $cat_id );
				echo '<a class="single-cat-link color-gray" href="'.$category_link.'">'.$category_detail->name.'</a>';
			} else{
				$count = 1; 
				$terms = get_the_terms($post->ID, 'product_cat' );
				if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
					foreach ($terms as $term) {
						if($term->slug != 'top-10-mais-vendidos'){
							if($count == 1){
								echo '<a class="single-cat-link" href="'.get_term_link($term->slug, 'product_cat').'">'.$term->name.'</a>';
							}
							$count++; 
						}
					}
				}
			}
			
			?>

			</span>



			<?php the_title( '<h1 class="avenir-black size-26">', '</h1>' );?>

			<h2 class="size-16"><?php echo get_field('phrase');?></h2>

		</div>



		<div class="row">



			<div class="col-12 col-lg-8">



				<div class="row">



					<div class="col-12 col-lg-6 mb-4">

					
					<?php
					/**
					 * Hook: woocommerce_before_single_product_summary.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
					?>



					</div>



					<div class="col-12 col-lg-6 mb-4">

						<div class="product-desc">



							<span class="color-blue text-uppercase">Objetivo:</span>



							<div class="color-gray size-15 lh-1-4">

								<?php echo get_field('objectives');?>

							</div>

						</div>

					</div>



					<div class="col-12 product-box-buy">

						<div class="product-form">

							<div class="product-form-title bg-blue px-5 py-3 color-white avenir-medium size-18">

								Por favor, preencha às questões abaixo para que seja possível gerar a cotação:

							</div>

							<div class="product-form-content px-5 py-4">

								<?php do_action( 'woocommerce_single_product_options' );?>

							</div>

						</div>

						<!--product-form-end--> 

						<div class="py-4 d-flex flex-column flex-lg-row align-items-center justify-content-center">

							<div class="summary entry-summary">
							<?php do_action( 'woocommerce_single_product_form' );?>
							</div>
							
							

						</div>

					</div>



				</div>



			</div>



			<div class="col-12 col-lg-4 mb-4">

				<div class="product-right-col ps-4 pe-4 ps-lg-0 pe-lg-5">



					<div class="d-flex d-lg-block flex-column align-items-center">



						<div class="mb-4">
						
							<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>

							<div class="size-16 color-gray avenir-medium mb-2">Divida sem juros no cartão</div>
							
							<?php
							if( has_term( 'top-10-mais-vendidos', 'product_cat', $post ) ){
								echo '<div class="color-red size-12 avenir-medium oblique"><span class="icon-star-full"></span> TOP 10 MAIS VENDIDOS</div>';
							}
							?>
						</div>


						<?php if( has_term( 'orcamento', 'tipos-de-produtos', $post ) ):?>
							<a href="#" class="btn-quero-comprar btn btn-blue btn-buy w-100">SOLICITAR PROPOSTA</a>
						<?php else:?>
							<a href="#" class="btn-quero-comprar btn btn-blue btn-buy w-100">QUERO COMPRAR</a>
						<?php endif;?>

					</div>


					<?php
					if( have_rows('testimonials') ):?>
					<div class="mb-4 mt-4 d-flex align-items-center justify-content-center justify-content-lg-start">

						<span class="btn-like me-1">

							<span class="icon-like"></span>

						</span>

						<span class="color-gray avenir-medium text-uppercase size-04vw size-xs-16">Quem comprou, recomenda!</span>

					</div>



					<div>
						<?php
							echo '<div id="product-testimony" class="owl-carousel owl-theme">';
							while ( have_rows('testimonials') ) : the_row();			
							
							$youtube = get_sub_field( "video_testimonials" );
							
							parse_str( parse_url( $youtube, PHP_URL_QUERY ), $youtube_var );							

						?>
													
							<div class="item">

								<a class="product-testimony-thumb" data-iframe="https://www.youtube.com/embed/<?php echo $youtube_var['v']; ?>" data-bs-toggle="modal" data-bs-target="#videoModal" >

									<img src="https://img.youtube.com/vi/<?php echo $youtube_var['v']; ?>/mqdefault.jpg" class="img-fluid"/>

								</a>

							</div>
						
						<?php	
							endwhile;
							echo '</div>';
						?>

					</div>
					
					<?php	
						endif;
						wp_reset_query();
					?>



				</div>

			</div>



		</div>



		<!--MODAL-->

		<div class="modal fade" id="videoModal" tabindex="-1" aria-labelledby="videoModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-lg modal-dialog-centered">

			<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title" id="videoModalLabel">Depoimento</h5>

				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

			</div>

			<div class="modal-body">

				<div class="video-container">

					<iframe width="640" height="360" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>

			</div>

			<div class="modal-footer">

			</div>

			</div>

		</div>

		</div>

		<!--modal-->



	</section>

	

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
	
	<section id="product-instructions">

		<div class="container">
			
			<div class="row">		
			
				<div class="col-md-12">
					<?php
						$entrega = get_field('deliveries', $post->ID);
						$saiba_mais = get_field('know-more', $post->ID);
						$observacao = get_field('observation', $post->ID);
						$beneficios = get_field('benefits', $post->ID);
						$segmentos = get_field('segments', $post->ID);
						$setores = get_the_term_list( $post->ID, 'setores', '', ', ');
						$setores = strip_tags( $setores );
						$perfis = get_the_term_list( $post->ID, 'perfis', '', ', ');
						$perfis = strip_tags( $perfis );
					?>
					
					<?php if($entrega):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">ENTREGAS:</h4>

						<div class="color-gray d-block"><?php echo $entrega;?></div>

					</div>
					<?php endif;?>

					<?php if($saiba_mais):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">SAIBA MAIS:</h4>

						<div class="color-gray d-block"><?php echo $saiba_mais;?></div>

					</div>
					<?php endif;?>
					
					<?php if($observacao):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">OBSERVAÇÃO:</h4>

						<div class="color-gray d-block"><?php echo $observacao;?></div>

					</div>
					<?php endif;?>

					<?php if($beneficios):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">BENEFÍCIOS:</h4>

						<div class="color-gray d-block"><?php echo $beneficios;?></div>

					</div>
					<?php endif;?>					

					<?php 
					if($setores):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">ATENDE AOS SETORES:</h4>
						
						<div class="color-gray d-block">
						<?php echo $setores;?>
						</div>
							
					</div>
					<?php endif;?>

					<?php if($segmentos):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">ATENDE AOS SEGMENTOS:</h4>
						
						<div class="color-gray d-block"><?php echo $segmentos;?></div>

					</div>
					<?php endif;?>
					
					<?php 
					if($perfis):?>
					<div class="mb-4">

						<h4 class="size-16 color-blue d-block">PÚBLICO ALVO:</h4>
						
						<div class="color-gray d-block">
						<?php echo $perfis;	?>
						</div>

					</div>
					<?php endif;?>
				</div>
				
			</div>
			
		</div>

	</section>

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>

