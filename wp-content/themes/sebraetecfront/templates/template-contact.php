<?php /* Template Name: Contato */  ?>

<?php get_header() ?>

<section id="contact">

  <div class="container pt-4">

      <h1 class="avenir-light color-gray">Contato</h1>

      <div id="contact-content" class="color-gray px-5">
        <?= the_content(); ?>
      </div>

      <div id="contact-form" class="mb-5 pt-5">

        <?php $shortcode = "'".get_field('contact_shortcode')."'"; ?>

        <?php echo do_shortcode($shortcode); ?>

      </div>

  </div>

</section>

<?php get_footer() ?>