<?php /* Template Name: Produto A*/  ?>
<?php get_header() ?>

    <?php get_template_part('template-parts/product/content','a'); ?>

    <?php get_template_part('template-parts/product/related'); ?>

    <?php get_template_part('template-parts/product/instructions'); ?>

<?php get_footer(); ?>