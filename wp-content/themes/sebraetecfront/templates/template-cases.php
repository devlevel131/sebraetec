<?php /* Template Name: Cases de Sucesso */  ?>

<?php get_header() ?>

<?php //get_template_part('template-parts/post/banner') ?>

<section id="content-principal" class="pt-3">

<div class="container color-gray">

    <div class="d-flex flex-column flex-lg-row justify-content-between">

        <?php /*form action="<?= home_url( '/' ); ?>" method="get" id="internal-search" class="mb-5 mb-lg-0">

          <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />

          <button type="submit"><span class="icon-search-2"></span></button>

          <input type="hidden" value="post" name="post_type" id="post_type" />

        </form*/ ?>
 
        <h3 class="size-30 avenir-light color-gray mb-4 d-block">
          Casos de Sucesso
        </h3>

    </div>

    <?= the_content(); ?>

  <div class="row pt-5">

    <?php

      $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

      $args = array(
        'posts_per_page' => 10, 
        'post_type' => 'casos', 
        'post_status' => 'publish',
        'paged' => $paged
      );	

      query_posts($args);
    ?>
    
    <?php if ( have_posts() ): ?>
    
      <?php while (have_posts()) : the_post(); ?>

      <div class="col-12 col-md-6 mb-5 ">

        <div class="">
              <?php $post_tags = get_the_tags(); ?>

              <?php if ($post_tags): ?>

                <span class="st-product-tag"><?= $post_tags[0]->name; ?></span>

              <?php else: ?>

                <span class="st-product-tag">Case de sucesso</span>

              <?php endif; ?>
        </div>

        <article class="row h-100">

          <div class="col-12 col-xl-7">
            <a href="<?= get_permalink();?>" class="d-block principal-img">
              <img src="<?= get_the_post_thumbnail_url( $post->ID, 'principal-size' ); ?>" />
            </a>
          </div>

          <div class="col-12 col-xl-5">

            <a href="<?= get_permalink();?>" class="color-gray avenir-medium pb-3 mt-3 mt-xl-0 lh-1 d-block">
              <?php the_title(); ?>
            </a>

            <?php

              $excerpt = get_the_excerpt();

              if(strlen($excerpt) > 110){
                
                $excerpt = mb_substr(get_the_excerpt(),0,110,'UTF-8');

                $exerpt_array = str_split($excerpt);

                $exerpt_array_r = array_reverse($exerpt_array, true);

                foreach ($exerpt_array_r as $key => $char ){
                  if ($char == " "){
                    unset($exerpt_array[$key]);
                    break;
                  }
                  else{
                    unset($exerpt_array[$key]);
                  }
                }

                $excerpt = implode("", $exerpt_array) . '...';

              }

            ?>

            <p class="size-14 avenir-light mb-3 lh-1">
              <?= $excerpt; ?>
            </p>

            <div class="">
              <a href="<?= get_permalink();?>" class="btn btn-yellow st-product-add-to-cart">SAIBA MAIS!</a>
            </div>

          </div>

        </article>

      </div>

      <?php endwhile; ?>
      <?php the_posts_pagination();?>
    <?php else: ?>


    <?php endif; ?>
    <?php wp_reset_query();?>

  </div>

</div>
</section>

<?php get_footer() ?>