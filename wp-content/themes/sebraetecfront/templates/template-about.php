<?php /* Template Name: Sobre */  ?>

<?php get_header() ?>

<section id="about">

  <div class="container pt-4">

      <h1><span class="avenir-light color-gray">O que é</span> <span class="color-blue avenir-black oblique">SEBRAETEC?</span></h1>

      <div id="about-content">
        <?= the_content(); ?>
      </div>

      <div id="about-areas" class="pt-5">

        <h3 class="avenir-black oblique text-uppercase color-blue"><?= get_field('area_titulo'); ?></h3>
        <p class="color-gray"><?= get_field('area_texto'); ?></p>

        <div class="row">

          <?php $cards = get_field('area_cards'); ?>

          <?php if($cards): ?>
          <?php foreach ($cards as $card): ?>

            <div class="col-12 col-md-6 col-lg-4 mb-5">

            <div class="about-card">
              
              <?php $area_titulo = $card['area_card_titulo']; ?>
              <?php $area_url = $card['area_card_url']; ?>

              <?php if($card['area_card_url']):?>
                <a href="<?= $area_url ?>" class="about-card-title"><?= $area_titulo ?></a>
              <?php else: ?>
                <a class="about-card-title"><?= $area_titulo ?></a>
              <?php endif; ?>

              

              <p class="color-gray"><?= $card['area_card_texto']; ?></p>
              
              <ul class="color-gray">
                <?php $lista = $card['area_card_lista']; ?>
                <?php if($lista): ?>
                <?php foreach ($lista as $topico): ?>

                  <li><?= $topico['area_card_topico']; ?></li>

                <?php endforeach; ?>
                <?php endif;?>
              <ul>

            </div>
            </div>

          <?php endforeach; ?>
          <?php endif;?>

        </div>
      </div>
      
      <div id="about-faq" class="pt-5">

          <h3 class="avenir-black oblique text-uppercase color-blue">
            Perguntas Frequentes (FAQ)
          </h3>

          <div class="accordion mb-5" id="accordionfaq">

            <?php $faqs = get_field('faq'); ?>
            <?php if($faqs): ?>
            <?php foreach ($faqs as $key => $faq): ?>   

            <div class="accordion-item">
              <div class="accordion-header" id="flush-heading-<?= $key ?>">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-<?= $key ?>" aria-expanded="true" aria-controls="flush-collapse-<?= $key ?>">
                  <?= $faq['faq_pergunta']; ?>
                </button>
              </div>
              <div id="flush-collapse-<?= $key ?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-1" data-bs-parent="#accordionfaq" style="">
                <div class="accordion-body">
                  <p><?= $faq['faq_resposta']; ?></p>
                </div>
              </div>
            </div>

            <?php endforeach ?>
            <?php endif; ?>

          </div>

		  </div>

      </div>

  </div>

</section>

<?php get_footer() ?>