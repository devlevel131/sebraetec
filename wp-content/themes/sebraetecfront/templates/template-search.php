<?php /* Template Name: Busca */  ?>
<?php get_header() ?>

<section id="product-list">

    <div class="container pt-4">

        <h1 class="color-gray size-24 d-block">Resultado da busca para <span class="color-blue">REDES SOCIAIS</span></h1>
        <h3 class="color-gray size-18 mb-5">"25 resultados"</h3>

        <div class="px-3">
   
            <div class="row">
                
                <?php for($i = 0; $i < 20; $i++): ?>

                <div class="col-12 col-md-4 col-lg-3">
                    <div class="st-product pb-4">

                        <div class="st-product-tag">Design</div>

                        <a class="mb-2 st-product-thumb">
                            <img src="<?= THEME_IMG ?>product_image.jpg" class="img-fluid"/>
                        </a>

                        <div class="color-gray avenir-medium mb-3">Certificação de Serviços Automotivos</div>

                        <div class="color-gray mb-2 size-14">Certifique o processo de reparação/produção automotivo de sua empresa.</div>

                        <div class="mt-auto">

                            <div  class="size-14 avenir-medium">De <s>R$889,00</s></div>

                            <div class="size-18 avenir-black">Por até R$389,00</div>
                            <div class="size-14 color-gray avenir-medium">Divida sem juros no cartão</div>
                            <div class="color-red size-12 avenir-medium oblique"><span class="icon-star-full"></span> TOP 10 MAIS VENDIDOS</div>

                            <div class="d-flex align-items-center justify-content-center my-2">
                                <a href="#" class="btn st-product-add-to-cart">COMPRAR</a>
                            </div>

                        </div>

                    </div>
                </div>

                <?php endfor; ?>
            </div>

            <nav aria-label="Page navigation example">
                <ul class="pagination pb-5">
                    <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous" aria-disabled="true">
                        <span class="icon-nav-left size-12" aria-hidden="true">
                
                        </span>
                    </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                    <li class="page-item"><a class="page-link" href="#">7</a></li>
                    <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span class="icon-nav-right size-12" aria-hidden="true">
                            
                        </span>
                    </a>
                    </li>
                </ul>
            </nav>
  
        </div>



    </div>

</section>



<?php get_footer() ?>