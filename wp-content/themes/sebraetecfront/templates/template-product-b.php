<?php /* Template Name: Produto tipo B*/  ?>
<?php get_header() ?>

    <?php get_template_part('template-parts/product/content','b'); ?>

    <?php get_template_part('template-parts/product/related'); ?>

    <?php get_template_part('template-parts/product/instructions'); ?>

<?php get_footer(); ?>