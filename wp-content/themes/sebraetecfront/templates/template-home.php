<?php /* Template Name: Home */  ?>
<?php get_header() ?>

<?php get_template_part('template-parts/home/banner','dinamic'); ?>

<?php get_template_part('template-parts/home/banner','top'); ?>

<?php get_template_part('template-parts/home/products'); ?>

<?php get_template_part('template-parts/home/video','1'); ?>

<?php get_template_part('template-parts/home/products','featured'); ?>

<?php get_template_part('template-parts/home/video','2'); ?>

<?php get_template_part('template-parts/home/products','carousel'); ?>

<?php get_template_part('template-parts/home/othercontents'); ?>

<?php get_template_part('template-parts/home/ebook'); ?>

<section id="end-products">

    <div class="container pt-0 pt-lg-4 pb-4">
        
        <div class="row d-none d-lg-flex">  			
		<?php $posts = get_field('produtos_destacados_final_home'); if( $posts ): ?>			
		<?php foreach( $posts as $post):?>			
		<?php setup_postdata($post); ?>		

			<?php 
			$ppl = get_the_permalink($post);
			if(!strpos($ppl, 'post_type')):	?>	
			<div class="col-4">				
				<div class="st-product">				
					<?php wc_get_template_part( 'content', 'product' );?>								
				</div>			
			</div>						
			<?php endif; ?>	

		<?php endforeach; ?>			
		<?php wp_reset_postdata();?>			
		<?php endif; ?>	
        </div>

        <!--MOBILE-->
        <div class="d-block d-lg-none">

			<div id="home-products-mob" class="product-carousel owl-carousel owl-theme">
            <?php $posts = get_field('produtos_destacados_home'); if( $posts ): ?>			
			<?php foreach( $posts as $post):?>			
			<?php setup_postdata($post); ?>				

				<?php 
				$ppl = get_the_permalink($post);
				if(!strpos($ppl, 'post_type')):	?>
				<div class="item">								
					<div class="st-product pb-4">						
						<?php wc_get_template_part( 'content', 'product' );?>										
					</div>				
				</div>			
				<?php endif; ?>	

			<?php endforeach; ?>			
			<?php wp_reset_postdata();?>			
			<?php endif; ?>	
            </div>

        </div>
        <!--MOBILE END-->

    </div>

    <div class="division d-block d-lg-none"></div>
</section>


<?php get_footer() ?>