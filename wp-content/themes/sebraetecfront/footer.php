<footer>

<div class="bg-blue-2 bg-lg-blue d-none d-lg-block">
    <div class="container py-4">
        <div class="row">

            <div class="col-12 col-lg-6">

                <span class="text-uppercase color-white d-block mb-5 avenir-black">Formas de pagamento</span>

                <div class="d-flex align-items-center justify-content-end pt-3">
				
					<?php if( have_rows('formas_de_pagamento_home', 59) ): ?>
					<div id="payments" class="">
					<?php while ( have_rows('formas_de_pagamento_home', 59) ) : the_row();	?>
						<div class="me-2 mb-2 d-inline-block"><img src="<?php echo get_sub_field('icones_formas_de_pagamento_home'); ?>"/></div>
					<?php 
					endwhile;
					?>
					</div>
					<?php
					endif;
					wp_reset_query();
					?>	

                </div>

            </div>
            <div class="col-12 col-lg-6">

                <div class="pt-5 px-5">

                    <div class="text-center avenir-black color-white mb-3"><?php echo get_field( "titulo_newsletter", 59 ); ?></div>

					<?php echo do_shortcode( '[contact-form-7 id="1607" title="Newsletter" html_id="newsletter"]' );?>

                </div>

            </div>

        </div>
    </div>
</div>
<div class="bg-blue-2 bg-lg-blue pt-4 pb-4 pb-lg-0">
    <div class="container">
        <div class="row pt-5">

            <div class="col-12 col-lg-2 text-center text-lg-end px-5 px-lg-0 mb-5">
                <a id="logo-footer" href="<?php echo get_home_url(); ?>">
                    <img src="<?= THEME_IMG ?>sebrae_logo.png" class="img-fluid"/>
                </a>
            </div>
            <div class="col-12 col-lg-7 px-lg-5">

                <div class="row">

                    <div class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center bottom-menu">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'bottom-menu-1',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>

                    
                    <div class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center bottom-menu">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'bottom-menu-2',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>

                    
                    <div class="size-13 col-6 col-lg-4 d-flex justify-content-lg-center mt-4 mt-lg-0 bottom-menu">	  <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'bottom-menu-3',
                                    'container'      => false,
                                    'fallback_cb'    => false
                                )
                            );
                        ?>
                    </div>

                </div>

            </div>
            <div class="col-12 col-lg-3 text-white text-center d-none d-lg-block">
                <div class="mb-1 mt-4 mt-lg-0 mb-lg-3">
                    <span class="icon-sebrae size-60"></span>
                </div>
                <div id="footer-social-icons" class="mb-4">
                    <ul class="d-flex align-items-center justify-content-center justify-content-lg-between">
					
						<?php if(get_field( "facebook", 59 )): ?>					
						<li class="pe-2 pe-lg-0">
							<a class="footer-social-icon size-14" href="<?php echo get_field( "facebook", 59 ); ?>">
								<span class="icon-facebook"></span>
							</a>
						</li>
						<?php endif;?>
						
						<?php if(get_field( "twitter", 59 )): ?>
						<li class="pe-2 pe-lg-0">
							<a class="footer-social-icon size-14" href="<?php echo get_field( "twitter", 59 ); ?>">
								<span class="icon-twitter"></span>
							</a>
						</li>
						<?php endif;?>
						
						<?php if(get_field( "youtube", 59 )): ?>
						<li class="pe-2 pe-lg-0">
							<a class="footer-social-icon size-14" href="<?php echo get_field( "youtube", 59 ); ?>">
								<span class="icon-youtube"></span>
							</a>
						</li>
						<?php endif;?>
						
						<?php if(get_field( "instagram", 59 )): ?>
						<li class="pe-2 pe-lg-0">
							<a class="footer-social-icon size-14" href="<?php echo get_field( "instagram", 59 ); ?>">
								<span class="icon-instagram"></span>
							</a>
						</li>
						<?php endif;?>
						
						<?php if(get_field( "linkedin", 59 )): ?>
						<li class="pe-2 pe-lg-0">
							<a class="footer-social-icon size-14" href="<?php echo get_field( "linkedin", 59 ); ?>">
								<span class="icon-linkedin-2"></span>
							</a>
						</li>
						<?php endif;?>
				
                        <?php /*li>
                            <a class="footer-social-icon-whatsapp size-50 color-white" href="">
                                <span class="icon-whatsapp"></span>
                            </a>
                        </li*/ ?>
                    </ul>

                </div>
                <div class="text-center size-20 size-lg-13">
                    <span><?php echo get_field( "texto_whatsapp", 59 ); ?></span><br/>
                    <span><?php echo get_field( "horario_de_atendimento", 59 ); ?></span>
                </div>
            </div>

            <div class="col-12 text-center py-5 size-11 size-lg-13 color-white d-none d-lg-block">
                <?php echo date('Y'); ?> © - <?php echo get_field( "copyright", 59 ); ?>
            </div>

        </div>
    </div>
</div>

<div class="bg-blue pt-5 d-block d-lg-none">
<div class="container">

    <div class="text-white text-center">

        <div class="">
            <span class="icon-sebrae size-60"></span>
        </div>

        <div id="footer-social-icons" class="py-4">
            <ul class="d-flex align-items-center justify-content-center justify-content-lg-between">
			
				<?php if(get_field( "facebook", 59 )): ?>					
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="<?php echo get_field( "facebook", 59 ); ?>">
                        <span class="icon-facebook"></span>
                    </a>
                </li>
				<?php endif;?>
				
				<?php if(get_field( "twitter", 59 )): ?>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="<?php echo get_field( "twitter", 59 ); ?>">
                        <span class="icon-twitter"></span>
                    </a>
                </li>
				<?php endif;?>
				
				<?php if(get_field( "youtube", 59 )): ?>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="<?php echo get_field( "youtube", 59 ); ?>">
                        <span class="icon-youtube"></span>
                    </a>
                </li>
				<?php endif;?>
				
				<?php if(get_field( "instagram", 59 )): ?>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="<?php echo get_field( "instagram", 59 ); ?>">
                        <span class="icon-instagram"></span>
                    </a>
                </li>
				<?php endif;?>
				
				<?php if(get_field( "linkedin", 59 )): ?>
                <li class="pe-2 pe-lg-0">
                    <a class="footer-social-icon size-14" href="<?php echo get_field( "linkedin", 59 ); ?>">
                        <span class="icon-linkedin-2"></span>
                    </a>
                </li>
				<?php endif;?>
				
                <?php /*li>
                    <a class="footer-social-icon-whatsapp size-50 color-white" href="">
                        <span class="icon-whatsapp"></span>
                    </a>
                </li*/ ?>
            </ul>
        </div>

        <div class="text-center size-20 size-lg-13">
            <span><?php echo get_field( "texto_whatsapp", 59 ); ?></span><br/>
            <span><?php echo get_field( "horario_de_atendimento", 59 ); ?></span>
        </div>

    </div>

    <div class="col-12 text-center py-5 size-11 size-lg-13 color-white">
        <?php echo date('Y'); ?> © - <?php echo get_field( "copyright", 59 ); ?>
    </div>

</div>
</div>

</footer>

<!--<script src="<?= THEME_ASSETS ?>jquery/jquery-3.6.0.min.js"></script>-->
<script src="<?= THEME_ASSETS ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?= THEME_ASSETS ?>owlcarousel/owl.carousel.min.js" ></script>
<script src="<?= THEME_JS ?>main.js"></script>
<?php wp_footer()?>	

<?php if (is_checkout()): ?>
<!-- Modal Term-->
<div class="modal" id="modal-term" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">TERMO DE ADESÃO E COMPROMISSO</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<div class="block-terms-produtos">
			<?php get_template_part( 'template-parts/termo-de-compra' ); ?>	
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="confirm-order-button" class="btn btn-blue">Aceitar</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

<?php if (is_front_page()): ?>
<!-- Modal Ebook -->
<div class="modal modal-sucesso" id="modal-ebook-sucesso" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
		<h5 class="modal-title"><?php echo get_field( "mensagem_agradecimento_modal_home", 59 ); ?></h5>
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

<!-- Modal Newsletter -->
<div class="modal modal-sucesso" id="modal-news-sucesso" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
		<h5 class="modal-title"><?php echo get_field( "mensagem_agredecimento_newsletter", 59 ); ?></h5>
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

</body>



</html>