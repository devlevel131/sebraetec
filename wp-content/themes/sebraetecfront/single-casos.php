<?php get_header() ?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php //$date_template = 'j \d\e F \d\e Y'; ?>

<?php get_template_part('template-parts/post/banner') ?>

<section id="single-casos" class="post-content pt-3">

  <article class="color-gray container pb-4 default-lists">
  
    <?php /*date class="color-gray size-16 gotham-bold">
      <?php echo get_the_date($date_template); ?>
    </date*/ ?>

    <div class="d-flex justify-content-end"> <a href="<?php echo get_home_url() . '/casos-de-sucesso'; ?>"> < Voltar</a></div>
    <h3 class="size-30 color-gray mb-5 d-none d-lg-block avenir-light">Casos de Sucesso</h3>

    <div class="case-tags">
        <?php $post_tags = get_the_tags(); ?>

        <?php if ($post_tags): ?>

          <span class="st-product-tag"><?= $post_tags[0]->name; ?></span>

        <?php else: ?>

          <span class="st-product-tag">Case de sucesso</span>

        <?php endif; ?>
    </div>

    <div class="">
      <h1 class="size-38 overflow-x-hidden avenir-medium color-blue d-block mb-2">
        <?= the_title()?>
      </h1>

      <h4 class="avenir-medium size-16 mb-4 d-block">
        <?= get_the_excerpt() ?>
      </h4>

      <?php get_template_part('template-parts/post/share') ?>

      <?php if(!empty(get_the_post_thumbnail_url())): ?>

        <div class="text-center post-thumbnail mb-4">
          <img src="<?= get_the_post_thumbnail_url($post->id,'medium_large')?>" class="d-inline-block img-fluid"/>
        </div>

      <?php endif; ?>
    
      <?= the_content() ?>
    </div>


  </article>

</section>

<?php endwhile; ?>

<?php else: ?>

<?php endif; ?>

<?php get_template_part('template-parts/post/related','product') ?>

<?php get_footer() ?>