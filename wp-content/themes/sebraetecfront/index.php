<?php get_header() ?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php $date_template = 'j \d\e F \d\e Y'; ?>

<!--<section class="post-content py-5">

    <article class="color-gray container py-4">-->

<section class="post-content pt-3 pb-5">

    <article class="color-gray container pb-4">
	
		<?php if(is_singular('post')): ?>
		
			<date class="color-gray size-16 gotham-bold">
				<?php echo get_the_date($date_template); ?>
			</date>

			<h2 class="size-30 size-sm-40 size-lg-48 overflow-x-hidden gotham-bold color-blue d-block pb-4">
				<?= the_title()?>
			</h2>

			<?php if(!empty(get_the_post_thumbnail_url())): ?>

				<div class="text-center post-thumbnail">
					<img src="<?= get_the_post_thumbnail_url()?>" class="d-inline-block img-fluid"/>
				</div>

			<?php endif; ?>
		
		<?php endif; ?>

        <?= the_content() ?>

    </article>

</section>

<?php endwhile; ?>

<?php else: ?>

<?php endif; ?>

<?php get_footer() ?>