<?php /* Template Name: Conteudo Principal */  ?>

<?php get_header() ?>

<?php get_template_part('template-parts/post/banner') ?>

<section id="content-principal" class="pt-3">

<div class="container color-gray">

    <div class="d-flex justify-content-end"> <a href="<?php echo get_home_url() . '/conteudo-principal'; ?>"> < Voltar</a></div>
    <h3 class="size-30 avenir-light color-gray mb-4 d-none d-lg-block">
      Resultado da busca para: <span class="color-blue"><?php the_search_query(); ?></span>
    </h3>

  <div class="row pt-5">
    
    <?php if ( have_posts() ): ?>
    
      <?php while (have_posts()) : the_post(); ?>

      <div class="col-12 col-md-6 mb-5 ">

        <article class="row h-100">

          <div class="col-12 col-xl-7">
            <a href="<?= get_permalink();?>" class="d-block principal-img">
              <img src="<?= get_the_post_thumbnail_url( $post->ID, 'principal-size' ); ?>" />
            </a>
          </div>

          <div class="col-12 col-xl-5">

            <a href="<?= get_permalink();?>" class="color-gray avenir-medium pb-3 mt-3 mt-xl-0 lh-1 d-block">
              <?php the_title(); ?>
            </a>

            <p class="size-14 avenir-light mb-3 lh-1"><?= get_the_excerpt(); ?></p>

            <div class="">
              <a href="<?= get_permalink();?>" class="btn st-product-add-to-cart">SAIBA MAIS!</a>
            </div>

          </div>

        </article>

      </div>

      <?php endwhile; ?>
      <?php the_posts_pagination();?>
    <?php else: ?>


    <?php endif; ?>
    <?php wp_reset_query();?>

  </div>

</div>
</section>

<?php get_footer() ?>