<?php

    define('THEME_URI', get_stylesheet_directory_uri().'/');
    define('THEME_ASSETS',   THEME_URI . 'assets/');
    define('THEME_IMG',   THEME_ASSETS . 'img/');
    define('THEME_FONTS',  THEME_ASSETS . 'fonts/');
    define('THEME_CSS',    THEME_ASSETS . 'css/');
    define('THEME_JS',     THEME_ASSETS . 'js/');

    add_theme_support('post-thumbnails');
    /*Registrando menu*/
    add_theme_support('menus');
    register_nav_menus([
        'category-menu' => 'Menu de categorias', 
        'sector-menu' => 'Menu de setores',
        'top-list-menu' => 'Lista em destaque do topo',
        'bottom-menu-1' => 'Menu do rodape 1',
        'bottom-menu-2' => 'Menu do rodape 2',
        'bottom-menu-3' => 'Menu do rodape 3'

    ]);
	
	add_image_size( 'blog-size', 390, 260, true );
	add_image_size( 'product-size', 390, 200, true );
	add_image_size( 'product-featured-size', 340, 180, true );
	add_image_size( 'principal-size', 540, 282, true );
	
	add_theme_support( 'woocommerce' );
	
	/**
 * Posttypes
 *
 * @package understrap
 */

//Cidades
if( ! function_exists( 'cidades_post_type' ) ) :
	function cidades_post_type() {
		$labels = array(
			'name' => 'Cidades',
			'singular_name' => 'Cidade',
			'add_new' => 'Adicionar Cidade',
			'all_items' => 'Todos os Cidades',
			'add_new_item' => 'Adicionar Cidade',
			'edit_item' => 'Editar Cidade',
			'new_item' => 'Novo Cidade',
			'view_item' => 'Ver Cidade',
			'search_items' => 'Buscar Cidades',
			'not_found' => 'Nenhum Cidade encontrado',
			'not_found_in_trash' => 'Nenhum Cidade encontrado na lixeira',
			'parent_item_colon' => 'Parent Cidade'
			//'menu_name' => default to 'name'
		);
		$args = array(
			'labels' => $labels,
			'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
			'publicly_queryable' => true,  // you should be able to query it
			'show_ui' => true,  // you should be able to edit it in wp-admin
			'exclude_from_search' => true,  // you should exclude it from search results
			'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
			'has_archive' => true,  // it shouldn't have archive page
			'supports' => array(
				'title',
				//'editor',
				//'thumbnail',
				//'excerpt',
				//'author',
				//'trackbacks',
				//'custom-fields',
				//'comments',
				//'revisions',
				//'page-attributes', // (menu order, hierarchical must be true to show Parent option)
				//'post-formats',
			),
			'menu_position' => 7,
		);
		register_post_type( 'cidade', $args );
		//flush_rewrite_rules();
		
	}
	add_action( 'init', 'cidades_post_type' );
endif; // end of function_exists()

//Cidades
if( ! function_exists( 'categorias_produto_type' ) ) :
	function categorias_produto_type() {
			
		register_taxonomy( 'tipos-de-produtos', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'default_term' => 'compra-direta',
				'show_admin_column' => false,
				'labels' => array(
					'name' => 'Tipos de produtos',
					'singular_name' => 'Tipo de produto',
				)
			)
		);
		
		register_taxonomy( 'setores', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'labels' => array(
					'name' => 'Setores',
					'singular_name' => 'Setor',
				)
			)
		);
		
		register_taxonomy( 'perfis', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'labels' => array(
					'name' => 'Perfis',
					'singular_name' => 'Perfil',
				)
			)
		);		
				
		
	}
	add_action( 'init', 'categorias_produto_type' );
endif; // end of function_exists()

/* Apenas o Estado da Bahia */
 
function aelia_checkout_shipping_filter_BA_states() {
   if ( !is_checkout() && !is_account_page() && !is_page(211) ) {
      return;
   }
   ?>
      
   <script>
   jQuery(document).ready(function($) {
 
      $(document.body).on('country_to_state_changed', function() {
 
         function set_shipping_state(state) {
            var $shipping_state = $('#billing_state');
            var $shipping_state_option = $('#billing_state option[value="' + state + '"]');
            var $shipping_state_option_no = $('#billing_state option[value!="' + state + '"]');
            $shipping_state_option_no.remove();
            $shipping_state_option.attr('selected', true);
         }
 
         var $shipping_country = $('#billing_country');
 
         var new_shipping_state = '';
 
         switch($shipping_country.val()) {
            case 'BR':
               new_shipping_state = 'BA';
               break;
         }
 
         if( ! $.isEmptyObject(new_shipping_state)) {
            set_shipping_state(new_shipping_state);
         } 
 
      });	  
 
   });  
   </script>
       
   <?php
}

add_action( 'wp_footer', 'aelia_checkout_shipping_filter_BA_states' );

/* Enviar e-mail do pedido de compra para o gestor */

/*function sv_conditional_email_recipient( $recipient, $order ) {

	// Bail on WC settings pages since the order object isn't yet set yet
	// Not sure why this is even a thing, but shikata ga nai
	$page = $_GET['page'] = isset( $_GET['page'] ) ? $_GET['page'] : '';
	if ( 'wc-settings' === $page ) {
		return $recipient; 
	}
	
	// just in case
	if ( ! $order instanceof WC_Order ) {
		return $recipient; 
	}

	$billing_city = $order->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$email = get_field('emails_de_destinatarios_cidade', $exist_city->ID );
        $recipient .= ','.$email;
	}
	
	return $recipient;
}
add_filter( 'woocommerce_email_recipient_new_order', 'sv_conditional_email_recipient', 10, 2 );*/

/* Enviar e-mail do pedido de orçamento para o gestor */

function add_cc_to_wc_order_emails( $header, $mail_id, $mail_obj ) {
    //$available_for = array( 'customer_on_hold_order', 'customer_processing_order', 'customer_completed_order' );
    
	$admin_emails = array( 
		'new_order',
        'adq_admin_new_quote'
    );
	
	$billing_city = $mail_obj->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$email = get_field('emails_de_destinatarios_cidade', $exist_city->ID );
	}
	
	if( in_array( $mail_id, $admin_emails ) ){

		$header .= 'Cc: ' . $email . "\r\n";
	
	}

	return $header;
	
}
add_filter( 'woocommerce_email_headers', 'add_cc_to_wc_order_emails', 99, 3 );


/* Adicionar os gestores regionais aos pedidos */

function before_checkout_create_order($order, $data) {

	$billing_city = $order->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$store_manager_id = get_field('gerentes_de_pedidos_cidade', $exist_city->ID);
	}

	$order->update_meta_data('gerentes_do_pedido_pedido', $store_manager_id);
}
add_action('woocommerce_checkout_create_order', 'before_checkout_create_order', 20, 2);

/* Mostrar os pedidos apenas para os gestores regionais */

function custom_admin_shop_manager_orders($query) {
	global $pagenow;
	$qv = &$query->query_vars;

	$currentUserRoles = wp_get_current_user()->roles;
	$user_id = get_current_user_id();

	if (in_array('gestor', $currentUserRoles)) {
		if ( $pagenow == 'edit.php' && isset($qv['post_type']) && $qv['post_type'] == 'shop_order' ) {
					
			$query->set( 'meta_query', array(
				array(
					'key'     => 'gerentes_do_pedido_pedido',
					'value'   => $user_id,
					'compare' => 'LIKE',
				)
			) );
			
		}
	}

	return $query;
}
add_filter('pre_get_posts', 'custom_admin_shop_manager_orders');

/* Validação do termo */

function add_fake_error($posted) {
    if ($_POST['confirm-order-flag'] == "1") {
        wc_add_notice( __( "custom_notice"), 'error');
    } 
}

add_action('woocommerce_after_checkout_validation', 'add_fake_error');

/* Remove safona termos e condições */

function golden_oak_web_design_woocommerce_checkout_terms_and_conditions() {
  remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
}
add_action( 'wp', 'golden_oak_web_design_woocommerce_checkout_terms_and_conditions' );

function wpa83367_price_html( $price, $product ){
    return 'De ' . str_replace( '<ins>', ' Por até:<ins>', $price );
}

//add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );

/* Add categoria produto content loop */

function add_produto_categoria() {
	global $post;
	
	$cat_id = get_post_meta( $post->ID, 'epc_primary_' . 'product_cat', true );
	if($cat_id){
		$category_detail = get_term_by('id', $cat_id, 'product_cat');
		$category_link = get_category_link( $cat_id );
		echo '<a class="st-product-tag" href="'.$category_link.'">'.$category_detail->name.'</a>';
	} else{
		$count = 1; 
		$terms = get_the_terms($post->ID, 'product_cat' );
		if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
			foreach ($terms as $term) {
				if($term->slug != 'top-10-mais-vendidos'){
					if($count == 1){
						echo '<a class="st-product-tag" href="'.get_term_link($term->slug, 'product_cat').'">'.$term->name.'</a>';
					}
					$count++; 
				}
			}
		}
	}
	
}

add_action('woocommerce_before_shop_loop_item', 'add_produto_categoria', 5);

/* Add frase de efeito produto content loop */

function add_produto_frase_efeito() {
	global $post;
	echo '<p class="frase-efeito color-gray mb-2 size-14">'.get_field('phrase', $post->ID).'</p>';
}

add_action('woocommerce_shop_loop_item_title', 'add_produto_frase_efeito', 20);

/* Add texto parcelamento produto content loop */

function add_produto_frase_parcelamento() {
	echo '<div class="divide-box">';
	echo '<div class="size-14 color-gray avenir-medium">Divida sem juros no cartão</div>';
}

add_action('woocommerce_after_shop_loop_item_title', 'add_produto_frase_parcelamento', 15);

/* Add label top 10 produtos produto content loop */

function add_produto_top_10_produtos() {
	global $post;
	if( has_term( 'top-10-mais-vendidos', 'product_cat', $post ) ){
		echo '<div class="color-red size-12 avenir-medium oblique"><span class="icon-star-full"></span> TOP 10 MAIS VENDIDOS</div>';
	}
	echo '</div>';
}

add_action('woocommerce_after_shop_loop_item_title', 'add_produto_top_10_produtos', 20);

/* Add botão de comprar */

function add_produto_botao_compra() {
	global $post;
	global $woocommerce_loop;
	if ( is_product() && $woocommerce_loop['name'] == 'related' ) {
			echo '<div class="d-flex align-items-center justify-content-center mt-3">';                
			echo '<a href="'.get_permalink( $post->ID ).'" class="btn st-product-add-to-cart">TAMBEM QUERO!</a>';
			echo '</div>';
	} else{
		if( has_term( 'orcamento', 'tipos-de-produtos', $post ) ){
			echo '<div class="d-flex align-items-center justify-content-center mt-3">';                
			echo '<a href="'.get_permalink( $post->ID ).'" class="btn st-product-add-to-cart">SOLICITAR PROPOSTA</a>';
			echo '</div>';
		}else{
			echo '<div class="d-flex align-items-center justify-content-center mt-3">';                
			echo '<a href="'.get_permalink( $post->ID ).'" class="btn st-product-add-to-cart">COMPRAR</a>';
			echo '</div>';
		}	
	}
	
}

add_action('woocommerce_after_shop_loop_item', 'add_produto_botao_compra', 10);

/* Remove itens */

remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);

remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart', 10);

remove_action('woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash', 10);

remove_action('woocommerce_single_product_summary','woocommerce_template_single_price', 10);

remove_action('woocommerce_single_product_form','woocommerce_template_single_price', 10);

remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);

remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering', 30);

// adds the suffix to the regular price (the <del> element) if the product is on sale

function change_format_sale_price( $price_html, $regular_price, $sale_price ) {
    $price_html = '<span class="preco-normal size-16 avenir-medium"><span class="preco-text">De</span> <del>' . ( is_numeric( $regular_price ) ? wc_price( $regular_price ) : $regular_price ) . '</del></span> <span class="preco-normal-desconto size-24 avenir-black color-gray mb-1"><span class="preco-text">Por até</span> <ins>' . ( is_numeric( $sale_price ) ? wc_price( $sale_price ) : $sale_price ) . '</ins></span>';
    return $price_html;
}
add_filter( 'woocommerce_format_sale_price', 'change_format_sale_price', 99, 3 );

// Adicionar form para single do produto
add_action( 'woocommerce_single_product_form', 'woocommerce_template_single_add_to_cart', 30 );


function custom_remove_all_quantity_fields( $return, $product ) {return true;}
//add_filter( 'woocommerce_is_sold_individually','custom_remove_all_quantity_fields', 10, 2 );


/* Remove tipos de produtos */

function remove_product_types( $types ){
    unset( $types['grouped'] );
    unset( $types['external'] );
	unset( $types['variable'] );
    return $types;
}

add_filter( 'product_type_selector', 'remove_product_types' );

/* Remove virtual e download checkbox */

add_filter( 'product_type_options', function( $options ) {

	// remove "Virtual" checkbox
	if( isset( $options[ 'virtual' ] ) ) {
		unset( $options[ 'virtual' ] );
	}

	// remove "Downloadable" checkbox
	if( isset( $options[ 'downloadable' ] ) ) {
		unset( $options[ 'downloadable' ] );
	}

	return $options;

} );

/* Senha Forte */

function reduce_min_strength_password_requirement( $strength ) {
    // 3 => Strong (default) | 2 => Medium | 1 => Weak | 0 => Very Weak (anything).
    return 0; 
}
add_filter( 'woocommerce_min_password_strength', 'reduce_min_strength_password_requirement' );

/* Remove Tags */
/**
 * Overwrite product_tag taxonomy properties to effectively hide it from WP admin ..
 */
add_action('init', function() {
    register_taxonomy('product_tag', 'product', [
        'public'            => false,
        'show_ui'           => false,
        'show_admin_column' => false,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
    ]);
}, 100);

/**
 * .. and also remove the column from Products table - it's also hardcoded there.
 */
add_action( 'admin_init' , function() {
    add_filter('manage_product_posts_columns', function($columns) {
        unset($columns['product_tag']);
        return $columns;
    }, 100);
});

/* Texto da busca */

function better_woocommerce_search_result_title( $page_title )
{
	if ( is_search() ) {
		if (! get_search_query()) {
			$page_title = sprintf( __( 'Resultado da busca para <span class="search-title color-blue">"Todos produtos"</span>', 'woocommerce' ), get_search_query() );
		} else {
			$page_title = sprintf( __( 'Resultado da busca para <span class="search-title color-blue">%s</span>', 'woocommerce' ), get_search_query() );
		}
	}
	return $page_title;
}

add_filter( 'woocommerce_page_title', 'better_woocommerce_search_result_title', 10, 1 );

/* Adicionar CPF no pedido */
function your_custom_field_function_name($order){
	echo '<p><strong>CPF:</strong> ' . get_post_meta($order->id, '_billing_cpf', true) . '</p>';
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'your_custom_field_function_name', 10, 1 );


// Account Edit Adresses: Reorder billing email and phone fields

function custom_billing_fields( $fields ) {
    // Only on account pages
    if( ! is_account_page() ) return $fields;

    ## ---- 2.  Sort billing email and phone fields ---- ##

	$fields['billing_cpf']['priority'] = 23;
    $fields['billing_cpf']['class'] = array('form-row-first');
	$fields['billing_cpf']['required'] = true;
	$fields['billing_sex']['priority'] = 24;
    $fields['billing_sex']['class'] = array('form-row-last');
	
	$fields['billing_birthdate']['priority'] = 25;
    $fields['billing_birthdate']['class'] = array('form-row-first');
	$fields['billing_email']['priority'] = 26;
    $fields['billing_email']['class'] = array('form-row-last');
	
	$fields['billing_company']['priority'] = 27;
    $fields['billing_company']['class'] = array('form-row-first');
	$fields['billing_company']['label'] = 'Razão Social';
	$fields['billing_company']['required'] = true;
	$fields['billing_cnpj']['priority'] = 28;
    $fields['billing_cnpj']['class'] = array('form-row-last');
	$fields['billing_cnpj']['required'] = true;
	
	$fields['billing_perfil']['priority'] = 29;
    $fields['billing_perfil']['class'] = array('form-row-wide');	
	$fields['billing_perfil']['label'] = 'Qual o seu perfil empresarial?';
	$fields['billing_perfil']['required'] = true;
	$fields['billing_perfil']['type'] = 'select';
	$fields['billing_perfil']['options'] = array(
		'' => 'Selecione seu perfil',
		'Microempreendedor Individual' => 'Microempreendedor Individual',
		'Microempresa' => 'Microempresa',
		'Empresa de Pequeno Porte' => 'Empresa de Pequeno Porte',
		'Produtor Rural' => 'Produtor Rural',
	);
	
	$fields['billing_setor']['priority'] = 30;
    $fields['billing_setor']['class'] = array('form-row-wide');	
	$fields['billing_setor']['label'] = 'Setor';
	$fields['billing_setor']['required'] = true;
	$fields['billing_setor']['type'] = 'select';
	$fields['billing_setor']['options'] = array(
		'' => 'Selecione o setor',
		'Agronegócio' => 'Agronegócio',
		'Comércio' => 'Comércio',
		'Economia Criativa' => 'Economia Criativa',
		'Indústria' => 'Indústria',
		'Serviço' => 'Serviço',
		'Turismo' => 'Turismo',
	);	
	
	/*$fields['billing_segmento']['priority'] = 31;
    $fields['billing_segmento']['class'] = array('form-row-wide');	
	$fields['billing_segmento']['label'] = 'Segmento';
	$fields['billing_segmento']['required'] = true;
	$fields['billing_segmento']['type'] = 'select';
	$fields['billing_segmento']['options'] = array(
		'' => 'Selecione o segmento',
		'Design' => 'Design',
		'Desenvolvimento Tecnológico' => 'Desenvolvimento Tecnológico',
		'Inovação' => 'Inovação',
		'Produtividade' => 'Produtividade',
		'Produtividade-intelectual' => 'Produtividade-intelectual',
		'Qualidade' => 'Qualidade',
		'Serviços Digitais' => 'Serviços Digitais',
		'Sustentabilidade' => 'Sustentabilidade',
	);		*/
	
	$fields['billing_postcode']['priority'] = 32;
    $fields['billing_postcode']['class'] = array('form-row-first');
	$fields['billing_address_1']['priority'] = 33;
    $fields['billing_address_1']['class'] = array('form-row-last');
	
	$fields['billing_number']['priority'] = 34;
    $fields['billing_number']['class'] = array('form-row-first');
	$fields['billing_neighborhood']['priority'] = 35;
    $fields['billing_neighborhood']['class'] = array('form-row-last');
		
	$fields['billing_address_2']['priority'] = 36;
    $fields['billing_address_2']['class'] = array('form-row-wide');
	$fields['billing_address_2']['label'] = 'Complemento';
	$fields['billing_address_2']['label_class'] = '';
	
	$fields['billing_state']['priority'] = 37;
    $fields['billing_state']['class'] = array('form-row-first');
	$fields['billing_city']['priority'] = 38;
    $fields['billing_city']['class'] = array('form-row-last');

	$fields['billing_phone']['priority'] = 39;
    $fields['billing_phone']['class'] = array('form-row-first');
	$fields['billing_cellphone']['priority'] = 40;
    $fields['billing_cellphone']['class'] = array('form-row-last');
	$fields['billing_cellphone']['required'] = true;

    return $fields;
}

add_filter(  'woocommerce_billing_fields', 'custom_billing_fields', 20, 1 );

/* Numero de produtos relacionados */

function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 8; // 4 related products
	$args['columns'] = 8; // arranged in 2 columns
	return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );


function rename_order_statuses( $order_statuses ) {
    $order_statuses['wc-request']  = _x( 'Pedido', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal']  = _x( 'Proposta', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal-sent']  = _x( 'Proposta enviada', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal-expired']  = _x( 'Proposta expirada', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal-rejected']  = _x( 'Proposta rejeitada', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal-canceled']  = _x( 'Proposta cancelada', 'Order status', 'woocommerce' );
	$order_statuses['wc-proposal-accepted']  = _x( 'Proposta aceita', 'Order status', 'woocommerce' );

    return $order_statuses;
}

add_filter( 'wc_order_statuses', 'rename_order_statuses', 20, 1 );

/**
 * @desc Remove in all product type
 */
function wc_remove_all_quantity_fields( $return, $product ) {
    return true;
}
add_filter( 'woocommerce_is_sold_individually', 'wc_remove_all_quantity_fields', 10, 2 );

/* Checkbox */

/**
 * Add WooCommerce additional Checkbox checkout field
 */
function bt_add_checkout_checkbox() {
   
    woocommerce_form_field( 'empresa_checkbox', array( // CSS ID
       'type'          => 'checkbox',
       'class'         => array('form-row mycheckbox'), // CSS Class
       'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
       'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
       'required'      => true, // Mandatory or Optional
       'label'         => 'Estou ciente que a compra é exclusiva para MEI, Micro ou Pequenas Empresas do estado da Bahia.', // Label and Link
    ));    
}

add_action( 'woocommerce_checkout_terms_and_conditions', 'bt_add_checkout_checkbox', 100 );

add_action( 'woocommerce_quotation_terms', 'bt_add_checkout_checkbox', 100 );

/**
 * Alert if checkbox not checked
 */ 
function bt_add_checkout_checkbox_warning() {
    if ( ! (int) isset( $_POST['empresa_checkbox'] ) ) {
        wc_add_notice( __( 'Marque a opção confirmando que está ciente dos nossos termos regionais e empresariais.' ), 'error' );
    }
}

add_action( 'woocommerce_checkout_process', 'bt_add_checkout_checkbox_warning', 100 );

/**
 * Add custom field as order meta with field value to database
 */
function bt_checkout_field_order_meta_db( $order_id ) {
    if ( ! empty( $_POST['empresa_checkbox'] ) ) {
        update_post_meta( $order_id, 'empresa_checkbox', sanitize_text_field( $_POST['empresa_checkbox'] ) );
    }
}	

add_action( 'woocommerce_checkout_update_order_meta', 'bt_checkout_field_order_meta_db' );

/**
 * Display field value on the backend WooCOmmerce order
 */
function bt_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Empresa Checkbox Label').':</strong> ' . get_post_meta( $order->get_id(), 'empresa_checkbox', true ) . '<p>'; 
}

//add_action( 'woocommerce_admin_order_data_after_billing_address', 'bt_checkout_field_display_admin_order_meta', 10, 1 );

/* Apenas posts publicados */
function filter_acf_relationship ($args, $field, $post_id) {
    $args['post_status'] = 'publish';
    return $args;
}

add_filter('acf/fields/relationship/query', 'filter_acf_relationship', 10, 3);

/* Remove Dashboard Widgets */
function remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}

if (!current_user_can('manage_options')) {
	add_action('admin_init', 'remove_dashboard_meta' );
}

/* Gestor redirect */
function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    global $user;
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {

        if ( in_array( 'gestor', $user->roles ) ) {
            // redirect them to the default place
            return admin_url( '/edit.php?post_type=shop_order' );
        } else{
			return admin_url();
		}
    } 
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

/* Remove Obrigatoriedade Nome de exibição */
function wc_save_account_details_required_fields( $required_fields ){
    unset( $required_fields['account_display_name'] );
    return $required_fields;
}
add_filter('woocommerce_save_account_details_required_fields', 'wc_save_account_details_required_fields' );

/* JS CEP */
 
function cep_orcamento() {
   if ( is_page(211) || is_account_page()) {?>
		<script type="text/javascript" src="<?php echo get_home_url();?>/wp-content/plugins/autocomplete-address-for-woocommerce/assets/js/autocomplete-address.4dd81181ce7c15cd78b1.js?ver=1.1.0" id="wc-autocomplete-address-js"></script>
		<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js'></script>

		<script>

		jQuery( function( $ ) {
			maskPhone( '#billing_phone, #billing_cellphone' );
			$( '#billing_birthdate' ).mask( '00/00/0000' );
			$( '#billing_postcode' ).mask( '00000-000' );
			$( '#billing_phone, #billing_cellphone, #billing_birthdate, #billing_postcode' ).attr( 'type', 'tel' );
			$( '#billing_cpf, #credit-card-cpf' ).mask( '000.000.000-00' );
			$( '#billing_cnpj' ).mask( '00.000.000/0000-00' );
			function maskPhone(selector) {
				var $element = $(selector),
						MaskBehavior = function(val) {
							return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
						},
						maskOptions = {
							onKeyPress: function(val, e, field, options) {
								field.mask(MaskBehavior.apply({}, arguments), options);
							}
						};

				$element.mask(MaskBehavior, maskOptions);
			}
		});

		</script>
	<?php

   }
}

add_action( 'wp_footer', 'cep_orcamento' );


/* Validar CPF Orcamento */
function cs_wc_validate_orcamento() {
	
	function is_cpf_orc( $cpf ) {
		$cpf = preg_replace( '/[^0-9]/', '', $cpf );

		if ( 11 !== strlen( $cpf ) || preg_match( '/^([0-9])\1+$/', $cpf ) ) {
			return false;
		}

		$digit = substr( $cpf, 0, 9 );

		for ( $j = 10; $j <= 11; $j++ ) {
			$sum = 0;

			for ( $i = 0; $i < $j - 1; $i++ ) {
				$sum += ( $j - $i ) * intval( $digit[ $i ] );
			}

			$summod11 = $sum % 11;
			$digit[ $j - 1 ] = $summod11 < 2 ? 0 : 11 - $summod11;
		}

		return intval( $digit[9] ) === intval( $cpf[9] ) && intval( $digit[10] ) === intval( $cpf[10] );
	}
	
	if ( is_checkout() ) {
		
		if ( isset( $_POST['billing_cpf'] ) && ! empty( $_POST['billing_cpf'] ) && !is_cpf_orc( $_POST['billing_cpf'] ) ) {
			wc_add_notice( sprintf( '<strong>%s</strong> %s.', __( 'CPF', 'woocommerce-extra-checkout-fields-for-brazil' ), __( 'is not valid', 'woocommerce-extra-checkout-fields-for-brazil' ) ), 'error' );
		}
	}
	
}

add_action( 'woocommerce_checkout_process', 'cs_wc_validate_orcamento', 100 );


// or add_action( 'woocommerce_edit_account_form_start', 'misha_add_field_edit_account_form' );
function misha_add_field_edit_account_form() {

	woocommerce_form_field(
        'billing_first_name',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'Nome'
        ),
        ( isset($_POST['billing_first_name']) ? $_POST['billing_first_name'] : '' )
    );
	
	woocommerce_form_field(
        'billing_last_name',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
            'required'    => true,
            'label'       => 'Sobrenome'
        ),
        ( isset($_POST['billing_last_name']) ? $_POST['billing_last_name'] : '' )
    );	

	woocommerce_form_field(
        'billing_cpf',
        array(
            'type'        => 'tel',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'CPF'
        ),
        ( isset($_POST['billing_cpf']) ? $_POST['billing_cpf'] : '' )
    );	
	
	woocommerce_form_field(
        'billing_sex',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
			'options' => array('' => 'Selecionar', 'Feminino' => 'Feminino', 'Masculino' => 'Masculino',),
            'required'    => true,
            'label'       => 'Sexo'
        ),
        ( isset($_POST['billing_sex']) ? $_POST['billing_sex'] : '' )
    );		
	
	woocommerce_form_field(
        'billing_company',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'Razão Social'
        ),
        ( isset($_POST['billing_company']) ? $_POST['billing_company'] : '' )
    );		
	
	woocommerce_form_field(
        'billing_cnpj',
        array(
            'type'        => 'tel',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
            'required'    => true,
            'label'       => 'CNPJ'
        ),
        ( isset($_POST['billing_cnpj']) ? $_POST['billing_cnpj'] : '' )
    );	
	
	woocommerce_form_field(
        'billing_perfil',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide'),
			'options' => array(
				'' => 'Selecione seu perfil',
				'Microempreendedor Individual' => 'Microempreendedor Individual',
				'Microempresa' => 'Microempresa',
				'Empresa de Pequeno Porte' => 'Empresa de Pequeno Porte',
				'Produtor Rural' => 'Produtor Rural',
			),
            'required'    => true,
            'label'       => 'Qual o seu perfil empresarial?'
        ),
        ( isset($_POST['billing_perfil']) ? $_POST['billing_perfil'] : '' )
    );
	
	woocommerce_form_field(
        'billing_setor',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide'),
			'options' => array(
				'' => 'Selecione o setor',
				'Agronegócio' => 'Agronegócio',
				'Comércio' => 'Comércio',
				'Economia Criativa' => 'Economia Criativa',
				'Indústria' => 'Indústria',
				'Serviço' => 'Serviço',
				'Turismo' => 'Turismo',
			),
            'required'    => true,
            'label'       => 'Setor'
        ),
        ( isset($_POST['billing_setor']) ? $_POST['billing_setor'] : '' )
    );

	/*woocommerce_form_field(
        'billing_segmento',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide'),
			'options' => array(
				'' => 'Selecione o segmento',
				'Design' => 'Design',
				'Desenvolvimento Tecnológico' => 'Desenvolvimento Tecnológico',
				'Inovação' => 'Inovação',
				'Produtividade' => 'Produtividade',
				'Produtividade-intelectual' => 'Produtividade-intelectual',
				'Qualidade' => 'Qualidade',
				'Serviços Digitais' => 'Serviços Digitais',
				'Sustentabilidade' => 'Sustentabilidade',
			),
            'required'    => true,
            'label'       => 'Segmento'
        ),
        ( isset($_POST['billing_segmento']) ? $_POST['billing_segmento'] : '' )
    );	*/
	
	woocommerce_form_field(
        'billing_postcode',
        array(
            'type'        => 'tel',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'CEP'
        ),
        ( isset($_POST['billing_postcode']) ? $_POST['billing_postcode'] : '' )
    );			
	
	woocommerce_form_field(
        'billing_address_1',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
            'required'    => true,
            'label'       => 'Endereço'
        ),
        ( isset($_POST['billing_address_1']) ? $_POST['billing_address_1'] : '' )
    );	
	
	woocommerce_form_field(
        'billing_number',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'Número'
        ),
        ( isset($_POST['billing_number']) ? $_POST['billing_number'] : '' )
    );		
	
	woocommerce_form_field(
        'billing_neighborhood',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
            'required'    => false,
            'label'       => 'Bairro'
        ),
        ( isset($_POST['billing_neighborhood']) ? $_POST['billing_neighborhood'] : '' )
    );		
	
	woocommerce_form_field(
        'billing_address_2',
        array(
            'type'        => 'text',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide'),
            'required'    => false,
            'label'       => 'Complemento'
        ),
        ( isset($_POST['billing_address_2']) ? $_POST['billing_address_2'] : '' )
    );	
	
	woocommerce_form_field(
        'billing_state',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
			'options' => array(
				'BA' => 'Bahia',
			),
            'required'    => true,
            'label'       => 'Estado'
        ),
        ( isset($_POST['billing_state']) ? $_POST['billing_state'] : '' )
    );	
	
	woocommerce_form_field(
        'billing_city',
        array(
            'type'        => 'select',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
			'options' => array('Abaíra' => 'Abaíra', 'Abaré' => 'Abaré', 'Acajutiba' => 'Acajutiba', 'Adustina' => 'Adustina', 'Água Fria' => 'Água Fria', 'Aiquara' => 'Aiquara', 'Alagoinhas' => 'Alagoinhas', 'Alcobaça' => 'Alcobaça', 'Almadina' => 'Almadina', 'Amargosa' => 'Amargosa', 'Amélia Rodrigues' => 'Amélia Rodrigues', 'América Dourada' => 'América Dourada', 'Anagé' => 'Anagé', 'Andaraí' => 'Andaraí', 'Andorinha' => 'Andorinha', 'Angical' => 'Angical', 'Anguera' => 'Anguera', 'Antas' => 'Antas', 'Antônio Cardoso' => 'Antônio Cardoso', 'Antônio Gonçalves' => 'Antônio Gonçalves', 'Aporá' => 'Aporá', 'Apuarema' => 'Apuarema', 'Araçás' => 'Araçás', 'Aracatu' => 'Aracatu', 'Araci' => 'Araci', 'Aramari' => 'Aramari', 'Arataca' => 'Arataca', 'Aratuípe' => 'Aratuípe', 'Aurelino Leal' => 'Aurelino Leal', 'Baianópolis' => 'Baianópolis', 'Baixa Grande' => 'Baixa Grande', 'Banzaê' => 'Banzaê', 'Barra' => 'Barra', 'Barra da Estiva' => 'Barra da Estiva', 'Barra do Choça' => 'Barra do Choça', 'Barra do Mendes' => 'Barra do Mendes', 'Barra do Rocha' => 'Barra do Rocha', 'Barreiras' => 'Barreiras', 'Barro Alto' => 'Barro Alto', 'Barro Preto' => 'Barro Preto', 'Barrocas' => 'Barrocas', 'Belmonte' => 'Belmonte', 'Belo Campo' => 'Belo Campo', 'Biritinga' => 'Biritinga', 'Boa Nova' => 'Boa Nova', 'Boa Vista do Tupim' => 'Boa Vista do Tupim', 'Bom Jesus da Lapa' => 'Bom Jesus da Lapa', 'Bom Jesus da Serra' => 'Bom Jesus da Serra', 'Boninal' => 'Boninal', 'Bonito' => 'Bonito', 'Boquira' => 'Boquira', 'Botuporã' => 'Botuporã', 'Brejões' => 'Brejões', 'Brejolândia' => 'Brejolândia', 'Brotas de Macaúbas' => 'Brotas de Macaúbas', 'Brumado' => 'Brumado', 'Buerarema' => 'Buerarema', 'Buritirama' => 'Buritirama', 'Caatiba' => 'Caatiba', 'Cabaceiras do Paraguaçu' => 'Cabaceiras do Paraguaçu', 'Cachoeira' => 'Cachoeira', 'Caculé' => 'Caculé', 'Caém' => 'Caém', 'Caetanos' => 'Caetanos', 'Caetité' => 'Caetité', 'Cafarnaum' => 'Cafarnaum', 'Cairu' => 'Cairu', 'Caldeirão Grande' => 'Caldeirão Grande', 'Camacan' => 'Camacan', 'Camaçari' => 'Camaçari', 'Camamu' => 'Camamu', 'Campo Alegre de Lourdes' => 'Campo Alegre de Lourdes', 'Campo Formoso' => 'Campo Formoso', 'Canápolis' => 'Canápolis', 'Canarana' => 'Canarana', 'Canavieiras' => 'Canavieiras', 'Candeal' => 'Candeal', 'Candeias' => 'Candeias', 'Candiba' => 'Candiba', 'Cândido Sales' => 'Cândido Sales', 'Cansanção' => 'Cansanção', 'Canudos' => 'Canudos', 'Capela do Alto Alegre' => 'Capela do Alto Alegre', 'Capim Grosso' => 'Capim Grosso', 'Caraíbas' => 'Caraíbas', 'Caravelas' => 'Caravelas', 'Cardeal da Silva' => 'Cardeal da Silva', 'Carinhanha' => 'Carinhanha', 'Casa Nova' => 'Casa Nova', 'Castro Alves' => 'Castro Alves', 'Catolândia' => 'Catolândia', 'Catu' => 'Catu', 'Caturama' => 'Caturama', 'Central' => 'Central', 'Chorrochó' => 'Chorrochó', 'Cícero Dantas' => 'Cícero Dantas', 'Cipó' => 'Cipó', 'Coaraci' => 'Coaraci', 'Cocos' => 'Cocos', 'Conceição da Feira' => 'Conceição da Feira', 'Conceição do Almeida' => 'Conceição do Almeida', 'Conceição do Coité' => 'Conceição do Coité', 'Conceição do Jacuípe' => 'Conceição do Jacuípe', 'Conde' => 'Conde', 'Condeúba' => 'Condeúba', 'Contendas do Sincorá' => 'Contendas do Sincorá', 'Coração de Maria' => 'Coração de Maria', 'Cordeiros' => 'Cordeiros', 'Coribe' => 'Coribe', 'Coronel João Sá' => 'Coronel João Sá', 'Correntina' => 'Correntina', 'Cotegipe' => 'Cotegipe', 'Cravolândia' => 'Cravolândia', 'Crisópolis' => 'Crisópolis', 'Cristópolis' => 'Cristópolis', 'Cruz das Almas' => 'Cruz das Almas', 'Curaçá' => 'Curaçá', 'Dário Meira' => 'Dário Meira', 'Dias d\'Ávila' => 'Dias d\'Ávila', 'Dom Basílio' => 'Dom Basílio', 'Dom Macedo Costa' => 'Dom Macedo Costa', 'Elísio Medrado' => 'Elísio Medrado', 'Encruzilhada' => 'Encruzilhada', 'Entre Rios' => 'Entre Rios', 'Érico Cardoso' => 'Érico Cardoso', 'Esplanada' => 'Esplanada', 'Euclides da Cunha' => 'Euclides da Cunha', 'Eunápolis' => 'Eunápolis', 'Fátima' => 'Fátima', 'Feira da Mata' => 'Feira da Mata', 'Feira de Santana' => 'Feira de Santana', 'Filadélfia' => 'Filadélfia', 'Firmino Alves' => 'Firmino Alves', 'Floresta Azul' => 'Floresta Azul', 'Formosa do Rio Preto' => 'Formosa do Rio Preto', 'Gandu' => 'Gandu', 'Gavião' => 'Gavião', 'Gentio do Ouro' => 'Gentio do Ouro', 'Glória' => 'Glória', 'Gongogi' => 'Gongogi', 'Governador Mangabeira' => 'Governador Mangabeira', 'Guajeru' => 'Guajeru', 'Guanambi' => 'Guanambi', 'Guaratinga' => 'Guaratinga', 'Heliópolis' => 'Heliópolis', 'Iaçu' => 'Iaçu', 'Ibiassucê' => 'Ibiassucê', 'Ibicaraí' => 'Ibicaraí', 'Ibicoara' => 'Ibicoara', 'Ibicuí' => 'Ibicuí', 'Ibipeba' => 'Ibipeba', 'Ibipitanga' => 'Ibipitanga', 'Ibiquera' => 'Ibiquera', 'Ibirapitanga' => 'Ibirapitanga', 'Ibirapuã' => 'Ibirapuã', 'Ibirataia' => 'Ibirataia', 'Ibitiara' => 'Ibitiara', 'Ibititá' => 'Ibititá', 'Ibotirama' => 'Ibotirama', 'Ichu' => 'Ichu', 'Igaporã' => 'Igaporã', 'Igrapiúna' => 'Igrapiúna', 'Iguaí' => 'Iguaí', 'Ilhéus' => 'Ilhéus', 'Inhambupe' => 'Inhambupe', 'Ipecaetá' => 'Ipecaetá', 'Ipiaú' => 'Ipiaú', 'Ipirá' => 'Ipirá', 'Ipupiara' => 'Ipupiara', 'Irajuba' => 'Irajuba', 'Iramaia' => 'Iramaia', 'Iraquara' => 'Iraquara', 'Irará' => 'Irará', 'Irecê' => 'Irecê', 'Itabela' => 'Itabela', 'Itaberaba' => 'Itaberaba', 'Itabuna' => 'Itabuna', 'Itacaré' => 'Itacaré', 'Itaeté' => 'Itaeté', 'Itagi' => 'Itagi', 'Itagibá' => 'Itagibá', 'Itagimirim' => 'Itagimirim', 'Itaguaçu da Bahia' => 'Itaguaçu da Bahia', 'Itaju do Colônia' => 'Itaju do Colônia', 'Itajuípe' => 'Itajuípe', 'Itamaraju' => 'Itamaraju', 'Itamari' => 'Itamari', 'Itambé' => 'Itambé', 'Itanagra' => 'Itanagra', 'Itanhém' => 'Itanhém', 'Itaparica' => 'Itaparica', 'Itapé' => 'Itapé', 'Itapebi' => 'Itapebi', 'Itapetinga' => 'Itapetinga', 'Itapicuru' => 'Itapicuru', 'Itapitanga' => 'Itapitanga', 'Itaquara' => 'Itaquara', 'Itarantim' => 'Itarantim', 'Itatim' => 'Itatim', 'Itiruçu' => 'Itiruçu', 'Itiúba' => 'Itiúba', 'Itororó' => 'Itororó', 'Ituaçu' => 'Ituaçu', 'Ituberá' => 'Ituberá', 'Iuiú' => 'Iuiú', 'Jaborandi' => 'Jaborandi', 'Jacaraci' => 'Jacaraci', 'Jacobina' => 'Jacobina', 'Jaguaquara' => 'Jaguaquara', 'Jaguarari' => 'Jaguarari', 'Jaguaripe' => 'Jaguaripe', 'Jandaíra' => 'Jandaíra', 'Jequié' => 'Jequié', 'Jeremoabo' => 'Jeremoabo', 'Jiquiriçá' => 'Jiquiriçá', 'Jitaúna' => 'Jitaúna', 'João Dourado' => 'João Dourado', 'Juazeiro' => 'Juazeiro', 'Jucuruçu' => 'Jucuruçu', 'Jussara' => 'Jussara', 'Jussari' => 'Jussari', 'Jussiape' => 'Jussiape', 'Lafaiete Coutinho' => 'Lafaiete Coutinho', 'Lagoa Real' => 'Lagoa Real', 'Laje' => 'Laje', 'Lajedão' => 'Lajedão', 'Lajedinho' => 'Lajedinho', 'Lajedo do Tabocal' => 'Lajedo do Tabocal', 'Lamarão' => 'Lamarão', 'Lapão' => 'Lapão', 'Lauro de Freitas' => 'Lauro de Freitas', 'Lençóis' => 'Lençóis', 'Licínio de Almeida' => 'Licínio de Almeida', 'Livramento de Nossa Senhora' => 'Livramento de Nossa Senhora', 'Luís Eduardo Magalhães' => 'Luís Eduardo Magalhães', 'Macajuba' => 'Macajuba', 'Macarani' => 'Macarani', 'Macaúbas' => 'Macaúbas', 'Macururé' => 'Macururé', 'Madre de Deus' => 'Madre de Deus', 'Maetinga' => 'Maetinga', 'Maiquinique' => 'Maiquinique', 'Mairi' => 'Mairi', 'Malhada' => 'Malhada', 'Malhada de Pedras' => 'Malhada de Pedras', 'Manoel Vitorino' => 'Manoel Vitorino', 'Mansidão' => 'Mansidão', 'Maracás' => 'Maracás', 'Maragogipe' => 'Maragogipe', 'Maraú' => 'Maraú', 'Marcionílio Souza' => 'Marcionílio Souza', 'Mascote' => 'Mascote', 'Mata de São João' => 'Mata de São João', 'Matina' => 'Matina', 'Medeiros Neto' => 'Medeiros Neto', 'Miguel Calmon' => 'Miguel Calmon', 'Milagres' => 'Milagres', 'Mirangaba' => 'Mirangaba', 'Mirante' => 'Mirante', 'Monte Santo' => 'Monte Santo', 'Morpará' => 'Morpará', 'Morro do Chapéu' => 'Morro do Chapéu', 'Mortugaba' => 'Mortugaba', 'Mucugê' => 'Mucugê', 'Mucuri' => 'Mucuri', 'Mulungu do Morro' => 'Mulungu do Morro', 'Mundo Novo' => 'Mundo Novo', 'Muniz Ferreira' => 'Muniz Ferreira', 'Muquém de São Francisco' => 'Muquém de São Francisco', 'Muritiba' => 'Muritiba', 'Mutuípe' => 'Mutuípe', 'Nazaré' => 'Nazaré', 'Nilo Peçanha' => 'Nilo Peçanha', 'Nordestina' => 'Nordestina', 'Nova Canaã' => 'Nova Canaã', 'Nova Fátima' => 'Nova Fátima', 'Nova Ibiá' => 'Nova Ibiá', 'Nova Itarana' => 'Nova Itarana', 'Nova Redenção' => 'Nova Redenção', 'Nova Soure' => 'Nova Soure', 'Nova Viçosa' => 'Nova Viçosa', 'Novo Horizonte' => 'Novo Horizonte', 'Novo Triunfo' => 'Novo Triunfo', 'Olindina' => 'Olindina', 'Oliveira dos Brejinhos' => 'Oliveira dos Brejinhos', 'Ouriçangas' => 'Ouriçangas', 'Ourolândia' => 'Ourolândia', 'Palmas de Monte Alto' => 'Palmas de Monte Alto', 'Palmeiras' => 'Palmeiras', 'Paramirim' => 'Paramirim', 'Paratinga' => 'Paratinga', 'Paripiranga' => 'Paripiranga', 'Pau Brasil' => 'Pau Brasil', 'Paulo Afonso' => 'Paulo Afonso', 'Pé de Serra' => 'Pé de Serra', 'Pedrão' => 'Pedrão', 'Pedro Alexandre' => 'Pedro Alexandre', 'Piatã' => 'Piatã', 'Pilão Arcado' => 'Pilão Arcado', 'Pindaí' => 'Pindaí', 'Pindobaçu' => 'Pindobaçu', 'Pintadas' => 'Pintadas', 'Piraí do Norte' => 'Piraí do Norte', 'Piripá' => 'Piripá', 'Piritiba' => 'Piritiba', 'Planaltino' => 'Planaltino', 'Planalto' => 'Planalto', 'Poções' => 'Poções', 'Pojuca' => 'Pojuca', 'Ponto Novo' => 'Ponto Novo', 'Porto Seguro' => 'Porto Seguro', 'Potiraguá' => 'Potiraguá', 'Prado' => 'Prado', 'Presidente Dutra' => 'Presidente Dutra', 'Presidente Jânio Quadros' => 'Presidente Jânio Quadros', 'Presidente Tancredo Neves' => 'Presidente Tancredo Neves', 'Queimadas' => 'Queimadas', 'Quijingue' => 'Quijingue', 'Quixabeira' => 'Quixabeira', 'Rafael Jambeiro' => 'Rafael Jambeiro', 'Remanso' => 'Remanso', 'Retirolândia' => 'Retirolândia', 'Riachão das Neves' => 'Riachão das Neves', 'Riachão do Jacuípe' => 'Riachão do Jacuípe', 'Riacho de Santana' => 'Riacho de Santana', 'Ribeira do Amparo' => 'Ribeira do Amparo', 'Ribeira do Pombal' => 'Ribeira do Pombal', 'Ribeirão do Largo' => 'Ribeirão do Largo', 'Rio de Contas' => 'Rio de Contas', 'Rio do Antônio' => 'Rio do Antônio', 'Rio do Pires' => 'Rio do Pires', 'Rio Real' => 'Rio Real', 'Rodelas' => 'Rodelas', 'Ruy Barbosa' => 'Ruy Barbosa', 'Salinas da Margarida' => 'Salinas da Margarida', 'Salvador' => 'Salvador', 'Santa Bárbara' => 'Santa Bárbara', 'Santa Brígida' => 'Santa Brígida', 'Santa Cruz Cabrália' => 'Santa Cruz Cabrália', 'Santa Cruz da Vitória' => 'Santa Cruz da Vitória', 'Santa Inês' => 'Santa Inês', 'Santa Luzia' => 'Santa Luzia', 'Santa Maria da Vitória' => 'Santa Maria da Vitória', 'Santa Rita de Cássia' => 'Santa Rita de Cássia', 'Santa Teresinha' => 'Santa Teresinha', 'Santaluz' => 'Santaluz', 'Santana' => 'Santana', 'Santanópolis' => 'Santanópolis', 'Santo Amaro' => 'Santo Amaro', 'Santo Antônio de Jesus' => 'Santo Antônio de Jesus', 'Santo Estêvão' => 'Santo Estêvão', 'São Desidério' => 'São Desidério', 'São Domingos' => 'São Domingos', 'São Félix' => 'São Félix', 'São Félix do Coribe' => 'São Félix do Coribe', 'São Felipe' => 'São Felipe', 'São Francisco do Conde' => 'São Francisco do Conde', 'São Gabriel' => 'São Gabriel', 'São Gonçalo dos Campos' => 'São Gonçalo dos Campos', 'São José da Vitória' => 'São José da Vitória', 'São José do Jacuípe' => 'São José do Jacuípe', 'São Miguel das Matas' => 'São Miguel das Matas', 'São Sebastião do Passé' => 'São Sebastião do Passé', 'Sapeaçu' => 'Sapeaçu', 'Sátiro Dias' => 'Sátiro Dias', 'Saubara' => 'Saubara', 'Saúde' => 'Saúde', 'Seabra' => 'Seabra', 'Sebastião Laranjeiras' => 'Sebastião Laranjeiras', 'Senhor do Bonfim' => 'Senhor do Bonfim', 'Sento Sé' => 'Sento Sé', 'Serra do Ramalho' => 'Serra do Ramalho', 'Serra Dourada' => 'Serra Dourada', 'Serra Preta' => 'Serra Preta', 'Serrinha' => 'Serrinha', 'Serrolândia' => 'Serrolândia', 'Simões Filho' => 'Simões Filho', 'Sítio do Mato' => 'Sítio do Mato', 'Sítio do Quinto' => 'Sítio do Quinto', 'Sobradinho' => 'Sobradinho', 'Souto Soares' => 'Souto Soares', 'Tabocas do Brejo Velho' => 'Tabocas do Brejo Velho', 'Tanhaçu' => 'Tanhaçu', 'Tanque Novo' => 'Tanque Novo', 'Tanquinho' => 'Tanquinho', 'Taperoá' => 'Taperoá', 'Tapiramutá' => 'Tapiramutá', 'Teixeira de Freitas' => 'Teixeira de Freitas', 'Teodoro Sampaio' => 'Teodoro Sampaio', 'Teofilândia' => 'Teofilândia', 'Teolândia' => 'Teolândia', 'Terra Nova' => 'Terra Nova', 'Tremedal' => 'Tremedal', 'Tucano' => 'Tucano', 'Uauá' => 'Uauá', 'Ubaíra' => 'Ubaíra', 'Ubaitaba' => 'Ubaitaba', 'Ubatã' => 'Ubatã', 'Uibaí' => 'Uibaí', 'Umburanas' => 'Umburanas', 'Una' => 'Una', 'Urandi' => 'Urandi', 'Uruçuca' => 'Uruçuca', 'Utinga' => 'Utinga', 'Valença' => 'Valença', 'Valente' => 'Valente', 'Várzea da Roça' => 'Várzea da Roça', 'Várzea do Poço' => 'Várzea do Poço', 'Várzea Nova' => 'Várzea Nova', 'Varzedo' => 'Varzedo', 'Vera Cruz' => 'Vera Cruz', 'Vereda' => 'Vereda', 'Vitória da Conquista' => 'Vitória da Conquista', 'Wagner' => 'Wagner', 'Wanderley' => 'Wanderley', 'Wenceslau Guimarães' => 'Wenceslau Guimarães', 'Xique-Xique' => 'Xique-Xique'),
            'required'    => true,
            'label'       => 'Cidade'
        ),
        ( isset($_POST['billing_city']) ? $_POST['billing_city'] : '' )
    );	
		
	woocommerce_form_field(
        'billing_phone',
        array(
            'type'        => 'tel',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-first form-group'),
            'required'    => true,
            'label'       => 'Telefone'
        ),
        ( isset($_POST['billing_phone']) ? $_POST['billing_phone'] : '' )
    );			
	
	woocommerce_form_field(
        'billing_cellphone',
        array(
            'type'        => 'tel',
			'class'     => array('woocommerce-form-row woocommerce-form-row--wide form-row form-row-last form-group'),
            'required'    => true,
            'label'       => 'Celular'
        ),
        ( isset($_POST['billing_cellphone']) ? $_POST['billing_cellphone'] : '' )
    );		
	
}
add_action( 'woocommerce_register_form', 'misha_add_field_edit_account_form' );

/* Validação de campos */
function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
	
	function is_cpf_reg( $cpf ) {
		$cpf = preg_replace( '/[^0-9]/', '', $cpf );

		if ( 11 !== strlen( $cpf ) || preg_match( '/^([0-9])\1+$/', $cpf ) ) {
			return false;
		}

		$digit = substr( $cpf, 0, 9 );

		for ( $j = 10; $j <= 11; $j++ ) {
			$sum = 0;

			for ( $i = 0; $i < $j - 1; $i++ ) {
				$sum += ( $j - $i ) * intval( $digit[ $i ] );
			}

			$summod11 = $sum % 11;
			$digit[ $j - 1 ] = $summod11 < 2 ? 0 : 11 - $summod11;
		}

		return intval( $digit[9] ) === intval( $cpf[9] ) && intval( $digit[10] ) === intval( $cpf[10] );
	}
	
	function is_cnpj_reg( $cnpj ) {
		$cnpj = sprintf( '%014s', preg_replace( '{\D}', '', $cnpj ) );

		if ( 14 !== strlen( $cnpj ) || 0 === intval( substr( $cnpj, -4 ) ) ) {
			return false;
		}

		for ( $t = 11; $t < 13; ) {
			for ( $d = 0, $p = 2, $c = $t; $c >= 0; $c--, ( $p < 9 ) ? $p++ : $p = 2 ) {
				$d += $cnpj[ $c ] * $p;
			}

			if ( intval( $cnpj[ ++$t ] ) !== ( $d = ( ( 10 * $d ) % 11 ) % 10 ) ) {
				return false;
			}
		}

		return true;
	}
	if ( isset( $_POST['billing_birthdate'] ) && empty( $_POST['billing_birthdate'] ) ) {
		 $validation_errors->add( 'billing_birthdate_error', __( 'Campo data de nascimento é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_cpf'] ) && empty( $_POST['billing_cpf'] ) ) {
		 $validation_errors->add( 'billing_cpf_error', __( 'Campo CPF é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_cpf'] ) && ! empty( $_POST['billing_cpf'] ) && !is_cpf_reg( $_POST['billing_cpf'] ) ) {
		$validation_errors->add( 'billing_cpf_error', __( 'Campo CPF é inválido.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
		 $validation_errors->add( 'billing_first_name_error', __( 'Campo nome é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
		 $validation_errors->add( 'billing_last_name_error', __( 'Campo sobrenome é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_sex'] ) && empty( $_POST['billing_sex'] ) ) {
		 $validation_errors->add( 'billing_sex_error', __( 'Campo sexo é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
		 $validation_errors->add( 'billing_company_error', __( 'Campo razão social é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_cnpj'] ) && empty( $_POST['billing_cnpj'] ) ) {
		 $validation_errors->add( 'billing_cnpj_error', __( 'Campo CNPJ é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_cnpj'] ) && ! empty( $_POST['billing_cnpj'] ) && !is_cnpj_reg( $_POST['billing_cnpj'] ) ) {
		$validation_errors->add( 'billing_cnpj_error', __( 'Campo CNPJ é inválido.', 'woocommerce' ) );		
	}		
	if ( isset( $_POST['billing_perfil'] ) && empty( $_POST['billing_perfil'] ) ) {
		 $validation_errors->add( 'billing_perfil_error', __( 'Campo perfil é obrigatório.', 'woocommerce' ) );
	}	
	if ( isset( $_POST['billing_setor'] ) && empty( $_POST['billing_setor'] ) ) {
		 $validation_errors->add( 'billing_setor_error', __( 'Campo setor é obrigatório.', 'woocommerce' ) );
	}		
	/*if ( isset( $_POST['billing_segmento'] ) && empty( $_POST['billing_segmento'] ) ) {
		 $validation_errors->add( 'billing_segmento_error', __( 'Campo segmento é obrigatório.', 'woocommerce' ) );
	}*/
	if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
		 $validation_errors->add( 'billing_postcode_error', __( 'Campo CEP é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
		 $validation_errors->add( 'billing_address_1_error', __( 'Campo endereço é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_number'] ) && empty( $_POST['billing_number'] ) ) {
		 $validation_errors->add( 'billing_number_error', __( 'Campo número é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_state'] ) && empty( $_POST['billing_state'] ) ) {
		 $validation_errors->add( 'billing_state_error', __( 'Campo estado é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
		 $validation_errors->add( 'billing_cidade_error', __( 'Campo cidade é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
		 $validation_errors->add( 'billing_phone_error', __( 'Campo telefone é obrigatório.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_cellphone'] ) && empty( $_POST['billing_cellphone'] ) ) {
		 $validation_errors->add( 'billing_cellphone_error', __( 'Campo celular é obrigatório.', 'woocommerce' ) );
	}
	
	return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

/* Salvar campos */
function wooc_save_extra_register_fields( $customer_id ) {
	  
	  if ( isset( $_POST['billing_birthdate'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_birthdate', sanitize_text_field( $_POST['billing_birthdate'] ) );
      }	  
      if ( isset( $_POST['billing_first_name'] ) ) {
		 //First name field which is by default
		 update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		 // First name field which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
		 // Last name field which is by default
		 update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		 // Last name field which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
	  if ( isset( $_POST['billing_cpf'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_cpf', sanitize_text_field( $_POST['billing_cpf'] ) );
      }
	  if ( isset( $_POST['billing_sex'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_sex', sanitize_text_field( $_POST['billing_sex'] ) );
      }
	  if ( isset( $_POST['billing_company'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
      }	  
	  if ( isset( $_POST['billing_cnpj'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_cnpj', sanitize_text_field( $_POST['billing_cnpj'] ) );
      }	  	  
	  if ( isset( $_POST['billing_perfil'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_perfil', sanitize_text_field( $_POST['billing_perfil'] ) );
      }	  		  
	  if ( isset( $_POST['billing_setor'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_setor', sanitize_text_field( $_POST['billing_setor'] ) );
      }	  	  
	  /*if ( isset( $_POST['billing_segmento'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_segmento', sanitize_text_field( $_POST['billing_segmento'] ) );
      }	  	*/
	  if ( isset( $_POST['billing_postcode'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
      }	  
	  if ( isset( $_POST['billing_address_1'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
      }	  	  
	  if ( isset( $_POST['billing_number'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_number', sanitize_text_field( $_POST['billing_number'] ) );
      }	  
	  if ( isset( $_POST['billing_neighborhood'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_neighborhood', sanitize_text_field( $_POST['billing_neighborhood'] ) );
      }	 	 
	  if ( isset( $_POST['billing_address_2'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
      }	 	  
	  if ( isset( $_POST['billing_state'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
      }		  
	  if ( isset( $_POST['billing_city'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
      }		  
	  if ( isset( $_POST['billing_phone'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
      }			  
	  if ( isset( $_POST['billing_cellphone'] ) ) {
		 // Phone input filed which is used in WooCommerce
		 update_user_meta( $customer_id, 'billing_cellphone', sanitize_text_field( $_POST['billing_cellphone'] ) );
      }			  
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

// Register Custom Post Type
function case_cpt() {

	$labels = array(
		'name'                  => _x( 'Casos de Sucesso', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Caso de Sucesso', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Casos de Sucesso', 'text_domain' ),
		'name_admin_bar'        => __( 'Caso de Sucesso', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Adicionar novo caso', 'text_domain' ),
		'add_new'               => __( 'Adicionar novo', 'text_domain' ),
		'new_item'              => __( 'Novo caso', 'text_domain' ),
		'edit_item'             => __( 'Editar caso', 'text_domain' ),
		'update_item'           => __( 'Atualizar caso', 'text_domain' ),
		'view_item'             => __( 'Ver caso', 'text_domain' ),
		'view_items'            => __( 'Ver casos', 'text_domain' ),
		'search_items'          => __( 'Buscar caso', 'text_domain' ),
		'not_found'             => __( 'Não encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Não encontrado na lixeira', 'text_domain' ),
		'featured_image'        => __( 'Imagem destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Definir imagem destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagem destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Usar imagem destacada', 'text_domain' ),
		'insert_into_item'      => __( 'Inserir no caso', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Caso de Sucesso', 'text_domain' ),
		'description'           => __( 'Casos de sucesso documentados', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businesswoman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'casos', $args );

}
add_action( 'init', 'case_cpt', 0 );

/*add_filter( 'login_url', 'custom_login_url', PHP_INT_MAX );
function custom_login_url( $login_url ) {
	$login_url = site_url( 'sb-adm.php', 'login' );	
    return $login_url;
}*/

add_filter ('woocommerce_add_to_cart_redirect', 'woo_redirect_to_checkout');

function woo_redirect_to_checkout() {

	if(isset($_POST["redirect"])){
		if($_POST["redirect"] == "true"){
			$cart_url = wc_get_cart_url();
			return $cart_url;
		}
	}
}


/* parte da solução do related.php */
add_filter( 'woocommerce_output_related_products_args', 'bbloomer_change_number_related_products', 9999 );
 
function bbloomer_change_number_related_products( $args ) {
 $args['posts_per_page'] = 30; // # of related products
 $args['columns'] = 4; // # of columns per row
 return $args;
}