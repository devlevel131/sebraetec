<?php
/**
 * Theme basic setup
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'understrap_setup' );

if ( ! function_exists( 'understrap_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function understrap_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on understrap, use a find and replace
		 * to change 'understrap' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'understrap', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'understrap' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
			)
		);

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'understrap_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Check and setup theme default settings.
		understrap_setup_theme_default_settings();

	}
}

/* Apenas o Estado da Bahia */
 
function aelia_checkout_shipping_filter_BA_states() {
   if ( !is_checkout() && !is_account_page() && !is_page(211) ) {
      return;
   }
   ?>
      
   <script>
   jQuery(document).ready(function($) {
 
      $(document.body).on('country_to_state_changed', function() {
 
         function set_shipping_state(state) {
            var $shipping_state = $('#billing_state');
            var $shipping_state_option = $('#billing_state option[value="' + state + '"]');
            var $shipping_state_option_no = $('#billing_state option[value!="' + state + '"]');
            $shipping_state_option_no.remove();
            $shipping_state_option.attr('selected', true);
         }
 
         var $shipping_country = $('#billing_country');
 
         var new_shipping_state = '';
 
         switch($shipping_country.val()) {
            case 'BR':
               new_shipping_state = 'BA';
               break;
         }
 
         if( ! $.isEmptyObject(new_shipping_state)) {
            set_shipping_state(new_shipping_state);
         } 
 
      });	  
 
   });  
   </script>
       
   <?php
}

add_action( 'wp_footer', 'aelia_checkout_shipping_filter_BA_states' );

/* Enviar e-mail do pedido de compra para o gestor */

/*function sv_conditional_email_recipient( $recipient, $order ) {

	// Bail on WC settings pages since the order object isn't yet set yet
	// Not sure why this is even a thing, but shikata ga nai
	$page = $_GET['page'] = isset( $_GET['page'] ) ? $_GET['page'] : '';
	if ( 'wc-settings' === $page ) {
		return $recipient; 
	}
	
	// just in case
	if ( ! $order instanceof WC_Order ) {
		return $recipient; 
	}

	$billing_city = $order->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$email = get_field('emails_de_destinatarios_cidade', $exist_city->ID );
        $recipient .= ','.$email;
	}
	
	return $recipient;
}
add_filter( 'woocommerce_email_recipient_new_order', 'sv_conditional_email_recipient', 10, 2 );*/

/* Enviar e-mail do pedido de orçamento para o gestor */

function add_cc_to_wc_order_emails( $header, $mail_id, $mail_obj ) {
    //$available_for = array( 'customer_on_hold_order', 'customer_processing_order', 'customer_completed_order' );
    
	$admin_emails = array( 
		'new_order',
        'adq_admin_new_quote'
    );
	
	$billing_city = $mail_obj->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$email = get_field('emails_de_destinatarios_cidade', $exist_city->ID );
	}
	
	if( in_array( $mail_id, $admin_emails ) ){

		$header .= 'Cc: ' . $email . "\r\n";
	
	}

	return $header;
	
}
add_filter( 'woocommerce_email_headers', 'add_cc_to_wc_order_emails', 99, 3 );


/* Adicionar os gestores regionais aos pedidos */

function before_checkout_create_order($order, $data) {

	$billing_city = $order->get_billing_city();
	
	$exist_city = get_page_by_title($billing_city, OBJECT, 'cidade');
	
	if ( $exist_city ){
		$store_manager_id = get_field('gerentes_de_pedidos_cidade', $exist_city->ID);
	}

	$order->update_meta_data('gerentes_do_pedido_pedido', $store_manager_id);
}
add_action('woocommerce_checkout_create_order', 'before_checkout_create_order', 20, 2);

/* Mostrar os pedidos apenas para os gestores regionais */

function custom_admin_shop_manager_orders($query) {
	global $pagenow;
	$qv = &$query->query_vars;

	$currentUserRoles = wp_get_current_user()->roles;
	$user_id = get_current_user_id();

	if (in_array('shop_manager', $currentUserRoles)) {
		if ( $pagenow == 'edit.php' && isset($qv['post_type']) && $qv['post_type'] == 'shop_order' ) {
					
			$query->set( 'meta_query', array(
				array(
					'key'     => 'gerentes_do_pedido_pedido',
					'value'   => $user_id,
					'compare' => 'LIKE',
				)
			) );
			
		}
	}

	return $query;
}
add_filter('pre_get_posts', 'custom_admin_shop_manager_orders');

/* Validação do termo */

function add_fake_error($posted) {
    if ($_POST['confirm-order-flag'] == "1") {
        wc_add_notice( __( "custom_notice"), 'error');
    } 
}

add_action('woocommerce_after_checkout_validation', 'add_fake_error');

/* Remove safona termos e condições */

function golden_oak_web_design_woocommerce_checkout_terms_and_conditions() {
  remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
}
add_action( 'wp', 'golden_oak_web_design_woocommerce_checkout_terms_and_conditions' );