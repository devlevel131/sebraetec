<?php
/**
 * Posttypes
 *
 * @package understrap
 */

//Cidades
if( ! function_exists( 'cidades_post_type' ) ) :
	function cidades_post_type() {
		$labels = array(
			'name' => 'Cidades',
			'singular_name' => 'Cidade',
			'add_new' => 'Adicionar Cidade',
			'all_items' => 'Todos os Cidades',
			'add_new_item' => 'Adicionar Cidade',
			'edit_item' => 'Editar Cidade',
			'new_item' => 'Novo Cidade',
			'view_item' => 'Ver Cidade',
			'search_items' => 'Buscar Cidades',
			'not_found' => 'Nenhum Cidade encontrado',
			'not_found_in_trash' => 'Nenhum Cidade encontrado na lixeira',
			'parent_item_colon' => 'Parent Cidade'
			//'menu_name' => default to 'name'
		);
		$args = array(
			'labels' => $labels,
			'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
			'publicly_queryable' => true,  // you should be able to query it
			'show_ui' => true,  // you should be able to edit it in wp-admin
			'exclude_from_search' => true,  // you should exclude it from search results
			'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
			'has_archive' => true,  // it shouldn't have archive page
			'supports' => array(
				'title',
				//'editor',
				//'thumbnail',
				//'excerpt',
				//'author',
				//'trackbacks',
				//'custom-fields',
				//'comments',
				//'revisions',
				//'page-attributes', // (menu order, hierarchical must be true to show Parent option)
				//'post-formats',
			),
			'menu_position' => 7,
		);
		register_post_type( 'cidade', $args );
		//flush_rewrite_rules();
		
	}
	add_action( 'init', 'cidades_post_type' );
endif; // end of function_exists()

//Cidades
if( ! function_exists( 'categorias_produto_type' ) ) :
	function categorias_produto_type() {
			
		register_taxonomy( 'tipos-de-produtos', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'labels' => array(
					'name' => 'Tipos de produtos',
					'singular_name' => 'Tipo de produto',
				)
			)
		);
		
		register_taxonomy( 'setores', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'labels' => array(
					'name' => 'Setores',
					'singular_name' => 'Setor',
				)
			)
		);
		
		register_taxonomy( 'perfis', // register custom taxonomy - category
			'product',
			array(
				'show_admin_column' => true,
				'hierarchical' => true,
				'labels' => array(
					'name' => 'Perfis',
					'singular_name' => 'Perfil',
				)
			)
		);		
				
		
	}
	add_action( 'init', 'categorias_produto_type' );
endif; // end of function_exists()
