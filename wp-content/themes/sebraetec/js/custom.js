/*
Custom JS
*/

/* Checkout Term */
jQuery(function($) {	  
	var checkout_form = $('form.checkout');

	checkout_form.on('checkout_place_order', function () {
		if ($('#confirm-order-flag').length == 0) {
			checkout_form.append('<input type="hidden" id="confirm-order-flag" name="confirm-order-flag" value="1">');
		}
		return true;
	});
	
	$(document.body).on('checkout_error', function () {
		var error_count = $('.woocommerce-error li').length;

		if (error_count == 1) { // Validation Passed (Just the Fake Error I Created Exists)
			// Show Confirmation Modal or Whatever
			$('.woocommerce-NoticeGroup-checkout .woocommerce-error').hide();
			$('#modal-term').modal('show');
		}else{ // Validation Failed (Real Errors Exists, Remove the Fake One)
			$('.woocommerce-error li').each(function(){
				var error_text = $(this).text();
				console.log(error_text);
				if (error_text == '\n\t\t\tcustom_notice\t\t'){
					$(this).css('display', 'none');
				}
			});
		}
	});
	
	$('#confirm-order-button').click(function () {
		$('#confirm-order-flag').val('');
		$('#place_order').trigger('click');
	});

});